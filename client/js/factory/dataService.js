app.factory('dataService', ['$http','$rootScope', function($http,$rootScope){
	return {
		get : function(url,filters,cb){
			$http.get($rootScope.base_path+url+"?query="+encodeURIComponent(JSON.stringify(filters)),{withCredentials: true})
				.success(function(data){ return cb(null,data);})
				.error(function(err){return cb(err,null);})

		},
		put : function(url,data,cb){
			$http.put($rootScope.base_path+url , data,{withCredentials: true})
				.success(function(data){ return cb(null,data);})
				.error(function(err){return cb(err,null);})
		},
		post : function(url,data,cb){
			$http.post($rootScope.base_path+url , data,{withCredentials: true})
				.success(function(data){ return cb(null,data);})
				.error(function(err){return cb(err,null);})
		},
		delete: function(url,data,cb){
			$http.delete($rootScope.base_path+url , data,{withCredentials: true})
				.success(function(data){ return cb(null,data);})
				.error(function(err){return cb(err,null);})
		},
		getAddressFromCoordinates :function(longitude,latitude,cb){
			$http.get("https://maps.googleapis.com/maps/api/geocode/json?latlng="+longitude+","+latitude+"&key=AIzaSyCVuaPrhXKsPoE5vNNAjitzAjLSvLvXZAY")
				.success(function(data){ return cb(null,data);})
				.error(function(err){return cb(err,null);})
		},
		getCoordinatesFromAddress :function(address,cb){
			$http.get("https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&key=AIzaSyCVuaPrhXKsPoE5vNNAjitzAjLSvLvXZAY")
				.success(function(data){ return cb(null,data);})
				.error(function(err){return cb(err,null);})
		}
	};

}])