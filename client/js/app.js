
var app = angular.module('yolo_itinerary_client',['ui.router','ui.grid','LocalStorageModule','ngDialog','ngFileUpload','dndLists','ngSanitize','ngTagsInput','ngAnimate', 'ui.bootstrap','slickCarousel','djds4rce.angular-socialshare','pascalprecht.translate','ngCookies','angular-img-cropper']);


app.config(function($stateProvider,$urlRouterProvider,$locationProvider,$translateProvider){
	$translateProvider.translations('en', translationsEN);
    $translateProvider.translations('zh_CN', translationszh_CN);
    $translateProvider.translations('zh_TW', translationszh_TW);
    $translateProvider.preferredLanguage('en');
    $translateProvider.useCookieStorage();
    $translateProvider.fallbackLanguage('en');

	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('index', {
			url : '',
			templateUrl : "/screens/index/index.html",
			controller : "indexCtrl"
		})

		.state('index.home', {
			url : '/',
			templateUrl : "/screens/home/home.html",
			controller : "homeCtrl"
		})

		.state('index.login', {
			url : '/login',
			templateUrl : "/screens/login/login.html",
			controller : "loginCtrl",
			notAccessAfterLogin : true
		})
		
		.state('index.sign-up', {
			url : '/sign-up',
			templateUrl : "/screens/sign-up/sign-up.html",
			controller : "signupCtrl",
            notAccessAfterLogin : true
		})
		.state('index.verify', {
			url : '/user/verify/:token/:email',
			templateUrl : "/screens/login/login.html",
			controller : "loginCtrl",
            notAccessAfterLogin : true
		})
		.state('index.forgot', {
			url : '/forgot',
			templateUrl : "/screens/forgot/forgot.html",
			controller : "forgotCtrl",
            notAccessAfterLogin : true
		})
		.state('index.setNewPassword', {
			url : '/forgot/password/:token/:email',
			templateUrl : "/screens/forgot/forgot.html",
			controller : "forgotCtrl",
            notAccessAfterLogin : true
		})
		.state('index.profile', {
			url : '/profile',
			templateUrl : "/screens/profile/profile.html",
			controller : "profileCtrl",
            AccessAfterLogin : true
		})

		.state('index.editprofile', {
			url : '/profile/edit',
			templateUrl : "/screens/profile/edit/edit.html",
			controller : "profileEditCtrl",
            AccessAfterLogin : true
		})
		
		.state('index.createItineraryDays', {
			url : '/itinerarys/create',
			templateUrl : "/screens/itinerarys/create/create.html",
			controller : "itinerarysCreateCtrl"
		})
		.state('index.editItinerarysDays', {
			url : '/itinerarys/create/:machine_name/:id',
			templateUrl : "/screens/itinerarys/create/create.html",
			controller : "itinerarysCreateCtrl"
		})

		.state('index.ItineraryView', {
			url : '/itinerarys/:machine_name/:id',
			templateUrl : "/screens/itinerarys/view/view.html",
			controller : "itinerarysViewCtrl"
		})
		.state('index.editItinerary', {
			url : '/itinerarys/:machine_name/:id/edit',
			templateUrl : "/screens/itinerarys/edit/edit.html",
			controller : "itinerarysEditCtrl"
		})
		.state('index.createJournal', {
			url : '/journal/create',
			templateUrl : "/screens/journal/create/create.html",
			controller : "createJournalCtrl",
            AccessAfterLogin : true
		})
		.state('index.singleJournal', {
			url : '/journal/:id',
			templateUrl : "/screens/journal/journal.html",
			controller : "singleJournalCtrl"
		})	
		.state('index.editJournal', {
			url : '/journal/edit/:id',
			templateUrl : "/screens/journal/edit/edit.html",
			controller : "editJournalCtrl",
            AccessAfterLogin : true
		})
		.state('index.createAttraction', {
			url : '/attraction/create',
			templateUrl : "/screens/attractions/create/create.html",
			controller : "createAttractionCtrl",
			AccessAfterLogin : true
		})
		.state('index.singleAttraction', {
			url : '/attraction/:id',
			templateUrl : "/screens/attractions/attraction.html",
			controller : "singleAttractionCtrl"
		})
		.state('index.editAttraction', {
			url : '/attraction/edit/:id',
			templateUrl : "/screens/attractions/edit/edit.html",
			controller : "editAttractionCtrl",
			AccessAfterLogin : true
		})
		.state('index.city', {
			url : '/destination/:id',
			templateUrl : "/screens/destinations/city.html",
			controller : "cityCtrl"
		})						

		/*Admin Routes*/
		//login route
		.state('admin', {
			url : '',
			templateUrl : "/screens/admin/index/index.html",
			controller : "adminIndexCtrl"
		})
		.state('admin.login', {
			url : '/admin/login',
			templateUrl : "/screens/admin/login/login.html",
			controller : "adminLoginCtrl",
			notAccessAfteradminLogin : true
		})
		.state('admin.admin', {
			url : '/admin',
			templateUrl : "/screens/admin/admin/admin.html",
			controller : "adminCtrl",
			adminLoginRequired : true,
		})


		.state('admin.users', {
			url : '/admin/users',
			templateUrl : "/screens/admin/users/users.html",
			controller : "adminUsersCtrl",
			adminLoginRequired : true,
		})
		.state('admin.createUser', {
			url : '/admin/users/create',
			templateUrl : "/screens/admin/users/create/create.html",
			controller : "usersCreateCtrl",
			adminLoginRequired : true,
		})
		.state('admin.editUser', {
			url : '/admin/users/edit/:id',
			templateUrl : "/screens/admin/users/edit/edit.html",
			controller : "usersEditCtrl",
			adminLoginRequired : true,
		})
		.state('admin.attractionsImport', {
			url : '/admin/attractions/import',
			templateUrl : "/screens/admin/attractions/import/import.html",
			controller : "attractionImportCtrl",
			adminLoginRequired : true,
		})
		.state('admin.attractions', {
			url : '/admin/attractions',
			templateUrl : "/screens/admin/attractions/attractions.html",
			controller : "attractionsCtrl",
			adminLoginRequired : true,
		})
		.state('admin.attractionsCreate', {
			url : '/admin/attractions/create',
			templateUrl : "/screens/admin/attractions/create/create.html",
			controller : "attractionsCreateCtrl",
			adminLoginRequired : true,
		})
		.state('admin.attractionsEdit', {
			url : '/admin/attractions/edit/:id',
			templateUrl : "/screens/admin/attractions/edit/edit.html",
			controller : "attractionsEditCtrl",
			adminLoginRequired : true,
		})
		.state('admin.destinations', {
			url : '/admin/destinations',
			templateUrl : "/screens/admin/destinations/destinations.html",
			controller : "destinationsCtrl",
			adminLoginRequired : true,
		})
		.state('admin.destinationsCreate', {
			url : '/admin/destinations/create',
			templateUrl : "/screens/admin/destinations/create/create.html",
			controller : "destinationsCreateCtrl",
			adminLoginRequired : true,
		})
		.state('admin.destinationsEdit', {
			url : '/admin/destinations/edit/:id',
			templateUrl : "/screens/admin/destinations/edit/edit.html",
			controller : "destinationsEditCtrl",
			adminLoginRequired : true,
		})
		.state('admin.journalsCreate', {
			url : '/admin/journals/create',
			templateUrl : "/screens/admin/journals/create/create.html",
			controller : "journalsCreateCtrl",
			adminLoginRequired : true,
		})
		.state('admin.journals', {
			url : '/admin/journals',
			templateUrl : "/screens/admin/journals/journals.html",
			controller : "adminJournalsCtrl",
			adminLoginRequired : true,
		})
		.state('admin.editJournal', {
			url : '/admin/journals/edit/:id',
			templateUrl : "/screens/admin/journals/edit/edit.html",
			controller : "journalsEditCtrl",
			adminLoginRequired : true,
		})
		.state('admin.showcomments', {
			url : '/admin/journals/:id/comments',
			templateUrl : "/screens/admin/comments/comments.html",
			controller : "showCommentsCtrl",
			adminLoginRequired : true,
		})
		.state('admin.editComments', {
			url : '/admin/comments/edit/:id',
			templateUrl : "/screens/admin/comments/edit/edit.html",
			controller : "commentsEditCtrl",
			adminLoginRequired : true,
		})
		.state('admin.itineraries', {
			url : '/admin/itineraries',
			templateUrl : "/screens/admin/itineraries/itineraries.html",
			controller : "adminItinerariesCtrl",
			adminLoginRequired : true,
		})		

// $locationProvider.html5Mode({
//                  enabled: true,
//                  requireBase: false
//           });

})



app.run(function($rootScope, $state,localStorageService,users) {
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
        var loginUser = localStorageService.get('yolo_itinerary_client');

        $rootScope.currentState = toState.name;

        //if only index then redirect to home
        if(toState.url == ""){
             $state.go('index.home');
              event.preventDefault();
        }

        //if already login , can't go on login page
        if(toState.notAccessAfterLogin && loginUser && loginUser._id){
            $state.go('index.home');
            $rootScope.currentState = 'index.home';
            event.preventDefault();
        }

        //if already login , can't go on login page
        if(toState.AccessAfterLogin && (!loginUser || !loginUser._id)){
            $state.go('index.login');
           $rootScope.currentState = 'index.login';
            event.preventDefault();
        }


        //trying to access admin pages without login where login is required
        if(toState.adminLoginRequired && ((loginUser && !loginUser.isadmin && !loginUser.issuperadmin) || !loginUser) ){
            $state.go('admin.login');
            $rootScope.currentState = 'admin.login';
            event.preventDefault();
        }
        
        //trying to access login admin page after login 
		if(toState.notAccessAfteradminLogin && loginUser && ( loginUser.isadmin || loginUser.issuperadmin)){
            $state.go('admin.admin');
            $rootScope.currentState = 'admin.admin';
            event.preventDefault();
        }


   })
});

/*app.controller('Ctrl', ['$translate', '$scope', function ($translate, $scope) {

  $scope.changeLanguage = function (langKey) {
  	console.log('in the change language');
    $translate.use(langKey);
};
}])*/

app.run(function($FB){
  $FB.init('1271313996230001');
});