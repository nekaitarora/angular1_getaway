app.service('itinerarys', ['$rootScope','dataService','$rootScope', function($rootScope,dataService,$rootScope){
	this.getDistance = function(query , cb){
		dataService.get("/api/itinerarys/distance",query,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.getDayDistanceTime = function(day , cb){
		dataService.post("/api/itinerarys/daydistancetime",{day : day},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.getAllModes = function(attraction, cb){
		dataService.post("/api/itinerarys/getallmodes",{attraction : attraction},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.createItinerary =function(itinerary,cb){
		dataService.post("/api/itinerarys/create",itinerary,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.getItinerary =function(machine_name,id,cb){
		dataService.get("/api/itinerarys/"+machine_name+"/"+id,{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}

	this.updateItinerary =function(id,json,cb){
		dataService.put("/api/itinerarys/"+id,json,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.deleteItinerary =function(id,cb){
		dataService.delete("/api/itinerarys/"+id,{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.getMap =function(day ,cb){
		dataService.post("/api/itinerarys/route-optimizer",{day : day},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.markdeleted =function(id,machine_name,cb){
		dataService.delete("/api/itinerarys/"+id+"/"+machine_name+"/markdeleted",{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.saveMyDraft = function(id,machine_name,cb){
		dataService.post("/api/itinerarys/clone/"+id+"/"+machine_name,{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}

	this.bookmarkItinerary = function(iid,cb){
		dataService.put("/api/itinerarys/bookmark/"+iid,{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.getAllItinerariesCount = function(filters,cb){
		dataService.get('/api/itinerarys/all/count',filters,function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})
	}
	this.getAllItineraries = function(filters , cb){
		dataService.get('/api/itinerarys/all',filters,function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})
	}	
	this.deletePermanently= function(uid,cb){
		dataService.delete('/api/itinerarys/delete/'+uid,{},function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		});	
	}
	this.getFilteredItineraries = function(filters , cb){
		dataService.get('/api/itinerarys/relatedItinerary',filters,function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})
	}
		
}]);