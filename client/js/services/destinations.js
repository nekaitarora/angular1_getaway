app.service('destinations', ['$rootScope','dataService','$rootScope', function($rootScope,dataService,$rootScope){

	this.getAllDestinations = function(query , cb){
		dataService.get("/api/destinations",query,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.getDestination = function(id , cb){
		dataService.get("/api/destinations/"+id,{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.getDestinationsCount = function(query , cb){
		dataService.get("/api/destinations/count",query,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.getCountries = function(cb){
		dataService.get("/api/destinations/countries",{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.destinationCreate = function(destination,cb){
		dataService.post("/api/destinations/create",destination,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.destinationUpdate = function(destination,cb){
		dataService.put("/api/destinations/"+destination._id,destination,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.destinationDelete = function(id,cb){
		dataService.delete("/api/destinations/"+id,{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.destinationRestaurantCount = function(id,cb){
		dataService.get("/api/destinations/restaurant/count/"+id,{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}

}]);