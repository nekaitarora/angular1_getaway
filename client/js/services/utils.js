app.service('utils', ['$rootScope','dataService', function($rootScope,dataService){
	this.getAllLanguages = function(cb){
		dataService.get("/api/utils/languages",{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.getAllCountries = function(cb){
		dataService.get("/api/utils/countries",{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.create_machine_name = function(data){
		if(data == undefined){
			return "";
		}
		var string = data.replace(/ /g,"-").toLowerCase();
		string = string.replace(/,/g,"-").toLowerCase();
	   // string = string.replace(/[^a-z0-9-]/g, '');
	    return string;
	}
}]);