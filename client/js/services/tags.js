app.service('tags', ['$rootScope','dataService','$rootScope', function($rootScope,dataService,$rootScope){

	this.createTags = function(tag,cb){
		dataService.post("/api/tags/create",tag,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})	
	}

	this.getallTags = function(filters , cb){
		dataService.get('/api/tags/all',filters,function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})
	}	

	this.getTag=function(uid,cb){
		dataService.get('/api/tags/'+uid,{},function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})		
	}

}]);