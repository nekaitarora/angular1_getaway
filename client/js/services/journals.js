app.service('journals', ['$rootScope','dataService','$rootScope', function($rootScope,dataService,$rootScope){

	this.createJournal = function(journal,cb){
		dataService.post("/api/journals/create",journal,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})	
	}
	this.deleteJournal= function(uid,cb){
		dataService.delete('/api/journals/'+uid,{},function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		});	
	}	
	this.getalljournalscount = function(filters,cb){
		dataService.get('/api/journals/all/count',filters,function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})
	}
	this.getalljournals = function(filters , cb){
		dataService.get('/api/journals/all',filters,function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})
	}
	this.getJournal=function(uid,cb){
		dataService.get('/api/journals/'+uid,{},function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})		
	}
	this.updatejournal = function(id, journal,cb){
		dataService.put('/api/journals/'+id,journal,function(err,result){
			if(err){return cb(err,null)}
			return cb(null,result);			
		})
	}
	this.bookmarkJournal = function(jid,cb){
		// console.log('attraction is',attraction);
		dataService.put("/api/journals/bookmark/"+jid ,{}, function(err,result){
			if(err){return cb(err,null);}
			$rootScope.loggedInUser = result;
			return cb(null,result);
		})			
	}
}]);