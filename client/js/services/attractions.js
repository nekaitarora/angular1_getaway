app.service('attractions', ['$rootScope','dataService','$rootScope', function($rootScope,dataService,$rootScope){

	this.importData = function(json,cb){
		dataService.post("/api/attractions/import",json,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.importAllData = function(json,cb){
		dataService.post("/api/attractions/import/all",json,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}
	this.getProgress = function(cb){
		dataService.get("/api/attractions/import/progress",{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})	
	}
	this.getAttractionsCount = function(query,cb){
		dataService.get("/api/attractions/count",query,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})	
	}
	this.getAllAttractions = function(query,cb){
		dataService.get("/api/attractions",query,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})	
	}
	this.getAttraction = function(id,cb){
		dataService.get("/api/attractions/"+id,{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})	
	}
	this.createAttraction = function(attraction,cb){
		dataService.post("/api/attractions/create",attraction,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})	
	}
	this.duplicateAttraction = function(id,cb){
		dataService.post("/api/attractions/duplicate/"+id,{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})	
	}
	this.deleteAttraction = function(id,cb){
		dataService.delete("/api/attractions/"+id,{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})			
	}

	this.updateAttraction = function(attraction,cb){
		dataService.put("/api/attractions/"+attraction._id,attraction,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})			
	}
	this.updateReviews = function(data,cb){
		dataService.put("/api/attractions/reviews/"+data._id,data,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})			
	}
	this.getReviews = function(query,cb){
		dataService.get("/api/attractions/reviews",query,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})	
	}	
	this.getAverageRating = function(query,cb){
		dataService.get("/api/attractions/averageRating/"+query,{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})	
	}
	this.bookmarkAttraction = function(aid,cb){
		// console.log('attraction is',attraction);
		dataService.put("/api/attractions/bookmark/"+aid ,{}, function(err,result){
			if(err){return cb(err,null);}
			$rootScope.loggedInUser = result;
			return cb(null,result);
		})			
	}	

}]);