app.service('categories', ['$rootScope','dataService','$rootScope', function($rootScope,dataService,$rootScope){

	this.getAllCategories = function(cb){
		dataService.get("/api/categories",{},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	}

}]);