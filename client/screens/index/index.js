app.controller('indexCtrl', ['$scope','$rootScope','users','localStorageService','$location','$state','destinations','$cookies','$translate', function($scope,$rootScope,users,localStorageService,$location,$state,destinations,$cookies,$translate){

	$rootScope.base_path = $location.protocol() + '://' + $location.host()+":"+$location.port();
	users.islogin();
	$scope.language = $cookies.get('NG_TRANSLATE_LANG_KEY');
	$scope.selectedItem = $scope.language.replace(/"/g,"");
	console.log('cookie language is',$scope.language,$scope.language.replace(/"/g,""));
	$scope.changeState = function(name){
		$rootScope.editProfileTab = name;
		console.log(name);
	}


	console.log('the selected language is ',$scope.selectedItem);
$scope.changeLanguage = function (langKey) {
  	console.log('in the change language',langKey);
    $translate.use(langKey);
};

	$scope.logout = function(){
		users.logout(function(err,result){
			if(result){
				$rootScope.loggedInUser = null;
				localStorageService.remove('yolo_itinerary_client');
					$state.go("index.home")			
				console.log('logout successfully');
			}
		});
	}

    //get all destinations for filters
	destinations.getAllDestinations({},function(err,result){
		if(err){return console.log(err);}
		$scope.destinations = result;
	})
}]);