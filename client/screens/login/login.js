app.controller('loginCtrl', ['$scope','users','localStorageService','$rootScope','dialogs','$state','$location', function($scope,users,localStorageService,$rootScope,dialogs,$state,$location){

	var query_params = $location.search();

	$scope.error='';
	$scope.initilizeVariables = function(){
		$scope.titles = {};
		$scope.titles.email = "Enter Your Email";
		$scope.titles.password = "Enter Your Password";

		$scope.errorClass = "";
	}

	//initialize variables 
	$scope.initilizeVariables();

	if($state.params.token  && $state.params.email){
		users.verifyuser('/api/users/verify/'+$state.params.email+'/'+$state.params.token,function(err,result){
			if(err){dialogs.openErrorDialog(err);return;}
			else{
				if(result._id){
					$scope.message = "Verification Done Please login ..";
				}else{
					$scope.message = "Something went Wrong";	
				}	
			}
		})
	}

	$scope.fblogin = function(){
		users.fblogin(function(err,result){
			if(err){dialogs.openErrorDialog(err);return;}
		});
	}

	$scope.login = function(){
	
		$scope.initilizeVariables();
		if($scope.user.email && $scope.user.password && $scope.loginForm.$valid){
			users.login($scope.user,function(err,result){
					if(err){
						if(err.message){
							$scope.errorClass = "error";
							$scope.titles.email = err.message;
							$scope.titles.password = err.message;
						}else{
							console.log("Login Error:",err);
						}
						return;
					}else{

						localStorageService.set('yolo_itinerary_client',result);
						$rootScope.loggedInUser = result;
							console.log("----------------------",query_params);
						if(query_params.id && query_params.machine_name){
							return 	$state.go('index.editItinerary',query_params);
						}
						$state.go('index.home');
					}
			})
		}else{
			$scope.loginForm.email.$touched =true;
			$scope.loginForm.password.$touched =true;
		}

	}
	// var storage = $storage('radioinfo');
	// var user = storage.getItem('user');
	// if(user){
	// 	$state.go('index.home');
	// }


	// $scope.forgotPassword = function(){
	// 	var config = {
	// 		'filter' : {
	// 			'local.email' : $scope.user.email
	// 		}
	// 	};
	// 	console.log("forgotPassword", $scope.user);
	// 	dataService.get('/api/users/reset/password', config, function(err, result){
	// 		if(err){
	// 			$scope.error= err;
	// 			console.log("-----",err);
	// 		}else{
	// 			console.log("result",result);
	// 		}
	// 	})
	// }

	// $scope.login = function(){
	// 	console.log("loginCtrl");
	// 	$scope.message= '';
	// 	dataService.post('/api/users/login',$scope.user,function(err,result){
	// 		if(err){
	// 			if(err.ermessage){
	// 				$scope.error = err.ermessage.username ? err.ermessage.username : err.ermessage.password;
	// 			}else{
	// 				console.log("error in rest api, see console on server",err);
	// 			}
	// 			return;
	// 		}else{
	// 			var storage = $storage('radioinfo');
	// 			storage.setItem('user', result);
	// 			console.log("root scpoe",$rootScope);
	// 			$rootScope.loggedInUser = result;
	// 			$state.go('index.home');
	// 		}
	// 	})
	// }



}]);