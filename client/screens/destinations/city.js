app.controller('cityCtrl', ['$scope','dialogs','$state','destinations','categories','attractions','journals','$rootScope','localStorageService','itinerarys', function($scope,dialogs,$state,destinations,categories,attractions,journals,$rootScope,localStorageService,itinerarys){
	$scope.sidebar = {};
	$scope.sidebar.filter = {published: true};

	$scope.changeState = function(){
		$state.go('index.city',{id :$scope.sidebar.filter.city})
	}

	destinations.getDestination($state.params.id,function(err,result){
		if(err){
			$state.go("index.home");
		}
		$scope.sidebar.filter.city = result._id;
		$scope.destination = result;
	})	

	destinations.getAllDestinations({},function(err,result){
		if(err){return console.log("getAllDestinations",err);}
		$scope.destinations = result;		
	})
	destinations.destinationRestaurantCount($state.params.id,function(err,result){
		if(err){return console.log("getAllDestinations",err);}
		$scope.restaurantCount = result.count;		
	})

	categories.getAllCategories(function(err,result){
		if(err){return console.log("getAllCategories",err);}
		$scope.sidebar.filter.category  = result[0]._id;
		$scope.categories = result;
		$scope.UpdateAttractions();
	})

	itinerarys.getAllItinerariesCount({filter : {"published":"true","trip_to.destination":$state.params.id}},function(err,result){
		if(err){return console.log("getAllDestinations",err);}
		$scope.itineraryCount = result.count;	
	})
	$scope.UpdateAttractions = function(){
		attractions.getAllAttractions($scope.sidebar,function(err,result){
			if(err){return console.log("getAllAttractions",err);}
			$scope.attractions = result;
		})
	}

	journals.getalljournals({filter:{destinations : $state.params.id,publish: true},sort:{created_by: -1},limit: 3},function(err,result){
		if(err){console.log("getAllAttractions",err);}
		console.log(result);
		$scope.journals = result;
	})

	$scope.bookmark = function(jid){
		if($rootScope.loggedInUser){
			journals.bookmarkJournal(jid,function(err,result){
				if(err){return console.log(err);}
				if(result.nModified > 0){
					var user = localStorageService.get('yolo_itinerary_client');
					user.bookmark_journal ? user.bookmark_journal.push(jid) : user.bookmark_journal= [jid] ;		
					$rootScope.loggedInUser = user;
					localStorageService.set('yolo_itinerary_client',user);
				}
			})
		}else{
			dialogs.openErrorDialog("Please Login!")
		}
	}
}])