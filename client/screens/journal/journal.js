app.controller('singleJournalCtrl', ['$scope','$location','journals','users','tags','comments','$rootScope','dialogs','$state','localStorageService', function($scope,$location,journals,users,tags,comments,$rootScope,dialogs,$state,localStorageService){
var journalData; 
var totalcomment;
var data = {};
$scope.tag_name=[];
$scope.comment_user = [];
$scope.itineraryIndex = 1;
$scope.count = 1;

$scope.currentUrl = $location.absUrl();

	$scope.config = {
	'fields' : {},
	'filter':{journal: $state.params.id},
		'sort' :{'created_on' :-1},
		'skip' : 0,
        'limit' : 0,
	};

    $scope.journalConfig = {
    'fields' : {title: 1, image_url: 1},
    'filter':{publish : true},
        'sort' :{'views' :-1},
        'skip' : 0,
        'limit' : 0,
    };

  $scope.bookmark = function(jid){
    console.log('jid is ',jid);
    if($rootScope.loggedInUser){
      journals.bookmarkJournal(jid,function(err,result){
        if(err){return console.log(err);}
        if(result.nModified > 0){
          var user = localStorageService.get('yolo_itinerary_client');
          user.bookmark_journal ? user.bookmark_journal.push(jid) : user.bookmark_journal = [jid] ;    
          $rootScope.loggedInUser = user;
          localStorageService.set('yolo_itinerary_client',user);
        }
      })
    }else{
      dialogs.openErrorDialog("Please Login!")
    }
  }

$scope.user = $rootScope.loggedInUser;

$scope.whatsappShare = function() {
$(document).ready(function() {

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
 $(document).on("click", '.whatsapp', function() {
        if( isMobile.any() ) {

            var text = $(this).attr("data-text");
            var url = $(this).attr("data-link");
            var message = encodeURIComponent(text) + " - " + encodeURIComponent(url);
            var whatsapp_url = "whatsapp://send?text=" + message;
            window.location.href = whatsapp_url;
        } else {
            alert("Please share this article in mobile device");
        }

    });
});
}

$scope.loggedUser = localStorageService.get('yolo_itinerary_client');
console.log('the logged in user is',$scope.loggedUser);

$scope.viewItinerary = function(itinerary){
    console.log('the itinerary data is',itinerary);
    $state.go('index.ItineraryView', {'machine_name': itinerary.machine_name,'id': itinerary._id});
}

$scope.journaldata = function(){
    journals.getJournal($state.params.id,function(err,result){
        if(err){
            console.log('can not get id');
            dialogs.openErrorDialog('Sorry! there is no such journal that exists now');
            $state.go('index.home');
        }
        else{
            if(!result._id){
                $state.go('index');
            }
            else{            
            console.log('result is ',result);
            if(result.publish == false){

                if(!$scope.loggedUser || result.created_by._id != $scope.loggedUser._id){
                    if(!$scope.loggedUser.isadmin && !$scope.loggedUser.issuperadmin){
                    dialogs.openErrorDialog('sorry! This journal is not available');
                    $state.go('index.home');
                }
                }
            }

            $scope.journal = result;
            $scope.journal.shareImageUrl = $rootScope.base_path+ result.image_url;
            journalData = result;


    		comments.getallComments($scope.config,function(err,result){
    			if(err){$scope.err = err; console.log(err); return;}
    			$scope.cmmnts = result;
                $scope.publishedComment = _.findWhere($scope.cmmnts,{"publish":true})
                console.log('a is here',$scope.publishedComment);
    			console.log('result of comments is',result);
    		})
            $scope.allJournal();
        }
        }
    })
}

  $scope.dayend=function(attraction){
    if(attraction.start_time){
      var start_time = new Date(attraction.start_time).getTime();
      var stay_hours = new Date(attraction.stay_time).getHours();
      var stay_minutes =new Date(attraction.stay_time).getMinutes();
      stay_minutes = stay_hours * 60 + stay_minutes;
      stay_minutes = stay_minutes * 60 *1000;
      return new Date(start_time + stay_minutes);
    }
  }


$scope.journaldata();

    $scope.showItinerary = function(number){
        $scope.itineraryIndex = number;
        $scope.count = number;
    }

    $scope.submitcomment = function(){
    	data.description = $scope.newcomment;
    	data.journal = $state.params.id;
        //console.log('data is',data);
    	comments.createComment(data,function(err,result){
    		if(err){$scope.err = err; console.log(err); return;}
    		//console.log('new comment updated');
    		$scope.journaldata();
            dialogs.openMessageDialog("Thank You! Your Comment has been sent for approval");
    		$scope.newcomment ="";

    	})
    }

$scope.like = function(){
    var data = {};
    data.likeBy = $rootScope.loggedInUser._id;
    journals.likejournal($state.params.id,data,function(err,result){
        if(err) {throw err;}
        //console.log('result',result);
        $scope.journal.likeBy = result.likeBy;

    })
}


$scope.editJournal = function(){
    console.log('hello journal edit');
    $state.go('index.editJournal', {'id': $state.params.id});
}


$scope.allJournal = function(){
    $scope.journalConfig.filter.destinations = $scope.journal.destinations;
    $scope.journalConfig.filter._id = {$ne:$scope.journal._id};
    // $scope.config.filter.category = $scope.attractionCategory;
    journals.getalljournals($scope.journalConfig,function(err,result){
        if(err) { dialogs.openErrorDialog(err);
            return
        }
        else{
            console.log('result of journalData',result);
            $scope.allJournals = result;
            }
    })
}


}])