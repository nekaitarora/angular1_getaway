app.controller('createJournalCtrl', ['$scope','$rootScope','destinations','journals','utils','tags','$state','dialogs','fileupload','itinerarys','localStorageService', function($scope,$rootScope,destinations,journals,utils,tags,$state,dialogs,fileupload,itinerarys,localStorageService){
	$scope.journal ={};
	$scope.journal.itineraries = [];
	$scope.publish = [true,false];
// var taghere;
	$scope.config = {
	'fields' : {'name' : 1},
	'filter':{},
	'sort' :{'name' :1},
	};

	var user = localStorageService.get('yolo_itinerary_client'); 

	$scope.itineraryConfig = {
	'fields' : {title: 1,machine_name: 1},
	'filter':{"created_by": user._id,"isdeleted": false},
	'sort' :{},
	};	

	!$rootScope.editProfileTab ? $rootScope.editProfileTab = "journal" : null;
	$scope.switchTab = function(name){
		$rootScope.editProfileTab = name;
	}

    $scope.languages = {"zn":"Chinese","en":"English"};

	$scope.language_change = function(){
		$scope.journal.language = $scope.languages[$scope.journal.language_code];
	}

	destinations.getAllDestinations({},function(err,result){
		if(err){console.log("getAllDestinations",err);}
		$scope.destinations = result;
	})

	tags.getallTags($scope.config, function(err,result){
		if(err){console.log("getalltags",err);}
		$scope.tag = result;
		console.log('result of tags is',$scope.tag);

		return;
	})
	
	itinerarys.getAllItineraries($scope.itineraryConfig, function(err,result){
		if(err){console.log("getItineraries",err);}
		$scope.itineraries = result;
		console.log('result of itinerary is',$scope.itineraries,user);

		return;
	})


	$scope.getTags = function(query){
     var regex = new RegExp(query, 'gi');
     return _.filter($scope.tag, function(obj){ return obj.name.match(regex);});
    }

	$scope.getItineraries = function(query){
     var regex = new RegExp(query, 'gi');
     return _.filter($scope.itineraries, function(obj){ return obj.machine_name.match(regex);});
    }    

	$scope.cancel = function(){
		$state.go('index.profile');
	}

	$scope.createJournal = function(){	
		if($scope.journal.itinerary){
		for(var i=0;i < $scope.journal.itinerary.length ; i++){
			$scope.journal.itineraries.push($scope.journal.itinerary[i]._id);
		}
	}
		if($scope.journalForm.$valid)
		{
			$scope.error = false;
			fileupload.uploadfile($scope.temp_file,'journals',function(err,result){
				if(err){ console.log("file not uploaded",err);}
				else
				{
					$scope.journal.image_url = result;
					console.log('itinerary is',$scope.journal.itineraries,$scope.journal.itinerary);
					journals.createJournal($scope.journal ,function(err,result){
						if(err)
						{
							if(err.message)
							{
								return $scope.error = err.message;
							}
							else
							{
								return dialogs.openErrorDialog(err);
							}
						}
						else
						{
							dialogs.openMessageDialog("Thank You! Your Journal has been sent to the admin for verification. Your journal will be live immediately after the admin approves it :) ");
							$state.go("index.profile");
						}
					})
				}
			});
		}
		else
		{
			$scope.submitted = true;
		}
	}

}]);