app.controller('profileCtrl', ['$scope','users','$rootScope','localStorageService','dialogs','$state','journals','attractions','itinerarys', function($scope,users,$rootScope,localStorageService,dialogs,$state,journals,attractions,itinerarys){
	$scope.user = localStorageService.get('yolo_itinerary_client');
	$scope.tabs = {};
	$scope.tabs.journals = 'created';
	$scope.tabs.attractions = 'created';
	$scope.tabs.itineraries = 'created';

	var user = localStorageService.get('yolo_itinerary_client')
	$scope.changeState = function(name){
		$rootScope.editProfileTab = name;
	}

	$scope.changeType = function(type,option){
		$scope.tabs[type] = option;

		if($scope.journals[$scope.tabs.journals].data.length == 0){
			$scope.getJournals();
		}
		if($scope.journals[$scope.tabs.journals].count == 0){
			$scope.setJournalCount();
		}

		if($scope.attractions[$scope.tabs.attractions].data.length == 0){
			$scope.getAttractions();
		}
		if($scope.attractions[$scope.tabs.attractions].count == 0){
			$scope.setAttractionCount();
		}

		if($scope.itineraries[$scope.tabs.itineraries].data.length == 0){
			$scope.getItineraries();
		}
		if($scope.itineraries[$scope.tabs.itineraries].count == 0){
			$scope.setItinerariesCount();
		}

	}	

$scope.loggedUser = localStorageService.get('yolo_itinerary_client');

	$scope.journals={};
	$scope.journals.created = {limit:3,page : 1 , skip:0,data:[],count:0 ,filter : {created_by : user._id,publish: true } ,sort:{created_on : -1}};
	$scope.journals.draft = {limit:3,page : 1 , skip:0,data:[],count:0  ,filter : {created_by : user._id,publish : false},sort:{created_on : -1}};
	$scope.journals.bookmarked = {limit:3,page : 1 , skip:0,data:[],count:0  ,filter : {_id: {$in : user.bookmark_journal } } ,sort:{created_on : -1} };

	$scope.nextJournals = function(){
		$scope.journals[$scope.tabs.journals].page++;
		$scope.journals[$scope.tabs.journals].skip = 3* ($scope.journals[$scope.tabs.journals].page - 1 );				
		$scope.getJournals();
	}
	$scope.prevJournals = function(){
		$scope.journals[$scope.tabs.journals].page--;
		$scope.journals[$scope.tabs.journals].skip = 3* ($scope.journals[$scope.tabs.journals].page - 1 );				
		$scope.getJournals();
	}

	$scope.getJournals = function() {
		delete $scope.journals[$scope.tabs.journals].data ;
		journals.getalljournals($scope.journals[$scope.tabs.journals],function(err,result){
			if(err){return console.log(err);}
			$scope.journals[$scope.tabs.journals].data = result;
		})
	}
	$scope.setJournalCount = function() {
		delete $scope.journals[$scope.tabs.journals].data;
		journals.getalljournalscount($scope.journals[$scope.tabs.journals],function(err,result){
			if(err){return console.log(err);}
			$scope.journals[$scope.tabs.journals].count = result.count;
		})
		
	}
	$scope.getJournals();
	$scope.setJournalCount();



	$scope.attractions={};
	$scope.attractions.created = {limit:3,page : 1 , skip:0,data:[] ,filter : {created_by : user._id,published: true } ,sort:{created_on : -1}};
	$scope.attractions.draft = {limit:3,page : 1 , skip:0,data:[] ,filter : {created_by : user._id,published : false},sort:{created_on : -1}};
	$scope.attractions.bookmarked = {limit:3,page : 1 , skip:0,data:[] ,filter : {_id: {$in : user.bookmark_attraction } } ,sort:{created_on : -1} };


	$scope.nextAttractions = function(){
		$scope.attractions[$scope.tabs.attractions].page++;
		$scope.attractions[$scope.tabs.attractions].skip = 3* ($scope.attractions[$scope.tabs.attractions].page - 1 );				
		$scope.getAttractions();
	}
	$scope.prevAttractions = function(){
		$scope.attractions[$scope.tabs.attractions].page--;
		$scope.attractions[$scope.tabs.attractions].skip = 3* ($scope.attractions[$scope.tabs.attractions].page - 1 );				
		$scope.getAttractions();
	}


	$scope.setAttractionCount = function() {
		delete $scope.attractions[$scope.tabs.attractions].data;

		attractions.getAttractionsCount($scope.attractions[$scope.tabs.attractions],function(err,result){
			if(err){return console.log(err);}
			$scope.attractions[$scope.tabs.attractions].count = result.count;
		})
	}	

	$scope.getAttractions = function() {
		delete $scope.attractions[$scope.tabs.attractions].data ;
		attractions.getAllAttractions($scope.attractions[$scope.tabs.attractions],function(err,result){
			if(err){return console.log(err);}
			$scope.attractions[$scope.tabs.attractions].data = result;
		})
	}

	$scope.getAttractions();
	$scope.setAttractionCount();





	$scope.itineraries={};
	$scope.itineraries.created = {limit:5,page : 1 , skip:0,data:[] ,filter : {created_by : user._id,published: true ,isdeleted: false} ,sort:{created_on : -1}};
	$scope.itineraries.draft = {limit:5,page : 1 , skip:0,data:[] ,filter : {created_by : user._id,published : false,isdeleted: false},sort:{created_on : -1}};
	$scope.itineraries.bookmarked = {limit:5,page : 1 , skip:0,data:[] ,filter : {_id: {$in : user.bookmark_itinerary } ,isdeleted: false} ,sort:{created_on : -1} };


	$scope.nextItineraries = function(){
		$scope.itineraries[$scope.tabs.itineraries].page++;
		$scope.itineraries[$scope.tabs.itineraries].skip = 5* ($scope.itineraries[$scope.tabs.itineraries].page - 1 );				
		$scope.getItineraries();
	}
	$scope.prevItineraries = function(){
		$scope.itineraries[$scope.tabs.itineraries].page--;
		$scope.itineraries[$scope.tabs.itineraries].skip = 5* ($scope.itineraries[$scope.tabs.itineraries].page - 1 );				
		$scope.getItineraries();
	}


	$scope.setItinerariesCount = function() {
		delete $scope.itineraries[$scope.tabs.itineraries].data;

		itinerarys.getAllItinerariesCount($scope.itineraries[$scope.tabs.itineraries],function(err,result){
			if(err){return console.log(err);}
			$scope.itineraries[$scope.tabs.itineraries].count = result.count;
		})
	}	

	$scope.getItineraries = function() {
		delete $scope.itineraries[$scope.tabs.itineraries].data;
		itinerarys.getAllItineraries($scope.itineraries[$scope.tabs.itineraries],function(err,result){
			if(err){return console.log(err);}
			$scope.itineraries[$scope.tabs.itineraries].data = result;
		})

	}

$scope.cancelBookmarkItinerary = function(id){
    console.log('user here is',$scope.loggedUser);
	var index = $scope.loggedUser.bookmark_itinerary.indexOf(id);
    console.log('id of itinerary is',id,index);
    itinerarys.bookmarkItinerary(id,function(err,result){
    	if(err){return console.log(err);}
    	console.log('itinerary bookmark');
    	if(result.nModified > 0){
          $scope.user.bookmark_itinerary ? user.bookmark_itinerary.splice(index,1) : "" ;    
          $rootScope.loggedInUser = user;
          localStorageService.set('yolo_itinerary_client',$scope.user);

          $scope.tabs.itineraries = 'bookmarked';
          $scope.getItineraries();
        }
    })
}

$scope.cancelBookmarkAttraction = function(id){
    console.log('user here is',$scope.loggedUser);
	var index = $scope.loggedUser.bookmark_attraction.indexOf(id);
    console.log('id of itinerary is',id,index);
    attractions.bookmarkAttraction(id,function(err,result){
    	if(err){return console.log(err);}
    	console.log('attraction bookmark');
    	if(result.nModified > 0){
          $scope.user.bookmark_attraction ? user.bookmark_attraction.splice(index,1) : "" ;    
          $rootScope.loggedInUser = user;
          localStorageService.set('yolo_itinerary_client',$scope.user);

          $scope.tabs.attractions = 'bookmarked';
          $scope.getAttractions();
        }
    })
}

$scope.cancelBookmarkJournal = function(id){
    console.log('user here is',$scope.loggedUser);
	var index = $scope.loggedUser.bookmark_journal.indexOf(id);
    console.log('id of itinerary is',id,index);
    journals.bookmarkJournal(id,function(err,result){
    	if(err){return console.log(err);}
    	console.log('journal bookmark');
    	if(result.nModified > 0){
          $scope.user.bookmark_journal ? user.bookmark_journal.splice(index,1) : "" ;    
          $rootScope.loggedInUser = user;
          localStorageService.set('yolo_itinerary_client',$scope.user);

          $scope.tabs.journals = 'bookmarked';
          $scope.getJournals();
        }
    })
}


	$scope.getItineraries();
	$scope.setItinerariesCount();
}])