app.controller('profileEditCtrl', ['$scope','users','$rootScope','localStorageService','dialogs','$state','fileupload','Upload', function($scope,users,$rootScope,localStorageService,dialogs,$state,fileupload,Upload){
	//used because of direct access after login fb
	!$rootScope.editProfileTab ? $rootScope.editProfileTab = "profile" : null;
	users.isloginwithcb(function(err,result){
		if(result){
			$scope.user = localStorageService.get('yolo_itinerary_client');
			$scope.user.profile_fields.ph_no = Number($scope.user.profile_fields.ph_no); 
			delete $scope.user.local.password;
		}else{
			$state.go('index.login');
		}
	});

        $scope.TimelineCropper = {};
        $scope.TimelineCropper.sourceImage = null;
        $scope.TimelineCropper.croppedImage   = null;


        $scope.ImageCropper = {};
        $scope.ImageCropper.sourceImage = null;
        $scope.ImageCropper.croppedImage   = null;



	$scope.cancel = function(){
		$scope.user = localStorageService.get('yolo_itinerary_client');
		$scope.user.profile_fields.ph_no = Number($scope.user.profile_fields.ph_no);
		delete $scope.user.local.password;
	}
	$scope.switchTab = function(name){
		$rootScope.editProfileTab = name;
	}
	$scope.updateUser = function(){
		if($scope.profileForm.$valid){
			if($scope.user.local.password || $scope.user.local.confirmPassword){
				if($scope.user.local.password !== $scope.user.local.confirmPassword){
					$scope.error = "Password Not match";
					return;
				}			
			}
			saveUser();
		}else{
			console.log($scope.profileForm);
			$scope.error = "Form Validation Failed";
		}
	}

	$scope.saveImage = function(){
		if($scope.ImageCropper.croppedImage){
			$scope.user.image = $scope.ImageCropper.croppedImage;
			saveUser();
		}else{
			$scope.imageError = "Please Select Image"
		}
	}

	$scope.saveTimelineImage = function(){
		if($scope.TimelineCropper.croppedImage){
			$scope.user.timeline_image = $scope.TimelineCropper.croppedImage;
			saveUser();
		}else{
			$scope.imageTimelineError = "Please Select Image"
		}
	}
	$scope.changePassword = function(){
		if(!$scope.user.new_password || !$scope.user.confirm_new_password || !$scope.user.old_password){
			$scope.passwordForm.new_password.$touched = true;
			$scope.passwordForm.confirm_new_password.$touched = true;
			$scope.passwordForm.old_password.$touched = true;
			return;
		}

		if($scope.user.new_password == $scope.user.confirm_new_password){
			users.udpatePassword($scope.user,function(err,result){
				if(err){$scope.password_error = err;console.log(err);return}
				else{
					dialogs.openMessageDialog("Password Updated");
					$state.go('index.profile');
				}

			});
		}else{
			$scope.password_error = 'Password doesn\'t match ';
		}
	}

	function saveUser(){
		users.updateuser($scope.user._id,$scope.user,function(err,result){
			if(err){
				if(err.message){
					$scope.error = err.message 
				}else{
					dialogs.openErrorDialog(err);
				}
				return;
			}
			else{
				localStorageService.set('yolo_itinerary_client',$scope.user);
				dialogs.openMessageDialog("<p>Your account information is updated..</p>");
				users.islogin();
				$state.go('index.profile');
			}
		})
	}
}])