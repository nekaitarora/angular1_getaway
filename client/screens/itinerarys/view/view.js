app.controller('itinerarysViewCtrl', ['$scope','$rootScope','$state','itinerarys','destinations','attractions','categories','dialogs','$filter','$sce','$timeout','$location','ngDialog','localStorageService', function($scope,$rootScope,$state,itinerarys,destinations,attractions,categories,dialogs,$filter,$sce,$timeout,$location,ngDialog,localStorageService){
  //get current url for sharing   
  $scope.currentUrl = $location.absUrl();
  
  //slider work of days
  $scope.slideron = 1;

//for showing cities after some time
  $scope.ctrl = {};
  $timeout(function() {
    $scope.ctrl.ready = true;
  }, 1000);
  
  $scope.mapday = "1";

//move days slider using anguular js
  $scope.move = function(move){
    if($scope.slideron > 1 && move == "-1"){
      $scope.slideron = $scope.slideron+ Number(move);
    }
    if($scope.slideron < $scope.total_days && move == "+1"){
      $scope.slideron = $scope.slideron+ Number(move);
    }
  }
//get attribute of day like start,end city and date
$scope.getDaysAttribute = function(){
  var days_dates = [];
  
  for(i=0;i<$scope.itinerary.trip_to.length;i++){
    var start_location = "";
    var start_date = new Date($scope.itinerary.trip_to[i].start_date);
    var end_date = new Date($scope.itinerary.trip_to[i].end_date);
    
    //if first then set end location to start_city
    if(i==0){
      start_location = $scope.itinerary.trip_from.city;  
    }else{
      start_location = $scope.itinerary.trip_to[i-1].destination.city
    }
    while(start_date.getDate() != end_date.getDate()  ){
      var new_json = {};
      new_json.date = angular.copy(start_date);
      new_json.start_location = start_location
      new_json.end_location = $scope.itinerary.trip_to[i].destination.city;
      days_dates.push(new_json);
      start_date.setDate(start_date.getDate() + 1);
    }
  }
  var last_day = angular.copy(days_dates[days_dates.length-1]);
  var last_date = new Date(last_day.date);
  last_day.date = last_date.setDate(last_date.getDate() + 1);
  days_dates.push(last_day);
  $scope.days_dates = days_dates;
} 

//close dialog on fb sharing
  $scope.callback = function(response){
    ngDialog.closeAll(); 
  }

//show map of day
  $scope.showMap = function(){
    var map_addr = "http://maps.google.com/maps?saddr=";
    if($scope.itinerary.days[$scope.mapday]){
      $scope.itinerary.days[$scope.mapday].forEach(function(day,index){
        if(index == 0){
          map_addr = map_addr + day.address;                  
        }else if(index == 1 ){
          map_addr = map_addr+ "&daddr=" + day.address;                   
        }else{
          map_addr = map_addr + " to:"  + day.address;                    
        }
      })
      map_addr = map_addr+ "&output=embed";
      $scope.mapSrc = map_addr;
      $scope.showingMap = true;
    }
  }

//share 
  $scope.share = function(){
    dialogs.openShareDialog($scope);
  }
//save as draft
  $scope.saveMyDraft = function(){
    itinerarys.saveMyDraft($state.params.id,$state.params.machine_name,function(err,result){
      if(err){return console.log("err",err);}
      $state.go("index.editItinerary",{id:result._id,machine_name:result.machine_name});
    });
  }

//hide map
  $scope.hideMap =function(){
    $scope.showingMap = false;
  }

//get itinerary from machine_name and id
  itinerarys.getItinerary($state.params.machine_name,$state.params.id,function(err,result){
    var user = localStorageService.get('yolo_itinerary_client');
    console.log('result of this itinerary is',result);
    if(err) {console.log('err is',err);}
    if(result == "" || result.isdeleted){
      if(user && (user.isadmin || user.issuperadmin)){
        dialogs.openErrorDialog("This Itinerary is deleted by the user");  
        // return;
      }
      else{
      console.log("err is getting itinerary",err);
      dialogs.openErrorDialog("Itinerary Not Found Or Deleted")
      $state.go("index.home");
      return;
    }
    }

    $scope.itinerary = result;
    $scope.itinerary_copy = angular.copy(result);
    $scope.getDaysAttribute();

    //create days json 
    var diff = new Date($scope.itinerary.end_date).getTime() - new Date($scope.itinerary.start_date).getTime() ;
    var days = diff/(24*60*60*1000);
    days =days+1;
    !$scope.itinerary.days ? $scope.itinerary.days = {} : null;
    for(i=0;i<days;i++){
      $scope.itinerary.days[i+1] ? $scope.itinerary.days[i+1]=$scope.itinerary.days[i+1] : $scope.itinerary.days[i+1] = [];
    }
    if(Object.keys($scope.itinerary.days).length > days){
      for(i = days+1; i<= Object.keys($scope.itinerary.days).length+1 ;i++){
        delete $scope.itinerary.days[i];
      }
    }
    $scope.total_days = Object.keys($scope.itinerary.days).length;
  })

  $scope.dayend=function(attraction){
    if(attraction.start_time){
      var start_time = new Date(attraction.start_time).getTime();
      var stay_hours = new Date(attraction.stay_time).getHours();
      var stay_minutes =new Date(attraction.stay_time).getMinutes();
      stay_minutes = stay_hours * 60 + stay_minutes;
      stay_minutes = stay_minutes * 60 *1000;
      return new Date(start_time + stay_minutes);
    }
  }

//bookmark itinerary
  $scope.bookmark = function(iid){
    if($rootScope.loggedInUser){
      itinerarys.bookmarkItinerary(iid,function(err,result){
        if(err){return console.log(err);}
        if(result.nModified > 0){
          var user = localStorageService.get('yolo_itinerary_client');
          user.bookmark_itinerary ? user.bookmark_itinerary.push(iid) : user.bookmark_itinerary= [iid] ;    
          $rootScope.loggedInUser = user;
          localStorageService.set('yolo_itinerary_client',user);
        }
      })
    }else{
      dialogs.openErrorDialog("Please Login!")
    }
  }

  $scope.deleteItinerary = function(){
        dialogs.openConfirmDialog("Delete Itinerary", "<div>Are you sure to delete <b>{{$scope.itinerary.title}}</b></div>",function(yes,no){
        if(yes){
            itinerarys.markdeleted($state.params.id,$state.params.machine_name,function(err,result){
              if(err){return console.log(err);}
              if(result.isdeleted){
                dialogs.openMessageDialog("deleted and move");
              }
            })
        }
        })
  }
}]);