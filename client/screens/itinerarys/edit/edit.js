app.controller('itinerarysEditCtrl', ['$scope','$rootScope','$state','itinerarys','destinations','attractions','categories','dialogs','$filter','$sce','$timeout','$location','ngDialog','dataService','$window','localStorageService', function($scope,$rootScope,$state,itinerarys,destinations,attractions,categories,dialogs,$filter,$sce,$timeout,$location,ngDialog,dataService,$window,localStorageService){
    //for time field to show am and pm
   $scope.currentUrl = $location.absUrl();

   $scope.currentUser = localStorageService.get('yolo_itinerary_client');

   $scope.ctrl = {};
   $timeout(function() {
        $scope.ctrl.ready = true;
    }, 1000);

	$scope.hstep = 1;
	$scope.mstep = 15;
	$scope.mytime = new Date();
	$scope.updating_itinerary_days =false;
	$scope.showSidebar = false;
	$scope.searchCategory = 'search';
	$scope.mapObj ={};
	$scope.mapObj.mapday = "1";
  	$scope.showingMap = false;
	$scope.destinations = []; 
	$scope.dragging_from = null;
    $scope.destinationsEditMode =false;
	$scope.destination_slider = {};
	$scope.destination_slider.newDestination = {};
    
  $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  }

$scope.slideron = 1;



  // Calender default required functions //
  function disabled(data) {
    return false;
  }
  $scope.dateOptions = {
    showWeeks: false,
    dateDisabled: disabled,
    formatYear: 'yy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1

  };

  $scope.setDate = function(year, month, day) {
    $scope.dt = new Date(year, month, day);
  };

  $scope.popup = {};
  /*
   * Managing Multiple Dates POPUPS
   */
  $scope.open = function(index, num) {
    $scope.popup[index] = [];
    $scope.popup[index][num] = true;
  }




  /*
   * Call on any date change
   */
  $scope.onDateChange = function(){
  	console.log("itinerary_trip_to_copy",$scope.itinerary_trip_to_copy)
    if($scope.itinerary_trip_to_copy && $scope.itinerary_trip_to_copy.length > 0){
      $scope.itinerary_trip_to_copy.forEach(function(city,index){
        //if first set city start date same as start date of itinerary and vice versa
        if(index == 0 ){
          city.start_date = $scope.itinerary.start_date; 
        }else{
        //if no start date get from previous one
          city.start_date = new Date($scope.itinerary_trip_to_copy[index-1].end_date).setDate(new Date($scope.itinerary_trip_to_copy[index-1].end_date).getDate() + 1);
        }
        city.start_date = new Date(city.start_date);

        //for last set end date 
        if(index == $scope.itinerary_trip_to_copy.length -1 ){
          !city.end_date  ? city.end_date = $scope.itinerary.end_date : $scope.itinerary.end_date = city.end_date; 
        }

        if(!city.end_date){
          city.end_date = city.start_date; 
        }

        //if start date > end date then reset it
        if(new Date(city.start_date).getTime() > new Date(city.end_date).getTime()){
          city.end_date =  new Date(city.start_date);
        }
      });
    }
  }

$scope.optimize = function(){


	var url = "https://maps.googleapis.com/maps/api/directions/json?origin="+$scope.days_dates[$scope.mapObj.mapday -1].end_location+"&destination="+$scope.days_dates[$scope.mapObj.mapday -1].end_location+"&waypoints=optimize:true|";
	var ids = [];
	var double_att = false;
	$scope.itinerary.days[$scope.mapObj.mapday].forEach(function(attraction){
		if(ids.indexOf(attraction.id) > -1){double_att =true;
		}else{
			ids.push(attraction.id);
		}
		
		url += attraction.latitude +","+ attraction.longitude+"|"
	})

	if(double_att){
		dialogs.openErrorDialog("You can't Optimize Path For Day "+$scope.mapObj.mapday);
		return;
	}else{
		url = url.substring(0, url.length - 1);
		url = url+ "&key=AIzaSyCTEszmb9WWtmWpC4LAw42GSkarHcQiTTM";

		dataService.post("/api/itinerarys/optimizepath",{url : url},function(err,result){

			dataService.post("/api/itinerarys/optimizepath",{url : url},function(err,result){
				var testday = [];
				result.routes[0].waypoint_order.forEach(function(position){
					delete $scope.itinerary.days[$scope.mapObj.mapday][position].start_time;

					delete $scope.itinerary.days[$scope.mapObj.mapday][position].next_distance;
					delete $scope.itinerary.days[$scope.mapObj.mapday][position].next_distance_url;
					delete $scope.itinerary.days[$scope.mapObj.mapday][position].next_duration;
					delete $scope.itinerary.days[$scope.mapObj.mapday][position].next_travel_mode;

					testday.push($scope.itinerary.days[$scope.mapObj.mapday][position]);
				})
				$scope.itinerary.days[$scope.mapObj.mapday] = testday;
				$scope.calulateTime($scope.mapObj.mapday ,true);
			})
		})
	}

}

$scope.getDaysAttribute = function(){
	var days_dates = [];
	
	for(i=0;i<$scope.itinerary.trip_to.length;i++){
		var start_location = "";
		var start_date = new Date($scope.itinerary.trip_to[i].start_date);
		var end_date = new Date($scope.itinerary.trip_to[i].end_date);
		end_date.setDate(end_date.getDate()+1);
	
		//if first then set end location to start_city
		if(i==0){
			start_location = $scope.itinerary.trip_from.city;  
		}else{
			start_location = $scope.itinerary.trip_to[i-1].destination.city;
		}

		while(start_date.getDate() != end_date.getDate()){
			var new_json = {};
			new_json.date = angular.copy(start_date);
			new_json.start_location = start_location
			new_json.end_location = $scope.itinerary.trip_to[i].destination.city;
			days_dates.push(new_json);

			start_date.setDate(start_date.getDate() + 1);
		}
	}
		// var last_day = angular.copy(days_dates[days_dates.length-1]);
		// var last_date = new Date(last_day.date);
		// last_day.date = last_date.setDate(last_date.getDate() + 1);
		// days_dates.push(last_day);
		// console.log(days_dates);
		console.log('day dates are as....',days_dates);
		$scope.days_dates = days_dates;
} 


  $scope.move = function(move){
		if($scope.slideron > 1 && move == "-1"){
			$scope.slideron = $scope.slideron+ Number(move);
		}
		if($scope.slideron < $scope.total_days && move == "+1"){
			$scope.slideron = $scope.slideron+ Number(move);
		}
  }

  $scope.callback = function(response){
    ngDialog.closeAll(); 
  }
  $scope.toggleSidebar = function(){
  	$scope.showSidebar = !$scope.showSidebar; 
  }
  $scope.searchCategorySwitch = function(show){
  	$scope.searchCategory = show;
  }
  $scope.toggleTravelMode = function(mode){
  	if($scope.currentMode == mode){
  		$scope.currentMode = null
  	}else{
  		$scope.currentMode = mode;
  	}
  }
	//get itinerary from machine_name and id
	itinerarys.getItinerary($state.params.machine_name,$state.params.id,function(err,result){
		console.log('the details of itinerary is as',result);
	    if(err || result == "" || ($scope.currentUser && !result.anonymous) ){
	    	if($scope.currentUser._id !== result.created_by){
	      	console.log("err is getting itinerary",err);
	      	dialogs.openErrorDialog("Itinerary Not Found Or Deleted")
	      	$state.go("index.home");
	      	return;
	  		}
	    }
  		if(result.isdeleted){
      		dialogs.openErrorDialog("Itinerary Not Found Or Deleted")
      		$state.go("index.home");
	  		return	  	
	  	}	    
	    if(!result.anonymous){
	    	if(!$scope.currentUser){
	      		dialogs.openErrorDialog("Itinerary Not Found Or Deleted")
	      		$state.go("index.home");
	      		return;	    		
	    	}
	    }

	    	console.log(result);
	    if(result.published ){
	        $state.go("index.ItineraryView",{machine_name : result.machine_name ,id : result._id });
	    }

		$scope.itinerary = result;
	    $scope.cityStayPlanOld = $scope.cityStayPlan();

		$scope.itinerary_trip_to_copy = angular.copy(result.trip_to);
		
		$scope.itinerary_copy = angular.copy(result);
		$scope.getDaysAttribute();
		
		//get only destinations within itinerary instead of getting all destinations
		$scope.itinerary.trip_to.forEach(function(destination){
			$scope.destinations.push(destination.destination);
		})
		$scope.search.filter.city =  $scope.destinations[0]._id;
		$scope.searchAttractions();




		
		var diff = new Date($scope.itinerary.end_date).getTime() - new Date($scope.itinerary.start_date).getTime() ;
		var days = diff/(24*60*60*1000);
		days = days+1;
		
		!$scope.itinerary.days ? $scope.itinerary.days = {} : null;

		
		// var copy_days = angular.copy($scope.itinerary.days);
		// $scope.itinerary.days = {};

		// for(i=0;i<$scope.days_dates.length;i++){
		// 	var previous_location ="";
		// 	if($scope.days_dates[i-1].end_location){
		// 		previous_location = $scope.days_dates[i-1].end_location;
		// 	} 
		// 	if(copy_days[i+1]){
		// 		if(copy_days[i+1] && copy_days[i+1][0]){
		// 			if(copy_days[i+1][0].city.city.trim().toLowerCase() == $scope.days_dates[i].end_location.trim().toLowerCase()){
		// 				$scope.itinerary.days[i+1] =  copy_days[i+1];
		// 			}
		// 		}

		// 	}else{
		// 		$scope.itinerary.days[i+1]= [];				
		// 	} 
		// 		console.log($scope.days_dates[i].end_location);
		// }
		// for(i=0;i<days;i++){
		// 	$scope.itinerary.days[i+1] ? $scope.itinerary.days[i+1]=$scope.itinerary.days[i+1] : $scope.itinerary.days[i+1] = [];
		// }

		// if(Object.keys($scope.itinerary.days).length > days){
		// 	for(i = days+1; i<= Object.keys($scope.itinerary.days).length+1 ;i++){
		// 		delete $scope.itinerary.days[i];
		// 	}
		// }
		$scope.total_days = Object.keys($scope.itinerary.days).length;
		   //get all destinations for filters getting for itinerary directly
		destinations.getAllDestinations({},function(err,result){
			console.log("itinerary..................",$scope.itinerary);
			if(err){return console.log(err);}
			$scope.allDestinations = result;

		    if($scope.itinerary.trip_from){
			  $scope.allDestinationsData = _.filter($scope.allDestinations, function(destination){ return destination._id !== $scope.itinerary.trip_from._id ? true : false });
		    }


		})
	})


	$scope.stayDays = function(destination){
		var start_date =  new Date(destination.start_date);
		var end_date =  new Date(destination.end_date);
		var diff = end_date.getTime() -  start_date.getTime(); 
		return (diff /1000/3600/24)+1 + " Days"; 
	}

	$scope.showMap = function(){
		var map_addr = "http://maps.google.com/maps?saddr=";
		if($scope.itinerary.days[$scope.mapObj.mapday]){
			$scope.itinerary.days[$scope.mapObj.mapday].forEach(function(day,index){
				if(index == 0){
					map_addr = map_addr + day.address;					
				}else if(index == 1 ){
					map_addr = map_addr+ "&daddr=" + day.address;					
				}else{
					map_addr = map_addr + " to:"  + day.address;					
				}
			})
			map_addr = map_addr+ "&output=embed";
			$scope.mapSrc = map_addr;
			$scope.showingMap = true;
		}
	}
	$scope.hideMap =function(){
		$scope.showingMap = false;
	}
	//All Sidebar Work First


    //get all categories for filters
	categories.getAllCategories(function(err,result){
		if(err){return console.log(err);}
		$scope.categories = result;
	})

	$scope.search = {};
	$scope.search.filter = {published: true};
	$scope.search.limit = 10;

	$scope.searchAttractions= function(type){
		if(type == 'by-name'){
			 delete $scope.search.filter.category;
		}
		if(type =='by-category'){
			delete $scope.search.name;
		}
		$scope.search.filter.city == '' ? delete $scope.search.filter.city : null; 
		$scope.search.filter.category == '' ? delete $scope.search.filter.category : null; 
		$scope.search.name == '' ? delete $scope.search.name : null; 
		
		attractions.getAllAttractions($scope.search,function(err,result){
			if(err){return console.log(err);}
			$scope.attractions = result;
		})
	}

	//Itinierary
	//timepicker related functions
	$scope.showTimepicker = function(type,index,key){
		$scope.timepicker_view = type+index+key;
	}

	$scope.hideTimepicker =function(){
		$scope.timepicker_view = null;	
		$scope.itinerary = $scope.itinerary_copy;
	}
	//remove attraction
	$scope.removeAttraction = function(day,index){
		$scope.itinerary.days[day].splice(index,1);
		$scope.calulateTime(day);
	}

	//calculate time of day of attractions
	$scope.calulateTime = function(day,consDel){
		//remove 2 consicutive attractions if comes due to move
		// if(!consDel){
		// 	for(i=0;i<$scope.itinerary.days[day].length;i++){
		// 		if($scope.itinerary.days[day][i-1]){
		// 			if($scope.itinerary.days[day][i-1].id == $scope.itinerary.days[day][i].id){
		// 				$scope.itinerary.days[day].splice(i ,1 );
		// 			}
		// 		}
		// 	}

		// }
		$scope.updating_itinerary_days = true;

		var itinerary =  angular.copy($scope.itinerary);
		
		for(i=0;i<itinerary.days[day].length;i++){
			itinerary.days[day][i].start_time = $filter('date')(new Date(itinerary.days[day][i].start_time), 'HH:mm');
			itinerary.days[day][i].stay_time = $filter('date')(new Date(itinerary.days[day][i].stay_time), 'HH:mm');
		}
	
		itinerarys.getDayDistanceTime(itinerary.days[day],function(err,result){
			if(err){
				$scope.itinerary = angular.copy($scope.itinerary_copy);
				$scope.updating_itinerary_days = false;
				return dialogs.openErrorDialog(err.message); 
			}
				for(i=0;i<result.length;i++){
					var start_time = result[i].start_time.split(":");
					result[i].start_time = new Date("Sat May 15 2016 08:00:00")
					result[i].start_time.setHours(start_time[0]);
					result[i].start_time.setMinutes(start_time[1]);

					var stay_time = result[i].stay_time.split(":");
					console.log("stay time",stay_time);
					result[i].stay_time = new Date("Sat May 15 2016 08:00:00");
					result[i].stay_time.setHours(stay_time[0]);
					result[i].stay_time.setMinutes(stay_time[1]);
				}
				$scope.itinerary.days[day] = result;
				$scope.updating_itinerary_days = false;
				$scope.itinerary_copy = angular.copy($scope.itinerary);

			if($scope.dragging_from != null && $scope.dragging_from != day){
				$scope.calulateTime($scope.dragging_from);
				$scope.dragging_from = null;
			}
			console.log('itinerary',$scope.itinerary);
		})
	}


	//called on every drop
    $scope.dropCallback = function(event, index, item, external, type, allowedType,day) {
    	//for sidebar  no drop should work
    	if(day == 'search'){
    		return;
    	}

    	var id = item._id ? item._id : item.id;
		if($scope.itinerary.days[day][index] && $scope.itinerary.days[day][index].id == id || $scope.itinerary.days[day][index-1] && $scope.itinerary.days[day][index-1].id == id){
			return false;
		}
		
    	//popup duplicacy while moving
    	if(_.findWhere($scope.itinerary.days[day] ,{id:item._id})){
    		dialogs.openConfirmDialog("Stop Optimization","<p>Are you sure you want to stop optimization for Day "+day+" </p>",function(yes,no){
    			if(yes){
		    		var array = $scope.droppedItem(item,day,index);
    				$scope.itinerary.days[day].splice(index , 0 , array);
    				$scope.calulateTime(day);
    			}else{
    				return;
    			}
    		});
    	}else{
    		return $scope.droppedItem(item,day,index);
    	}


    };

$scope.droppedItem = function(item,day,index){
    	//return dulicate copying error 
    	var id = item._id ? item._id : item.id;


    	//remove duplicacy while moving
    	if(_.findWhere($scope.itinerary.days[day] ,{id:item.id})){
			var item_index = "";
				$scope.itinerary.days[day].forEach(function(attraction,index){
					if(attraction.id == item.id){
	    				item_index = index;
					}
				})
			$scope.itinerary.days[day].splice(item_index , 1);
    	}
	   	if($scope.days_dates[day-1].end_location.trim().toLowerCase() != item.city.city.trim().toLowerCase()){
    		return false;
    	}


//    	set image in image fields
		// item.image_url[0] && !$scope.itinerary.image ?  $scope.itinerary.image = item.image_url[0] :  null;

 		var newitem ={};
		newitem.name = item.name;
		newitem.id = item._id ? item._id : item.id;
		newitem.yelp_image_url = item.yelp_image_url;
		newitem.image_url = item.image_url ? item.image_url[0] : null;
		newitem.longitude = item.longitude;
		newitem.latitude = item.latitude;
		newitem.address = item.address;
		newitem.city = {image : item.city.image ,city :item.city.city,_id : item.city._id};
		newitem.type = "attraction";
		return newitem;
}
$scope.dragChange = function(type , key){
	if(type = 'start'){
		$scope.dragging_from = key;
	}
}
    //show the different modes and work on chnage 
    $scope.showDiffMode = function(key , index){
    	itinerarys.getAllModes($scope.itinerary.days[key][index],function(err,result){
    		if(err){return console.log("error in getting modes ",err );}
    		$scope.travelModes = result;

    		dialogs.openTravelModeDialog($scope,function(err,result){
    			if(err){return;}
				$scope.itinerary.days[key][index].next_travel_mode = result; 
    			$scope.calulateTime(key);
    		});
    	})
    }
    $scope.saveItinerary =function(mode,nomessage){
    	console.log("$save itinerary..................",$scope.itinerary);
    	if($scope.itinerary.isdeleted){
    		return;
    	}

    	if($scope.itinerary.anonymous && !$scope.currentUser){
        	itinerarys.updateItinerary($state.params.id,$scope.itinerary,function(err,result){});
			$window.location.href = "/#/login?id="+$scope.itinerary._id+"&machine_name="+$scope.itinerary.machine_name;
			return;
    	}
    
    	$scope.ishidden = true;
		$('slick').slick('unslick');

		if($scope.itinerary.days['1']){
			$scope.itinerary.days['1'][0] && $scope.itinerary.days['1'][0].image_url ? $scope.itinerary.image = $scope.itinerary.days['1'][0].image_url  :$scope.itinerary.image = $scope.itinerary.trip_to[0].destination.image;
		}else{
			$scope.itinerary.image = $scope.itinerary.trip_to[0].destination.image;	
		}


    	if(mode == 'draft'){
    		$scope.itinerary.published = false;
    		$scope.itinerary.anonymous = false;

    		itinerarys.updateItinerary($state.params.id,$scope.itinerary,function(err,result){
    			if(err){return console.log(err);}
    			$scope.itinerary  =result;
 				$scope.refreshCarousel();
    			if(!nomessage){
    				dialogs.openMessageDialog("Itinerary Saved in Draft Mode");
    			}
    		})
    	}

    	if(mode == 'finish'){
    		$scope.itinerary.published = true;
    		$scope.itinerary.anonymous = false;
    		itinerarys.updateItinerary($state.params.id,$scope.itinerary,function(err,result){
    			if(err){return console.log(err);}
    			$scope.itinerary  =result;
  				$scope.refreshCarousel();
    			dialogs.openShareDialog($scope);
    		})
    	}
    	if(mode == 'share'){
    			dialogs.openShareDialog($scope);
    	}

    	if(mode == 'delete'){
    		dialogs.openConfirmDialog("Delete Itinerary", "<div>Are you sure to delete <b>{{$scope.itinerary.title}}</b></div>",function(yes,no){
				if(yes){
		    		itinerarys.markdeleted($state.params.id,$state.params.machine_name,function(err,result){
		    			if(err){return console.log(err);}
		    			if(result.isdeleted){
		    				dialogs.openMessageDialog("deleted and move");
		    			}
		    		})
				}

    			
    		})
    	}
    }
	$scope.redirect = function(){
		$scope.edit = true;
		// $scope.saveItinerary('draft',true);
		// $state.go('index.editItinerarysDays',{machine_name : $state.params.machine_name , id : $state.params.id});
	}


	$scope.editDestinations = function(){
		if($scope.itinerary.published){
			$state.go("index.ItineraryView",{machine_name : $scope.itinerary.machine_name ,id : $scope.itinerary._id });
		}
		if($scope.destinationsEditMode){

			$scope.itinerary.trip_to = angular.copy($scope.itinerary_trip_to_copy); 
	
			if($scope.itinerary.trip_to.length > 0){
				$scope.itinerary.end_date = $scope.itinerary.trip_to[$scope.itinerary.trip_to.length -1].end_date;
			}else{
				$scope.itinerary.end_date = $scope.itinerary.start_date; 
			}

			//update title
			var diff = new Date($scope.itinerary.end_date).getTime() - new Date($scope.itinerary.start_date).getTime() ;
		    var days = diff/(24*60*60*1000);
		    days = days + 1;
		    console.log('days is .......',days);

		    //if End date is lesser than start date somehow , break it
		    if(diff < 0){
		      $scope.error = "End Date should after the Start Date";
		      return;
		    }
		    //Create machine name and title
		    var trip_to = "";
		    $scope.itinerary.trip_to.forEach(function(destination){
		      trip_to += destination.city ? destination.city.trim() : destination.destination.city.trim()  + ", ";
		    })
		    trip_to = trip_to.substring(0, trip_to.length - 2);

		    $scope.itinerary.title = days + " Day Trip to " +trip_to +" from "+$scope.itinerary.trip_from.city;
		    
			$scope.cityStayPlanNew = $scope.cityStayPlan();
			$scope.getDaysAttribute()
			$scope.updateDayPlan();	
		}

		// $scope.saveItinerary('draft',true);
	    $scope.onDateChange();
		$scope.destinationsEditMode = !$scope.destinationsEditMode;

	}

	/*
   * Get City Wise Stay Plan  in array
   */
  $scope.cityStayPlan = function(){
    var new_obj =[];
    console.log("$scope.itinerary.trip_to",$scope.itinerary.trip_to);
    $scope.itinerary.trip_to.forEach(function(destination){
      var start_date =  new Date(destination.start_date);
      var end_date =  new Date(destination.end_date);
      var diff = end_date.getTime() -  start_date.getTime(); 
      
      var temp_obj ={};
      temp_obj.city = destination.destination.city.trim().toLowerCase();
      temp_obj.days = (diff /1000/3600/24)+1 ; 
      new_obj.push(temp_obj);
    })
    return new_obj;
  }


  /*
   * Set Days JSON
   */
  $scope.updateDayPlan = function() {
    var itinerary_days = $scope.itinerary.days;
    $scope.itinerary.days = {};

    var total_import_count = 1;
    var total_export_count = 1;
    for (i = 0; i < $scope.cityStayPlanNew.length; i++) {
      if (itinerary_days && $scope.cityStayPlanOld && $scope.cityStayPlanNew && $scope.cityStayPlanNew[i] && $scope.cityStayPlanOld[i] && $scope.cityStayPlanNew[i].city == $scope.cityStayPlanOld[i].city) {
        if ($scope.cityStayPlanNew[i].days >= $scope.cityStayPlanOld[i].days) {
          for (var j = 1; j <= $scope.cityStayPlanNew[i].days; j++) {
            if ($scope.cityStayPlanOld[i] && j <= $scope.cityStayPlanOld[i].days) {
              $scope.itinerary.days[total_export_count] = itinerary_days[total_import_count] ? itinerary_days[total_import_count] : [];
              total_import_count = total_import_count + 1;
            }else {
              $scope.itinerary.days[total_export_count] = [];
            }
            total_export_count++;
          }
        } else {
             console.log("14"); 
          for (var j = 1; j <= $scope.cityStayPlanOld[i].days; j++) {
            if (j <= $scope.cityStayPlanNew[i].days) {
              $scope.itinerary.days[total_export_count] = itinerary_days[total_import_count] ? itinerary_days[total_import_count] : [];
              total_export_count++;
            }
            total_import_count = total_import_count + 1;
          }
        }

      } else {
        console.log("$scope.cityStayPlanNew",$scope.cityStayPlanNew);
        console.log("$scope.cityStayPlanNew",$scope.cityStayPlanNew[i]);
        for (var j = 1; j <= $scope.cityStayPlanNew[i].days; j++) {
          $scope.itinerary.days[total_export_count] = [];
          total_export_count++;
        }
      }
    }
    console.log("3");
  }

	$scope.removeCity = function(index){
		$('slick').slick('unslick');

		$scope.ishidden = true;
		$scope.itinerary_trip_to_copy.splice(index ,1);
		$scope.itinerary.trip_to  = $scope.itinerary_trip_to_copy;
		setTimeout(function(){ $scope.refreshCarousel(); }, 10);
		$scope.cityStayPlanNew = $scope.cityStayPlan();
		$scope.getDaysAttribute();
		$scope.updateDayPlan();	
		$scope.UpdateDestinations();

	}

  $scope.addNewDestinatton = function(){
    if($scope.destination_slider.newDestination){
			if($scope.itinerary.trip_to[$scope.itinerary.trip_to.length -1 ]){
				var start_date = new Date($scope.itinerary.trip_to[$scope.itinerary.trip_to.length -1 ].end_date);
			}else{
				var start_date = $scope.itinerary.start_date;
			}
			var end_date = new Date(start_date);
			end_date.setDate(end_date.getDate() + 1);

			$('slick').slick('unslick');
			$scope.ishidden = true;
			$scope.itinerary.trip_to.push({destination : $scope.destination_slider.newDestination ,start_date : start_date,end_date :end_date});
			$scope.itinerary_trip_to_copy = $scope.itinerary.trip_to;
			setTimeout(function(){ $scope.refreshCarousel(); }, 10);
			$scope.cityStayPlanNew = $scope.cityStayPlan();
			$scope.getDaysAttribute()
			$scope.updateDayPlan();	

			$scope.UpdateDestinations();
			$scope.searchAttractions();
		}

	}

	$scope.refreshCarousel = function(){
		$scope.ishidden =false;
		$('slick').slick({
		  infinite: false,
		  slidesToShow: 8,
		  slidesToScroll: 1
		});
	}
	$scope.UpdateDestinations = function(){
		$scope.destinations = [];
		//get only destinations within itinerary instead of getting all destinations
		$scope.itinerary.trip_to.forEach(function(destination){
			$scope.destinations.push(destination.destination);
		})
		if($scope.destinations[0]){
		$scope.search.filter.city = $scope.destinations[0]._id; 
	}
	}
    $scope.deleteItinerary = function(){
    	itinerarys.deleteItinerary($state.params.id,function(err,result){
    		if(err){return console.log("err",err);}
				dialogs.openMessageDialog("Itinerary Deleted")
    	})
    }
    $scope.dayend=function(attraction){
    	if(attraction.start_time){
    		var start_time = new Date(attraction.start_time).getTime();
			var stay_hours = new Date(attraction.stay_time).getHours();
			var stay_minutes =new Date(attraction.stay_time).getMinutes();
			stay_minutes = stay_hours * 60 + stay_minutes;
			stay_minutes = stay_minutes * 60 *1000;
			return new Date(start_time + stay_minutes);

    	}

    }

}]);