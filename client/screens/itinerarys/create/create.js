app.controller('itinerarysCreateCtrl', ['$scope','$rootScope','$state','itinerarys','destinations','attractions','categories','dialogs','utils', function($scope,$rootScope,$state,itinerarys,destinations,attractions,categories,dialogs,utils){

  // Calender default required functions //
  function disabled(data) {
    return false;
  }
  $scope.dateOptions = {
    showWeeks: false,
    dateDisabled: disabled,
    formatYear: 'yy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };

  $scope.setDate = function(year, month, day) {
    $scope.dt = new Date(year, month, day);
  };

  // Calender default required  functions end //

  //Decalair required variables //
  $scope.popup = {};
  $scope.addCity = {};
  $scope.editingMode = 'trip_from';
  $scope.itinerary ={};
  $scope.itinerary.is_return = "true";
  $scope.trip_to_destinations = [];

  // Decalair required variables end //

  // default required calling queries //
  destinations.getAllDestinations({},function(err,result){
	$scope.destinations = result;
    $scope.trip_to_destinations = angular.copy(result);
  })
  // default required calling queries //


// Custom Functions //
  /*
   * Remove City from Destinations
   */
  $scope.removeCity = function(index){
    $scope.itinerary.trip_to.splice(index,1);
    
    //if last city we removing then set length 0 otherwise autocomplete error 
	if($scope.itinerary.trip_to.length == 0){
	  $scope.addCity.new_city = [];
	}
  }
  
  /*
   *For Autocomplete Destinations Filtrations
   */
  $scope.getDestinationsData = function(query){
    var regex = new RegExp(query, 'gi');
    return _.filter($scope.trip_to_destinations, function(obj){ return obj.city.match(regex) && obj._id !== $scope.itinerary.trip_from._id;});
  }

  /*
   * For Hiding Autocomplete
   */
  $scope.hideTagInput = function(tag){
    $scope.addCity.new_city= tag;
    $scope.addTripToCity();
  }

  /*
   * Managing Multiple Dates POPUPS
   */
  $scope.open = function(index, num) {
    $scope.popup[index] = [];
    $scope.popup[index][num] = true;
  }

  /*
   * Start City Change
   */
  $scope.startCityChange =function(){
  	// remove selected value from destinations
    if($scope.itinerary.trip_from){
	  $scope.trip_to_destinations = _.filter($scope.destinations, function(destination){ return destination._id !== $scope.itinerary.trip_from._id ? true : false });
    }
  	// remove selected value from destinations if already added
    if($scope.itinerary.trip_from && $scope.itinerary.trip_to &&  $scope.itinerary.trip_to.length >0){
      $scope.itinerary.trip_to = _.filter($scope.itinerary.trip_to, function(destination){  return destination.destination._id !== $scope.itinerary.trip_from._id ? true : false });
    }
   	// if all destinations get deleted then set length 0 otherwise autocomplete error 
    if($scope.itinerary.trip_to.length == 0){
      $scope.addCity.new_city = [];
    }		
  }

  /*
   * Add Destination City In Trip and Set Its Dates
   */
  $scope.addTripToCity = function(){
    if($scope.addCity.new_city && $scope.addCity.new_city != null){
      var new_city = {};
      new_city.destination = $scope.addCity.new_city;

      if($scope.itinerary.trip_to && $scope.itinerary.trip_to[$scope.itinerary.trip_to.length-1]){
        new_city.start_date = new Date($scope.itinerary.trip_to[$scope.itinerary.trip_to.length-1].end_date); 
        new_city.start_date.setDate(new_city.start_date.getDate() + 1);
      }else{
        new_city.start_date =$scope.itinerary.start_date ; 
      }

      new_city.end_date =  new Date(new_city.start_date);
      new_city.end_date.setDate(new_city.end_date.getDate() + 1);
      !$scope.itinerary.trip_to ? $scope.itinerary.trip_to = []: null; 

      //update end date of itinerary 
      $scope.itinerary.end_date = new_city.end_date;
      $scope.itinerary.trip_to.push(new_city);
    }
  }

  /*
   * Call on any date change
   */
  $scope.onDateChange = function(){
    if($scope.itinerary.trip_to && $scope.itinerary.trip_to.length > 0){
      $scope.itinerary.trip_to.forEach(function(city,index){
        //if first set city start date same as start date of itinerary and vice versa
        if(index == 0 ){
          city.start_date = $scope.itinerary.start_date; 
        }else{
        //if no start date get from previous one
          city.start_date = new Date($scope.itinerary.trip_to[index-1].end_date).setDate(new Date($scope.itinerary.trip_to[index-1].end_date).getDate() + 1);
        }
        city.start_date = new Date(city.start_date);

        //for last set end date 
        if(index == $scope.itinerary.trip_to.length -1 ){
          !city.end_date  ? city.end_date = $scope.itinerary.end_date : $scope.itinerary.end_date = city.end_date; 
        }

        if(!city.end_date){
          city.end_date = city.start_date; 
        }

        //if start date > end date then reset it
        if(new Date(city.start_date).getTime() > new Date(city.end_date).getTime()){
          city.end_date =  new Date(city.start_date);
        }
      });
    }
  }


  /*
   * Get City Wise Stay Plan  in array
   */
  $scope.cityStayPlan = function(){
    var new_obj =[];
    $scope.itinerary.trip_to.forEach(function(destination){
      var start_date =  new Date(destination.start_date);
      var end_date =  new Date(destination.end_date);
      var diff = end_date.getTime() -  start_date.getTime(); 
      
      var temp_obj ={};
      temp_obj.city = destination.destination.city.trim().toLowerCase();
      temp_obj.days = (diff /1000/3600/24)+1 ; 
      new_obj.push(temp_obj);
    })
    return new_obj;
  }

  /*
   * Set Days JSON
   */
  $scope.updateDayPlan = function() {
    var itinerary_days = $scope.itinerary.days;
    $scope.itinerary.days = {};

    var total_import_count = 1;
    var total_export_count = 1;
    for (i = 0; i < $scope.cityStayPlanNew.length; i++) {
      if (itinerary_days && $scope.cityStayPlanOld && $scope.cityStayPlanNew && $scope.cityStayPlanNew[i] && $scope.cityStayPlanOld[i] && $scope.cityStayPlanNew[i].city == $scope.cityStayPlanOld[i].city) {
        if ($scope.cityStayPlanNew[i].days >= $scope.cityStayPlanOld[i].days) {
          for (var j = 1; j <= $scope.cityStayPlanNew[i].days; j++) {
                console.log("12");
            if ($scope.cityStayPlanOld[i] && j <= $scope.cityStayPlanOld[i].days) {
                  console.log("13");
              $scope.itinerary.days[total_export_count] = itinerary_days[total_import_count] ? itinerary_days[total_import_count] : [];
                  console.log("15");
              total_import_count = total_import_count + 1;
            }else {
              $scope.itinerary.days[total_export_count] = [];
            }
            total_export_count++;
          }
        } else {
             console.log("14"); 
          for (var j = 1; j <= $scope.cityStayPlanOld[i].days; j++) {
            if (j <= $scope.cityStayPlanNew[i].days) {
              $scope.itinerary.days[total_export_count] = itinerary_days[total_import_count] ? itinerary_days[total_import_count] : [];
              total_export_count++;
            }
            total_import_count = total_import_count + 1;
          }
        }

      } else {
        console.log("$scope.cityStayPlanNew",$scope.cityStayPlanNew);
        console.log("$scope.cityStayPlanNew",$scope.cityStayPlanNew[i]);
        for (var j = 1; j <= $scope.cityStayPlanNew[i].days; j++) {
          $scope.itinerary.days[total_export_count] = [];
          total_export_count++;
        }
      }
    }
    console.log("3");
  }


  /*
   * Call on LETMEPLAN Button
   */
  $scope.letMePlan = function(){
    $scope.itinerary.end_date = angular.copy($scope.itinerary.trip_to[$scope.itinerary.trip_to.length -1].end_date);
    var diff = new Date($scope.itinerary.end_date).getTime() - new Date($scope.itinerary.start_date).getTime() ;
    var days = diff/(24*60*60*1000);
    days = days + 1;

    //if End date is lesser than start date somehow , break it
    if(diff < 0){
      console.log("hye i am error");
      $scope.error = "End Date should after the Start Date";
      return;
    }
    //Create machine name and title
    var trip_to = "";
    $scope.itinerary.trip_to.forEach(function(destination){
      trip_to += destination.city ? destination.city.trim() : destination.destination.city.trim()  + ", ";
    })
    trip_to = trip_to.substring(0, trip_to.length - 2);

    $scope.itinerary.title = days + " Day Trip to " +trip_to +" from "+$scope.itinerary.trip_from.city;
    
    //create machine_name if not has already
    if(!$scope.itinerary.machine_name){
	 $scope.itinerary.machine_name = utils.create_machine_name($scope.itinerary.title);	
    }
 
    //get city wise days plan
    $scope.cityStayPlanNew = $scope.cityStayPlan();
    $scope.updateDayPlan();

    //finally go for creation
    itinerarys.createItinerary($scope.itinerary,function(err,result){
      if(err){return $scope.error = "Error in creation"; console.log("err",err);}
      $state.go("index.editItinerary",{machine_name:result.machine_name, id : result._id});
    });
  }

// Custom Functions End //

// Edit Work //
  if($state.params.machine_name && $state.params.id){
    itinerarys.getItinerary($state.params.machine_name,$state.params.id,function(err,result){
	  if(err){console.log(err);$state.go('index.createItineraryDays'); return;}
	  $scope.itinerary = result;
	  
	  //set itinerary middle dates when comes from home :)
	  $scope.onDateChange();
	  
	  $scope.itinerary.start_date = new Date(result.start_date);
  	  $scope.itinerary.trip_to[$scope.itinerary.trip_to.length -1].end_date = new Date(result.end_date);
	  
	  //this will set destinations etc
	  $scope.startCityChange();
	  
	  //save city wise days plan
	  $scope.cityStayPlanOld = $scope.cityStayPlan();
	})
  }else{
    $scope.itinerary = {};
    $scope.itinerary.trip_to = [];
  }
// Edit Work End//



}]);