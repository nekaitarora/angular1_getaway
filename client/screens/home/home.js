app.controller('homeCtrl', ['$scope','$rootScope','$state','itinerarys','destinations','journals','attractions','categories','dialogs','utils','$interval', function($scope,$rootScope,$state,itinerarys,destinations,journals,attractions,categories,dialogs,utils,$interval){
$scope.popup = {};
$scope.popup.opened1 =false;
$scope.popup.opened2 =false;






$scope.config = {
	'fields' : {},
	'filter':{featured:true},
	'sort' :{views:-1},
		'skip' : 0,
		'limit' : 3,
};
//$scope.base = 0;
        var stop;
        function change() {
        	$scope.limitbase = 0;
          if ( angular.isDefined(stop) ) return;
          stop = $interval(function() {
            if ($scope.destinations.length > 0 && $scope.limitbase < ($scope.destinations.length-3)) {
              $scope.limitbase = $scope.limitbase + 3;
            } else {
              $scope.limitbase = 0;
            }
          }, 50000);
        };

change();
	journals.getalljournals($scope.config,function(err,result){
		if(err){$scope.error = err; console.log(err); return;}
		$scope.journal = result;
		// console.log('result',result);
	})
$scope.checkjournal = function(_id){
	$state.go('index.singleJournal',{'id': _id});
}
$scope.getcity = function(_id){
	$state.go('index.city',{'id': _id});
}

  // Disable weekend selection
  function disabled(data) {
    return false;
  }
$scope.open = function(num) {
	$scope.popup['opened'+num] = true;
};

  $scope.dateOptions = {
    showWeeks: false,
    dateDisabled: disabled,
    formatYear: 'yy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };
  $scope.setDate = function(year, month, day) {
    $scope.dt = new Date(year, month, day);
  };

	$scope.itinerary ={}; 

    //get all destinations for filters
	destinations.getAllDestinations({},function(err,result){
		if(err){return console.log(err);}
		$scope.destinations = result;
		$scope.trip_to_destinations = angular.copy(result);
	})

	$scope.itineraryCreate = function(){
			$scope.formSubmitted =true;
			if($scope.itineraryCreateForm.$valid){
				
				//finding date and days 
				var diff = new Date($scope.itinerary.end_date).getTime() - new Date($scope.itinerary.start_date).getTime() ;
				var days = diff/(24*60*60*1000);
					days = days +1;
				if(diff < 0){
					$scope.error = "End Date should after the Start Date";
					return;
				}
				//get required data in variables
				var trip_to = "";
				$scope.itinerary.trip_to.forEach(function(destination){
					trip_to += destination.city + ",";
				})
				trip_to = trip_to.substring(0, trip_to.length - 1);
				//create title and machine_name
				$scope.itinerary.title = days + " Day Trip to " +trip_to +" from "+$scope.itinerary.trip_from.city;
				$scope.itinerary.machine_name = utils.create_machine_name($scope.itinerary.title);

				itinerarys.createItinerary($scope.itinerary,function(err,result){
					if(err){return $scope.error = "Error in creation"; console.log("err",err);}
					console.log(result);
					$state.go("index.editItinerarysDays",{machine_name:result.machine_name, id : result._id});
				})
			}else{
				console.log("form Validation Pending");
			}
	}
	$scope.getDestinationsData = function(query){
      var regex = new RegExp(query, 'gi');
 	  return _.filter($scope.trip_to_destinations, function(obj){

 	  	if($scope.itinerary.trip_from){
 	   		return obj.city.match(regex)  && obj._id !== $scope.itinerary.trip_from._id;
 	  	}else{
 	  		return obj.city.match(regex);
 	  	}

 	   });
	}

	$scope.startCityChange =function(){
		if($scope.itinerary.trip_from){
			$scope.trip_to_destinations = _.filter($scope.destinations, function(destination){ return destination._id !== $scope.itinerary.trip_from._id ? true : false });
		}

		if($scope.itinerary.trip_from && $scope.itinerary.trip_to && $scope.itinerary.trip_to.length >0){
			$scope.itinerary.trip_to = _.filter($scope.itinerary.trip_to, function(destination){  return destination._id !== $scope.itinerary.trip_from._id ? true : false });
		}		
	}
}]);


