app.controller('attractionsCtrl', ['$scope','attractions','$state','categories','destinations','dialogs','uiGridConstants', function($scope,attractions,$state,categories,destinations,dialogs,uiGridConstants){

$scope.attraction ={};

	$scope.ratings = [0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5];
	
	categories.getAllCategories(function(err,result){
		if(err){console.log("getAllCategories",err);}
		$scope.categories = result;
	})
	destinations.getAllDestinations({},function(err,result){
		if(err){console.log("getAllDestinations",err);}
		$scope.destinations = result;
	})

	//default config for pagination work
	$scope.count = 0;
	$scope.currentpage =1 ;
	$scope.limit =10;

	
 	//get fields for showing
	$scope.config = {
		'fields' : {'city' : 1,'category' : 1,'rating':1,'name':1,'id':1,'image_url': 1,'published': 1},
		'skip' : 0,
		'limit' : $scope.limit,
		'filter':{},
		sort :{"rating" :-1 }
	};
	$scope.attraction = {};

	//grid fields definition
	$scope.attractionsGrid= {
     	paginationPageSize: 5,
     	useExternalPagination: true,
		columnDefs : [
		{displayName : "Name" , field : "name"},
		// {displayName : "Rating" , field : "rating"},
		{displayName : "City" , field : "city.city"},
		{displayName : "Category" , field : "category.title"},
		{displayName : "Operations" , field : "operations",cellTemplate: '<button ng-click="grid.appScope.viewAttraction(row.entity)" class="btn btn-xs btn-info table_btns">View</button><button ng-show="row.entity.id" ng-click="grid.appScope.duplicateAttraction(row.entity)"  class="btn btn-xs btn-warning table_btns">Duplicate</button> <button ng-show="!row.entity.id" ng-click="grid.appScope.editAttraction(row.entity)"  class="btn btn-xs btn-warning table_btns">Edit</button> <button ng-if="row.entity.published == false" ng-click="grid.appScope.publishAttraction(row.entity)" class="btn btn-xs btn-warning table_btns">Publish</button><button ng-if="row.entity.published == true" ng-click="grid.appScope.UnpublishAttraction(row.entity)" class="btn btn-xs btn-warning table_btns">Unpublish</button> <button ng-show="!row.entity.id" class="btn btn-xs btn-danger table_btns" ng-click="grid.appScope.deleteAttraction(row.entity,rowRenderIndex)">Delete</button>',enableSorting: false },
		]
	}
   $scope.attractionsGrid.enableHorizontalScrollbar = uiGridConstants.scrollbars.NEVER;
   $scope.attractionsGrid.enableVerticalScrollbar = uiGridConstants.scrollbars.NEVER;
	
	// 	//default set attractions
	getcount();
	get_attractions();

	$scope.viewAttraction = function(data){
		 $state.go('index.singleAttraction', {'id': data._id});
	};

	//duplicate attraction
	$scope.duplicateAttraction = function(attraction){
		attractions.duplicateAttraction(attraction._id,function(err,result){
			if(err){return console.log(err);}
			$state.go("admin.attractionsEdit",{id : result._id});
		})
	} 
	//get total pages count
	$scope.getTotalPages = function(){
		return $scope.getTotalPagesCount = Math.ceil($scope.count / $scope.config.limit);
	}


	$scope.publishAttraction = function(attraction){
		console.log('publish this attraction');
		console.log('attraction data here is',attraction);
		$scope.attraction  = attraction;
		$scope.attraction.published = true;
		attractions.updateAttraction($scope.attraction,function(err,result){
			if(err) {console.log(err); return;}
			console.log('result of publish is',result);
			attraction.published = result.published;
		})
	}

	$scope.UnpublishAttraction = function(attraction){
		console.log('unpublish this atraction');
		console.log('attraction data here is',attraction);
		$scope.attraction  = attraction;
		$scope.attraction.published = false;
		attractions.updateAttraction($scope.attraction,function(err,result){
			if(err) {console.log(err); return;}
			console.log('result of unpublish is',result);
			attraction.published = result.published;
		})
	}


// //edit page
	$scope.editAttraction =  function(attraction){
		 $state.go('admin.attractionsEdit', {'id': attraction._id});
	};

//next page work
	$scope.nextPage = function(){
	if($scope.currentpage < $scope.getTotalPagesCount){
			$scope.currentpage++;
			$scope.config.skip = $scope.config.skip + $scope.limit;
			get_attractions();	
		}
	}
//previous page work
	$scope.previousPage = function(){
		if($scope.currentpage > 1 ){
			$scope.currentpage--;
			$scope.config.skip = $scope.config.skip - $scope.limit;
			get_attractions();	
		}
	}
// Search attractions by name 
	$scope.searchAttraction = function(){
		if($scope.AttractionName != ''){
			$scope.config.name = $scope.AttractionName;
		}
		if($scope.filter){
			$scope.filter.rating || $scope.filter.rating==0 ? $scope.config.filter.rating = $scope.filter.rating : delete $scope.config.filter.rating;
			$scope.filter.category? $scope.config.filter.category = $scope.filter.category._id : delete $scope.config.filter.category;
			$scope.filter.city ? $scope.config.filter.city = $scope.filter.city._id : delete $scope.config.filter.city;
			
			if($scope.filter.type && $scope.filter.type == 'yolo'){
				$scope.config.filter.id = { $exists: false} ;
			}else if($scope.filter.type &&  $scope.filter.type == 'yelp'){
 				$scope.config.filter.id = { $exists: true} ;
			}else{
				delete $scope.config.filter.id; 
			}

			if( $scope.filter.openclosetime =='true'){
				$scope.config.filter.openclosetime= { $exists: true};
			}else if( $scope.filter.openclosetime =='false'){
				$scope.config.filter.openclosetime= { $exists: false};
			}else{
				delete $scope.config.filter.openclosetime;
			}

			if( $scope.filter.image_url =='true'){
				$scope.config.filter.image_url= { $exists: true};
			}else if( $scope.filter.image_url =='false'){
				$scope.config.filter.image_url= { $exists: false};
			}else{
				delete $scope.config.filter.image_url;
			}

			if( $scope.filter.published =='true'){
				$scope.config.filter.published= true;
			}else if( $scope.filter.published =='false'){
				$scope.config.filter.published= false;
			}else{
				delete $scope.config.filter.published;
			}			

		}else{
			$scope.config.filter = {};
		}

		$scope.currentpage = 1;
		get_attractions();
	}

	$scope.disableFilter = function(){
		$scope.config.filter = {};
		delete $scope.config.name;
		$scope.filter={};
		get_attractions();
	}

//Delete user
	$scope.deleteAttraction =  function(attraction, index){

		dialogs.openConfirmDialog('Are you sure want to delete Attraction? :<b> '+attraction.name+'</b>','', function(yes,no){
	      if(no){
	        return;
	      }else{
	      	attractions.deleteAttraction(attraction._id ,function(err,result){
	      		if(err){
	      			return console.log('not able to delete',err);
	      			dialogs.openErrorDialog(err);
	      		}

    	    	if(result.length > 0){
        			console.log('result is itinerary');
	        		dialogs.openMessageDialog('Sorry! This attraction can not be deleted because it is being used in itinerary');
    			}
    			else{
        			console.log('result of deleted');
        			dialogs.openMessageDialog('Attraction has been deleted');
					$scope.attractionsGrid.data.splice(index,1);
    			}	
	      	})
	      }
	    })		
	}


	function getcount(){
		attractions.getAttractionsCount($scope.config,function(err,result){
			if(err){console.log(err);return;}
			else{
				$scope.count = result.count;
				$scope.getTotalPages();
			}
		})
	}
	

	function get_attractions(){
		attractions.getAllAttractions($scope.config,function(err,result){
			if(err){$scope.error = err; console.log(err); return;}
			$scope.attractionsGrid.data = result;
			console.log('the result is ',result);
			getcount();
			$(window).resize()
		})
	}



}]);
