app.controller('attractionImportCtrl', ['$scope','attractions','categories','destinations', function($scope,attractions,categories,destinations){
	
	$scope.importjson = {};
	$scope.importjson.limit = 20;
	$scope.importjson.sort  = 0;
	
	$scope.importAllParams ={};

	$scope.importAllprogress = false;

	$scope.calling = false;

	categories.getAllCategories(function(err,result){
		if(result){
			$scope.categories =result;
		}
	})
	destinations.getAllDestinations({},function(err,result){
		if(result){$scope.cities = result;}
	})

	$scope.offset=[0,20,40,60,80,100,120,140,160,180,200,220,240,260,280,300,320,340,360,380,400,420,440,460,480,500,520,540,560,580,600,620,640,660,680,700,720,740,760,780,800,820,840,860,880,900,920,940,960,980];

	setInterval(function(){ 
		$scope.updateProgress()
	}, 3000);


// $scope.importedJson = {"台北市":{"restaurants":{"0":{"records":4,"status":"done"}},"publicservicesgovt":{"0":{"records":20,"status":"done"}}},"İstanbul":{"localservices":{"0":{"records":20,"status":"done"}}},"Paris":{"localservices":{"0":{"records":20,"status":"done"},"20":{"records":19,"status":"done"}}},"London ":{"restaurants":{"0":{"records":20,"status":"done"}}},"Marseille":{"eventservices":{"0":{"records":20,"status":"done"}}}};
	$scope.import = function(){
		$scope.importForm.term.$touched =true;
		$scope.importForm.category.$touched =true;
		$scope.importForm.offset.$touched =true;
		if($scope.importForm.$valid){
			$scope.calling = true;
			$scope.error = false;
			attractions.importData($scope.importjson,function(err,result){
				if(err){console.log(err);$scope.error = err;$scope.calling = false;}
				else{
					$scope.calling = false;
					$scope.progress = result;
				}
			})
		}
	} 

	$scope.importAll = function(){
		$scope.importForm.term.$touched =true;
		$scope.importForm.category.$touched =true;
		$scope.importForm.offset.$touched =true;
		if($scope.importForm.$valid){

			$scope.importAllParams = angular.copy($scope.importjson); 

			$scope.error = false;
			attractions.importAllData($scope.importjson,function(err,result){
				if(err){console.log(err);$scope.error = err;$scope.calling = false;}
				else{
					$scope.progress = result;
				}
			})	
		}
	} 


	$scope.updateProgress = function(){
		attractions.getProgress(function(err,result){
			if(err){
				$scope.error = "Not able to get progress";
			}else{
				$scope.progress = result;
				if($scope.importAllParams && $scope.importAllParams.category_filter &&  $scope.importAllParams.term) {
					if(!$scope.progress[$scope.importAllParams.term.city]){
						$scope.progress[$scope.importAllParams.term.city] = {};
					}
					if(!$scope.progress[$scope.importAllParams.term.city][$scope.importAllParams.category_filter.name]){
						$scope.progress[$scope.importAllParams.term.city][$scope.importAllParams.category_filter.name] = {imported : 0 , total_count:1000};
					}
				}
			}
		})
	}

}]);
