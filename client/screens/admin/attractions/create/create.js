app.controller('attractionsCreateCtrl', ['$scope','attractions','categories','destinations','utils','$state','dialogs','fileupload','$filter','dataService', function($scope,attractions,categories,destinations,utils,$state,dialogs,fileupload,$filter,dataService){
	
	$scope.filers = {};
	$scope.timing = [];
	$scope.attraction = {};
	$scope.attraction.rating = 1;
	$scope.ratings = [0,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5];
	$scope.allDays = {0:"sunday",1:"monday",2:"tuesday",3:"wednesday",4:"thursday",5:"friday",6:"saturday"};
    $scope.attraction.is_always_open = "true";


    $scope.clickme = function() {
        $scope.timing.push({});
    }

	$scope.removeopenclosetime = function(index){
		$scope.timing.splice(index,1);
	}    
	
	categories.getAllCategories(function(err,result){
		if(err){console.log("getAllCategories",err);}
		$scope.categories = result;
	})
	destinations.getAllDestinations({},function(err,result){
		if(err){console.log("getAllDestinations",err);}
		$scope.destinations = result;
	})
	utils.getAllLanguages(function(err,result){
		if(err){console.log("getAllLanguages",err);}
		$scope.languages = result;		
	})
	$scope.language_change = function(){
		$scope.attraction.language = $scope.languages[$scope.attraction.language_code];
	}

	$scope.alwaysopen = function(){
		//console.log("test"+$scope.attraction.is_always_open);
		//console.log(document.getElementById('is_always_open').checked);
		if($scope.attraction.is_always_open == 'true'){
			//console.log('what?');
			if($scope.timing.length > 0){
				dialogs.openConfirmDialog('Warning Message','Your custom time settings will be erased. Do you want to continue?', function(yes,no){
	      			if(no){
	        			$scope.attraction.is_always_open ? $scope.attraction.is_always_open = 'false' : $scope.attraction.is_always_open = 'true';
	        		return;
	      			}else{
	      				$scope.attraction.is_always_open ? $scope.attraction.is_always_open = 'true' : $scope.attraction.is_always_open = 'false';
	      			}
	    		})				
			}
		}
	}



var myCenter;
$scope.cityPlace = function(location){
    console.log('location',location);
    if(location.location){
    $scope.citylat = location.location.lat;
    $scope.citylng = location.location.lng;
    // console.log('loc',location);
    myCenter = new google.maps.LatLng($scope.citylat,$scope.citylng);
        // dataService.getCoordinatesFromAddress(city, function(err, data) {
        //     console.log('data',data);
        //     $scope.attraction.longitude = data.results[0].geometry.location.lat;
        //     $scope.attraction.latitude = data.results[0].geometry.location.lng;
        //     // show map
        //     $scope.showMap($scope.attraction.longitude,$scope.attraction.latitude);
        // });
        $scope.showMap($scope.citylat,$scope.citylng);
    }
    else{
        console.log('no location found');
        myCenter = new google.maps.LatLng();   
    }
}

    // var myCenter = new google.maps.LatLng($scope.citylat,$scope.citylng);
    var marker;
     $scope.placeMarker =function(location) {
        // var marker;
        if (marker && marker.setMap) {
            marker.setMap(null);
        }
        // if(!marker){
        marker = new google.maps.Marker({
            position: location,
            map: map,
        // });
    })
    // else if(marker){
    //     marker.setPosition(location);
    // }

        var infowindow = new google.maps.InfoWindow({
            content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
        });
        $scope.attraction.longitude = location.lng();
        $scope.attraction.latitude = location.lat();
        console.log('location is ',$scope.attraction.latitude,$scope.attraction.longitude);
        infowindow.open(map,marker);

    }

    $scope.showMap = function(longitude,latitude) {
        var mapProp = {
            center: myCenter,
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        console.log('checking',new google.maps.LatLng(longitude,latitude));
        $scope.placeMarker(new google.maps.LatLng(longitude,latitude));
        google.maps.event.addListener(map, 'click', function(event) {
            $scope.placeMarker(event.latLng);
            console.log(event.latLng);
            $scope.$digest();
            // $scope.attraction.longitude = event.latLng.lng();
            // $scope.attraction.latitude = event.latLng.lat();
        });
    }
    // $scope.showMap(29.135,75.719);

   

    // google.maps.event.addDomListener(window, 'load', initialize);

    $scope.current = function() {
        console.log('hello');

        //navigator.geolocation.getCurrentPosition(function(position){console.log('hello123',position.coords.latitude,position.coords.longitude)});
        navigator.geolocation.getCurrentPosition(function(position) {
                console.log('hello', position);
                $scope.attraction.longitude = position.coords.longitude;
                $scope.attraction.latitude = position.coords.latitude;
            })
            
    }
    $scope.checkaddress = function(address) {
        console.log(address);
        dataService.getCoordinatesFromAddress(address, function(err, data) {
            console.log(data);
            $scope.attraction.longitude = data.results[0].geometry.location.lat;
            $scope.attraction.latitude = data.results[0].geometry.location.lng;
            // show map
            myCenter = new google.maps.LatLng($scope.attraction.latitude,$scope.attraction.longitude); 
            $scope.showMap($scope.attraction.longitude,$scope.attraction.latitude);
        });
    }





	$scope.createAttraction = function(){
		$scope.count = 1;
		var days1 = [];
		if($scope.CreateAttractionForm.$valid){
			$scope.attraction.openclosetime = [];
			if($scope.attraction.is_always_open == 'true'){
				//console.log('$scope.attraction.is_always_open',$scope.attraction.is_always_open);
				$scope.attraction.is_always_open = true;
				delete $scope.timing;
			}
			if($scope.timing){
			for(var i=0;i<$scope.timing.length;i++){
				days1[i] = {};
				days1[i].day = $scope.timing[i].day;
				days1[i].opentime = $filter('date')($scope.timing[i].opentime, 'HH:mm');
				days1[i].closetime = $filter('date')($scope.timing[i].closetime, 'HH:mm');
			}
			//console.log('timing',$scope.timing);
		}
			$scope.attraction.openclosetime = days1;
			$scope.error = false;
			fileupload.uploadfile($scope.temp_file,'attractions',function(err,result){
				if(err){ console.log("file not uploaded",err);}
				else{
					$scope.attraction.image_url = result;
					attractions.createAttraction($scope.attraction ,function(err,result){
						if(err){
							if(err.message){
								return $scope.error = err.message;
							}else{
								return dialogs.openErrorDialog(err);
							}
						}else{
							dialogs.openMessageDialog("Attraction Created");
							$state.go("admin.attractions");
						}
					})
				}
			});
		}else{
			$scope.submitted = true;
		}
	}


}]);
