app.controller('attractionsEditCtrl', ['$scope', 'attractions', 'categories', 'destinations', 'utils', '$state', 'dialogs', 'fileupload', '$filter','dataService', function($scope, attractions, categories, destinations, utils, $state, dialogs, fileupload, $filter,dataService) {
    $scope.filers = {};
    $scope.ratings = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5];
    $scope.allDays = {0:"sunday",1:"monday",2:"tuesday",3:"wednesday",4:"thursday",5:"friday",6:"saturday"};
    $scope.timing = [];
    $scope.image_url = [];
    $scope.attraction = {};
    var myCenter;

    $scope.clickme = function() {
        $scope.timing.push({});
    }

	$scope.removeopenclosetime = function(index){
		$scope.timing.splice(index,1);
	}

    $scope.removeImage = function(index){
        console.log('the image is',$scope.image_url);
        $scope.image_url.splice(index,1)
    }

    $scope.cancel = function(){
        $state.go('admin.attractions');
    }

    categories.getAllCategories(function(err, result) {
        if (err) { console.log(err); }
        $scope.categories = result;
    })
    destinations.getAllDestinations({}, function(err, result) {
        if (err) { console.log("getAllDestinations", err); }
        $scope.destinations = result;
        //console.log('destinations',result);
    })
    utils.getAllLanguages(function(err, result) {
        if (err) { console.log("getAllLanguages", err); }
        $scope.languages = result;
    })

    attractions.getAttraction($state.params.id, function(err, result) {
        if (err) {
            dialogs.openErrorDialog("Wrong Attraction Id");
            $state.go("admin.attractions");
        } else {
            console.log(result);
            $scope.attraction = result;
            $scope.image_url = result.image_url;
            console.log('image_url is',$scope.image_url);
            $scope.showMap($scope.attraction.latitude,$scope.attraction.longitude);
            for (var a = 0; a < result.openclosetime.length; a++) {
                // // var opentime =new Date(result.openclosetime[a].open.time).toLocaleTimeString();
                // var open =$filter('date')(result.openclosetime[a].open.time, 'HH:mm');
                // console.log("sdsds"+open);
                var opening = new Date();
                opening.setHours(result.openclosetime[a].open.time.split(':')[0]);
                opening.setMinutes(result.openclosetime[a].open.time.split(':')[1]);
                var opendate = $filter('date')(opening, 'yyyy-MM-dd hh:mm:a');

                var closing = new Date();
                closing.setHours(result.openclosetime[a].close.time.split(':')[0]);
                closing.setMinutes(result.openclosetime[a].close.time.split(':')[1]);
                var closedate = $filter('date')(closing, 'yyyy-MM-dd hh:mm:a');
                // console.log('hello', typeof opening);
                $scope.timing[a] = {};
                $scope.timing[a].opentiming = new Date(opendate);
                $scope.timing[a].day = result.openclosetime[a].open.day;
                $scope.timing[a].closetiming = new Date(closedate);
            }
            //console.log('openclosetime', result.openclosetime);
        }
        if($scope.timing.length > 0){
        	$scope.attraction.is_always_open = 'false';
        }
        else{
        	$scope.attraction.is_always_open = 'true';
        }
    })






// var myCenter;
$scope.cityPlace = function(location){
    console.log('location',location);
    if(location.location){
    $scope.citylat = location.location.lat;
    $scope.citylng = location.location.lng;
    // console.log('loc',location);
    myCenter = new google.maps.LatLng($scope.citylat,$scope.citylng);
        // dataService.getCoordinatesFromAddress(city, function(err, data) {
        //     console.log('data',data);
        //     $scope.attraction.longitude = data.results[0].geometry.location.lat;
        //     $scope.attraction.latitude = data.results[0].geometry.location.lng;
        //     // show map
        //     $scope.showMap($scope.attraction.longitude,$scope.attraction.latitude);
        // });
        $scope.showMap($scope.citylat,$scope.citylng);
    }
    else{
        console.log('no location found');
        myCenter = new google.maps.LatLng();   
    }
}

    // var myCenter = new google.maps.LatLng($scope.citylat,$scope.citylng);
    var marker;
     $scope.placeMarker =function(location) {
        // var marker;
        if (marker && marker.setMap) {
            marker.setMap(null);
        }
        // if(!marker){
        marker = new google.maps.Marker({
            position: location,
            map: map,
        // });
    })
    // else if(marker){
    //     marker.setPosition(location);
    // }

        var infowindow = new google.maps.InfoWindow({
            content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
        });
        $scope.attraction.longitude = location.lng();
        $scope.attraction.latitude = location.lat();
        console.log('location is ',$scope.attraction.latitude,$scope.attraction.longitude);
        infowindow.open(map,marker);

    }

    $scope.showMap = function(longitude,latitude) {
        var myCenter = new google.maps.LatLng($scope.attraction.latitude,$scope.attraction.longitude);
        var mapProp = {
            center: myCenter,
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        console.log('checking',new google.maps.LatLng(longitude,latitude));
        $scope.placeMarker(new google.maps.LatLng(longitude,latitude));
        google.maps.event.addListener(map, 'click', function(event) {
            $scope.placeMarker(event.latLng);
            console.log(event.latLng);
            $scope.$digest();
            // $scope.attraction.longitude = event.latLng.lng();
            // $scope.attraction.latitude = event.latLng.lat();
        });
    }
    // $scope.showMap($scope.attraction.latitude,$scope.attraction.longitude);

   

    // google.maps.event.addDomListener(window, 'load', initialize);

    $scope.current = function() {
        console.log('hello');

        //navigator.geolocation.getCurrentPosition(function(position){console.log('hello123',position.coords.latitude,position.coords.longitude)});
        navigator.geolocation.getCurrentPosition(function(position) {
                console.log('hello', position);
                $scope.attraction.longitude = position.coords.longitude;
                $scope.attraction.latitude = position.coords.latitude;
            })
            
    }
    $scope.checkaddress = function(address) {
        console.log(address);
        dataService.getCoordinatesFromAddress(address, function(err, data) {
            console.log(data);
            $scope.attraction.longitude = data.results[0].geometry.location.lat;
            $scope.attraction.latitude = data.results[0].geometry.location.lng;
            // show map
            myCenter = new google.maps.LatLng($scope.attraction.latitude,$scope.attraction.longitude); 
            $scope.showMap($scope.attraction.longitude,$scope.attraction.latitude);
        });
    }











    $scope.language_change = function() {
        $scope.attraction.language = $scope.languages[$scope.attraction.language_code];
    }

/*	$scope.alwaysopencheckbox = function(){
		dialogs.openConfirmDialog('Are you sure you want to revert this checkbox? All your timings Data will be deleted.','', function(yes,no){
	      if(no){
	        //$scope.attraction.is_always_open = false;
	        $scope.attraction.is_always_open ? $scope.attraction.is_always_open = false : $scope.attraction.is_always_open = true;
	        return;
	      }else{
	      	//$scope.attraction.is_always_open = true;
	      	$scope.attraction.is_always_open ? $scope.attraction.is_always_open = true : $scope.attraction.is_always_open = false;
	      }
	    })
	}*/

	$scope.alwaysopen = function(){
		//console.log("test"+$scope.attraction.is_always_open);
		//console.log(document.getElementById('is_always_open').checked);
		if($scope.attraction.is_always_open == 'true'){
			//console.log('what?');
			if($scope.timing.length > 0){
				dialogs.openConfirmDialog('Warning Message','Your custom time settings will be erased. Do you want to continue?', function(yes,no){
	      			if(no){
	        			$scope.attraction.is_always_open ? $scope.attraction.is_always_open = 'false' : $scope.attraction.is_always_open = 'true';
	        		return;
	      			}else{
	      				$scope.attraction.is_always_open ? $scope.attraction.is_always_open = 'true' : $scope.attraction.is_always_open = 'false';
	      			}
	    		})				
			}
		}
	}

    $scope.updateAttraction = function() {
        var days1 = [];
        console.log('previous images are ',$scope.image_url);
        //console.log('data here', $scope.attraction);
        //console.log('data here is ', $scope.timing);
		if($scope.attraction.is_always_open == 'true'){
				//console.log('$scope.attraction.is_always_open',$scope.attraction.is_always_open);
			$scope.attraction.is_always_open = true;
			delete $scope.timing;
		}
        $scope.attraction.openclosetime = [];
        if($scope.timing){
        for (var i = 0; i < $scope.timing.length; i++) {
            days1[i] = {};
            days1[i].day = $scope.timing[i].day;
            days1[i].opentiming = $filter('date')($scope.timing[i].opentiming, 'HH:mm');
            days1[i].closetiming = $filter('date')($scope.timing[i].closetiming, 'HH:mm');
        }
    }
        $scope.attraction.openclosetime = days1;
        if ($scope.EditAttractionForm.$valid) {
            if ($scope.temp_file) {
                console.log('temp file is',$scope.temp_file.name);
                $scope.error = false;
                // $scope.image_url.push()
                fileupload.uploadfile($scope.temp_file, 'attractions', function(err, result) {
                    if (err) { console.log("file not uploaded", err); } else {
                        var newImageArray = $scope.image_url.concat(result);
                        console.log('newImageArray',newImageArray);
                        $scope.attraction.image_url = newImageArray;
                        attractions.updateAttraction($scope.attraction, function(err, result) {
                            if (err) {
                                if (err.message) {
                                    return $scope.error = err.message;
                                } else {
                                    return dialogs.openErrorDialog(err);
                                }
                            } else {
                                dialogs.openMessageDialog("Attraction Updated");
                                $state.go("admin.attractions");
                            }
                        })
                    }
                });
            } else {
                attractions.updateAttraction($scope.attraction, function(err, result) {
                    if (err) {
                        if (err.message) {
                            return $scope.error = err.message;
                        } else {
                            return dialogs.openErrorDialog(err);
                        }
                    } else {
                        dialogs.openMessageDialog("Attraction Updated");
                        $state.go("admin.attractions");
                    }
                })
            }
        } else {
            console.log($scope.EditAttractionForm);
            $scope.submitted = true;
        }
    }

}]);
