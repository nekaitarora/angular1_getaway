app.controller('adminIndexCtrl', ['$scope','$rootScope','users','localStorageService','$state','$location', function($scope,$rootScope,users,localStorageService,$state,$location){


	// $rootScope.base_path = "http://localhost:8080";
	// $rootScope.base_path = "http://testsite.com:8080";
	// $rootScope.base_path = "http://139.162.5.142:8080";
	$rootScope.base_path = $location.protocol() + '://' + $location.host()+":"+$location.port();
	
	// //check if login and set rootscope and  storage accordingly
	users.islogin();

	$scope.logout = function(){
		users.logout(function(err,result){
			if(result){
				$rootScope.loggedInUser = false;
				localStorageService.remove('yolo_itinerary_client');
				$state.go('admin.login');					
				console.log('logout successfully');
			}
		});
	}

}]);