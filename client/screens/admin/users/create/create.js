app.controller('usersCreateCtrl', ['$scope','users','dialogs','$state','$rootScope','fileupload', function($scope, users,dialogs,$state,$rootScope,fileupload){
$scope.images = [];

$scope.TimelineCropper = {};
$scope.TimelineCropper.sourceImage = null;
$scope.TimelineCropper.croppedImage   = null;


$scope.ImageCropper = {};
$scope.ImageCropper.sourceImage = null;
$scope.ImageCropper.croppedImage   = null;




	$scope.createUser = function(){
		if($scope.user.local.password == $scope.user.local.confirmPassword){
		   $scope.user.verified	=true;
		   	if($scope.TimelineCropper.croppedImage){
				$scope.user.timeline_image = $scope.TimelineCropper.croppedImage;
			}
		   	if($scope.ImageCropper.croppedImage){
				$scope.user.image = $scope.ImageCropper.croppedImage;
			}
			users.createUser($scope.user,function(err,result){
    			if (err) { 
	            	if(err.message){
	            		$scope.error = err.message
	            	}else{
			           dialogs.openErrorDialog(err)
	            	}
	            	return;
	            }else{
	           		dialogs.openMessageDialog('User Created Successfully');
					$state.go('admin.users');
				}
			})
		}else{
			$scope.error = 'Password doesn\'t match ';
		}
	}
}]);