app.controller('usersEditCtrl', ['$scope','users','dialogs','$state','$rootScope','localStorageService','fileupload', function($scope, users,dialogs,$state,$rootScope,localStorageService,fileupload){
    $scope.user={};
$scope.images = [];
    //get user for edit
    users.getUser($state.params.id,function(err,result){
        if(err){dialogs.openErrorDialog(err);return}
        else{
            result.status = String(result.status);
            result.isadmin = String(result.isadmin);
            $scope.user = result;
        }
    })
    $scope.TimelineCropper = {};
    $scope.TimelineCropper.sourceImage = null;
    $scope.TimelineCropper.croppedImage   = null;


    $scope.ImageCropper = {};
    $scope.ImageCropper.sourceImage = null;
    $scope.ImageCropper.croppedImage   = null;

    $scope.UpdateUser = function() {
        $scope.error = null;
        if ($scope.createForm.$valid){
            if ($scope.user.local.password != undefined || $scope.user.local.confirmPassword != undefined) {
                 if ($scope.user.local.password != $scope.user.local.confirmPassword) {
                        $scope.error = ['Password doesn\'t match '];
                        return;
                }
            }
            if($scope.TimelineCropper.croppedImage){
                $scope.user.timeline_image = $scope.TimelineCropper.croppedImage;
            }
            if($scope.ImageCropper.croppedImage){
                $scope.user.image = $scope.ImageCropper.croppedImage;
            }
            users.updateuser($state.params.id,$scope.user,function(err,result){
                if (err) { 
                    if(err.message){
                        $scope.error = err.message;
                    }else{
                         dialogs.openErrorDialog(err);
                    }
                    return;
                }else{
                    if(result._id == $rootScope.loggedInUser._id){
                        $rootScope.loggedInUser = result;
                        localStorageService.set('yolo_itinerary_client',result);
                    }
                    dialogs.openMessageDialog('User Updated Successfully');
                    $state.go('admin.users');
                }

            })
        }else{
            $scope.error = "Form Validation Failing.."
        }
    }

}]);