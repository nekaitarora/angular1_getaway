app.controller('adminCtrl', ['$scope','attractions','users', function($scope,attractions,users){
	attractions.getAttractionsCount({},function(err,result){
		if(err){$scope.totalAttractions = 0;console.log(err);}
		else{
			$scope.totalAttractions = result.count;
		}
	})

	users.getalluserscount({},function(err,result){
		if(err){$scope.totalUsers = 0;console.log(err);}
		else{
			$scope.totalUsers = result.count;
		}
	})
}]);