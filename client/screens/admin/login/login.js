app.controller('adminLoginCtrl', ['$scope','localStorageService','users','dialogs','$rootScope','$state', function($scope,localStorageService,users,dialogs,$rootScope,$state){
	$scope.login = function(){
		users.login($scope.user,function(err,result){
				if(err){
					if(err.message){
						$scope.error = err.message;
					}else{
						dialogs.openErrorDialog(err);
					}
					return;
				}else{
					$scope.error= null;
					localStorageService.set('yolo_itinerary_client',result);
					$rootScope.loggedInUser = result;
					if($rootScope.loggedInUser && $rootScope.loggedInUser.isadmin && $rootScope.loggedInUser.issuperadmin){
						dialogs.openMessageDialog("<p>User Login Successfully</p>");
					}
					$state.go('admin.admin');
				}
		})
	}

}]);