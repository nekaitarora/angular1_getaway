app.controller('adminItinerariesCtrl', ['$scope','users','journals','$state','dialogs','$rootScope','uiGridConstants','itinerarys', function($scope, users,journals,$state,dialogs,$rootScope,uiGridConstants,itinerarys){

//default config for pagination work
	$scope.count = 0;
	$scope.currentpage =1 ;
	$scope.limit =10;

// 	//get fields for showing
	$scope.config = {
	'fields' : {'title' : 1,'published' : 1,'created_on' :1,machine_name: 1,featured: 1},
	'filter':{},
	'sort' :{'created_on' :-1},
		'skip' : 0,
		'limit' : $scope.limit,
	};
	$scope.itinerary = {};
	$scope.itinerary ={title : '', published: '', created_on: ''};
   
	//grid fields definition
	$scope.itinerariesGrid= {
     	paginationPageSize: 5,
     	useExternalPagination: true,
		columnDefs : [
		{displayName : "Title" , field : "title"},
		{displayName : "Published" , field : "published"},
		{displayName : "featured" , field : "featured"},
		{displayName : "Created" , field : "created_on", type: 'date', cellFilter: 'date:\"dd/MM/yyyy h:mma\"' },
		{displayName : "Operations" , field : "operations",cellTemplate: '<button ng-click="grid.appScope.viewItinerary(row.entity)" class="btn btn-xs btn-info table_btns">View</button><button ng-if="row.entity.published == true && row.entity.featured == false" ng-click="grid.appScope.featureItinerary(row.entity)" class="btn btn-xs btn-warning table_btns">Feature</button><button ng-if="row.entity.published == true && row.entity.featured == true" ng-click="grid.appScope.UnfeatureItinerary(row.entity)" class="btn btn-xs btn-warning table_btns">Unfeature</button><button class="btn btn-xs btn-danger table_btns" ng-click="grid.appScope.deleteItinerary(row.entity,rowRenderIndex)">Delete</button>',enableSorting: false },
		]
	}
   $scope.itinerariesGrid.enableHorizontalScrollbar = uiGridConstants.scrollbars.NEVER;
   $scope.itinerariesGrid.enableVerticalScrollbar = uiGridConstants.scrollbars.NEVER;




	getcount();
	get_data();

	//get total pages count
	$scope.getTotalPages = function(){
		return $scope.getTotalPagesCount = Math.ceil($scope.count / $scope.config.limit);
	}

	//next page work
	$scope.nextPage = function(){
		if($scope.currentpage < $scope.getTotalPagesCount){
			$scope.currentpage++;
			$scope.config.skip = $scope.config.skip + $scope.limit;
			get_data();	
		}
	}
	
	//previous page work
	$scope.previousPage = function(){
		if($scope.currentpage > 1 ){
			$scope.currentpage--;
			$scope.config.skip = $scope.config.skip - $scope.limit;
			get_data();	
		}
	}
	function getcount(){
		//get total count of itineraries
		itinerarys.getAllItinerariesCount($scope.config,function(err,result){
			if(err){console.log(err);return;}
			else{
				$scope.count = result.count;
				$scope.getTotalPages();
			}
		})
	}



	function get_data(){
		itinerarys.getAllItineraries($scope.config,function(err,result){
			if(err){$scope.error = err; console.log(err); return;}
			$scope.itinerariesGrid.data = result;
			getcount();
		})
	}



// //edit page
	// $scope.editjournal =  function(journal){
	// 	 $state.go('admin.editJournal', {'id': journal._id});
	// };

	$scope.viewItinerary =  function(itinerary){
		console.log('itinerary is',itinerary);
		 $state.go('index.ItineraryView', {'machine_name': itinerary.machine_name,'id': itinerary._id});
	};

//Delete itinerary
	$scope.deleteItinerary =  function(itinerary, index){
		dialogs.openConfirmDialog('Are you sure you want to delete this itinerary permanently <b> '+itinerary.title+'</b>','', function(yes,no){
	      if(no){
	        console.log("Delete itinerary account");
	        return;
	      }else{
	      	itinerarys.deletePermanently(itinerary._id,function(err,result){
	      		if(err){	
					dialogs.openErrorDialog(err);return;
	      		}
	      		console.log('result here is ',result);
        		if(result.length > 0){
        			console.log('result is journal');
        			dialogs.openMessageDialog('Sorry! This itinerary can not be deleted because it is being used in journal');
    			}
    			else{
        			console.log('result of deleted');
        			dialogs.openMessageDialog('Itinerary has been successfully deleted');
	      			$scope.itinerariesGrid.data.splice(index,1);	
    			}
	      	})
	      }
	    })		
	}

	// $scope.showcomments =  function(journal){
	// 	 $state.go('admin.showcomments', {'id': journal._id});
	// };

	$scope.featureItinerary = function(itinerary){
		console.log('feature this itineray');
		itinerarys.updateItinerary(itinerary._id,{featured: true},function(err,result){
			if(err) {console.log(err); return;}
			console.log('result of feature is',result);
			itinerary.featured = result.featured;
		})
	}

	$scope.UnfeatureItinerary = function(itinerary){
		console.log('Unfeature this itineray');
		itinerarys.updateItinerary(itinerary._id,{featured: false},function(err,result){
			if(err) {console.log(err); return;}
			console.log('result of feature is',result);
			itinerary.featured = result.featured;
		})
	}

// Search itinerary by title or publish
	$scope.searchItinerary = function(){

		delete $scope.config.title;
		delete $scope.config.published;
		// delete $scope.config.language;

		if($scope.itinerary.title != ''){
			$scope.config.title =  $scope.itinerary.title;
		}	
		if($scope.filter){
			
			console.log('filter with publish');

			if( $scope.filter.published =='true'){
				$scope.config.filter.published= true;
			}else if( $scope.filter.published =='false'){
				$scope.config.filter.published = false;
			}else if($scope.filter.featured == 'true'){
				$scope.config.filter.featured= true;
			}else if($scope.filter.featured == 'false'){
				$scope.config.filter.featured = false;
			}else if($scope.filter.isdeleted == 'true'){
				$scope.config.filter.isdeleted= true;
			}else if($scope.filter.isdeleted == 'false'){
				$scope.config.filter.isdeleted = false;
			}else{
				delete $scope.config.filter.isdeleted;
				delete $scope.config.filter.featured;
				delete $scope.config.filter.published;
			}
		}
		else{
			$scope.config.filter={};
		}
		get_data();
	}

	$scope.disableFilter = function(){
		delete $scope.config.title;
		$scope.itinerary ={};
		$scope.config.filter={};
		$scope.filter = {};
		get_data();
	}



}]);

