app.controller('adminJournalsCtrl', ['$scope','journals','$state','dialogs','uiGridConstants', function($scope,journals,$state,dialogs,uiGridConstants){

//default config for pagination work
	$scope.count = 0;
	$scope.currentpage =1 ;
	$scope.limit =10;

// 	//get fields for showing
	$scope.config = {
	'fields' : {'title' : 1,'publish' : 1,'language': 1,'created_on' :1,'featured': 1},
	'filter':{},
	'sort' :{'created_on' :-1},
		'skip' : 0,
		'limit' : $scope.limit,
	};
	$scope.journal = {};
	$scope.journal ={title : '', publish: '', language: '', created_on: ''};
   
	//grid fields definition
	$scope.journalsGrid= {
     	paginationPageSize: 5,
     	useExternalPagination: true,
		columnDefs : [
		{displayName : "Title" , field : "title"},
		{displayName : "Publish" , field : "publish"},
		{displayName : "Language" , field : "language"},
		{displayName : "Created" , field : "created_on", type: 'date', cellFilter: 'date:\"dd/MM/yyyy h:mma\"' },
		{displayName : "Operations" , field : "operations",cellTemplate: '<button ng-click="grid.appScope.viewJournal(row.entity)" class="btn btn-xs btn-info table_btns">View</button><button ng-click="grid.appScope.editjournal(row.entity)" class="btn btn-xs btn-warning table_btns">Edit</button><button class="btn btn-xs btn-danger table_btns" ng-click="grid.appScope.deletejournal(row.entity,rowRenderIndex)">Delete</button><button ng-click="grid.appScope.showcomments(row.entity)" class="btn btn-xs btn-warning table_btns">Comments</button>',enableSorting: false },
		]
	}
   $scope.journalsGrid.enableHorizontalScrollbar = uiGridConstants.scrollbars.NEVER;
   $scope.journalsGrid.enableVerticalScrollbar = uiGridConstants.scrollbars.NEVER;



	// 	//default set journals
	getcount();
	get_data();

	$scope.viewJournal = function(data){
		 $state.go('index.singleJournal', {'id': data._id});
	};

	//get total pages count
	$scope.getTotalPages = function(){
		return $scope.getTotalPagesCount = Math.ceil($scope.count / $scope.config.limit);
	}

	//next page work
	$scope.nextPage = function(){
		if($scope.currentpage < $scope.getTotalPagesCount){
			$scope.currentpage++;
			$scope.config.skip = $scope.config.skip + $scope.limit;
			get_data();	
		}
	}
	
	//previous page work
	$scope.previousPage = function(){
		if($scope.currentpage > 1 ){
			$scope.currentpage--;
			$scope.config.skip = $scope.config.skip - $scope.limit;
			get_data();	
		}
	}
	function getcount(){
		//get total count of journals
		journals.getalljournalscount($scope.config,function(err,result){
			if(err){console.log(err);return;}
			else{
				$scope.count = result.count;
				$scope.getTotalPages();
			}
		})
	}

	function get_data(){
		journals.getalljournals($scope.config,function(err,result){
			if(err){$scope.error = err; console.log(err); return;}
			$scope.journalsGrid.data = result;
			getcount();
		})
	}



// //edit page
	$scope.editjournal =  function(journal){
		 $state.go('admin.editJournal', {'id': journal._id});
	};

//Delete journal
	$scope.deletejournal =  function(journal, index){
		dialogs.openConfirmDialog('Are you sure want to delete journal? :<b> '+journal.title+'</b>','', function(yes,no){
	      if(no){
	        console.log("Delete journal account");
	        return;
	      }else{
	      	journals.deleteJournal(journal._id,function(err,result){
	      		if(err){	
					dialogs.openErrorDialog(err);return;
	      		}else{
	      			$scope.journalsGrid.data.splice(index,1);	
	      		}
	      	})
	      }
	    })		
	}

	$scope.showcomments =  function(journal){
		 $state.go('admin.showcomments', {'id': journal._id});
	};


// Search journal by title or publish or language
	$scope.searchJournal = function(){

		delete $scope.config.title;
		delete $scope.config.publish;
		delete $scope.config.language;

		if($scope.journal.title != ''){
			$scope.config.title =  $scope.journal.title;
		}	
		if($scope.filter){
			$scope.filter.language ? $scope.config.filter.language = $scope.filter.language : delete $scope.config.filter.language;
			if( $scope.filter.publish =='true'){
				$scope.config.filter.publish= true;
			}else if( $scope.filter.publish =='false'){
				$scope.config.filter.publish= false;
			}else{
				delete $scope.config.filter.publish;
			}

			if( $scope.filter.featured =='true'){
				$scope.config.filter.featured= true;
			}else if( $scope.filter.featured =='false'){
				$scope.config.filter.featured= false;
			}else{
				delete $scope.config.filter.featured;
			}

		}
		else{
			$scope.config.filter={};
		}
		get_data();
	}

	$scope.disableFilter = function(){
		delete $scope.config.title;
		$scope.journal ={};
		$scope.config.filter={};
		$scope.filter = {};
		get_data();
	}



}]);

