app.controller('journalsEditCtrl', ['$scope','journals','utils','destinations','dialogs','tags','$state','$rootScope','localStorageService','fileupload','itinerarys', function($scope,journals,utils,destinations,dialogs,tags,$state,$rootScope,localStorageService,fileupload,itinerarys){
    $scope.journal={};
    $scope.config = {
    'fields' : {'name' : 1},
    'filter':{},
    'sort' :{'name' :1},
    };

    var user = localStorageService.get('yolo_itinerary_client'); 

    $scope.itineraryConfig = {
    'fields' : {title: 1,machine_name: 1},
    'filter':{"created_by": user._id,"isdeleted": false},
    'sort' :{},
    };

/*    utils.getAllLanguages(function(err,result){
        if(err){console.log("getAllLanguages",err);}
        $scope.languages = result;      
    })
*/
//console.log('tag at 0 position is',$scope.journal.tags[0].name);

    $scope.getTags = function(query){
     var regex = new RegExp(query, 'gi');
     return _.filter($scope.tag, function(obj){ return obj.name.match(regex);});
    }

    $scope.getItineraries = function(query){
     var regex = new RegExp(query, 'gi');
     return _.filter($scope.itineraries, function(obj){ return obj.machine_name.match(regex);});
    } 


    $scope.languages = {"zn":"Chinese","en":"English"};
    
    //get journal for edit
    journals.getJournal($state.params.id,function(err,result){
        if(err){dialogs.openErrorDialog(err);return}
        else{
            result.status = String(result.status);
            result.isadmin = String(result.isadmin);
            $scope.journal = result;
            console.log('tag at 0 position is',$scope.journal);
        }
    })

    tags.getallTags($scope.config, function(err,result){
        if(err){console.log("getalltags",err);}
        $scope.tag = result;
        //$scope.journal.tags = result;
/*      $scope.tags =[{name:"1111"},{name :"22222"}];*/
        return;
    })

    itinerarys.getAllItineraries($scope.itineraryConfig, function(err,result){
        if(err){console.log("getItineraries",err);}
        $scope.itineraries = result;
        console.log('result of itinerary is',$scope.itineraries,user);
        // taghere = result;
/*      $scope.tags =[{name:"1111"},{name :"22222"}];*/
        return;
    })


    destinations.getAllDestinations({},function(err,result){
        if(err){console.log("getAllDestinations",err);}
        $scope.destinations = result;
    })

    $scope.cancel = function(){
        $state.go('admin.journals');
    }

    $scope.language_change = function(){
        $scope.journal.language = $scope.languages[$scope.journal.language_code];
    }

    $scope.UpdateJournal = function() {
        $scope.error = null;
        if ($scope.editJournalForm.$valid){
/*            if ($scope.user.local.password != undefined || $scope.user.local.confirmPassword != undefined) {
                 if ($scope.user.local.password != $scope.user.local.confirmPassword) {
                        $scope.error = ['Password doesn\'t match '];
                        return;
                }
            }*/

            if($scope.temp_file){
                fileupload.uploadfile($scope.temp_file,'journals',function(err,result){
                    if(err){ console.log("file not uploaded",err);}
                    else{
                        $scope.journal.image_url = result;
                        //console.log('$scope.journal.image',$scope.journal.image_url);
                             journals.updatejournal($state.params.id,$scope.journal,function(err,result){
                                if (err) { 
                                    if(err.message){
                                        $scope.error = err.message;
                                    }else{
                                         dialogs.openErrorDialog(err);
                                    }
                                    return;
                                }else{
/*                                    if(result._id == $rootScope.loggedInUser._id){
                                        $rootScope.loggedInUser = result;
                                        localStorageService.set('yolo_itinerary_client',result);
                                    }*/
                                    dialogs.openMessageDialog('Journal Updated Successfully');
                                    $state.go('admin.journals');
                                }

                            })
                    }

                })
            }else{
                journals.updatejournal($state.params.id,$scope.journal,function(err,result){
                    if (err) { 
                        if(err.message){
                            $scope.error = err.message;
                        }else{
                             dialogs.openErrorDialog(err);
                        }
                        return;
                    }else{
/*                        if(result._id == $rootScope.loggedInUser._id){
                            $rootScope.loggedInUser = result;
                            localStorageService.set('yolo_itinerary_client',result);
                        }*/
                        dialogs.openMessageDialog('Journal Updated Successfully');
                        $state.go('admin.journals');
                    }

                })
            }



        }else{
            $scope.error = "Form Validation Failing.."
        }
    }

}]);