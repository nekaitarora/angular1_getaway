app.controller('showCommentsCtrl', ['$scope','comments','$state','dialogs','uiGridConstants', function($scope,comments,$state,dialogs,uiGridConstants){

//default config for pagination work
	$scope.count = 0;
	$scope.currentpage =1 ;
	$scope.limit =10;

// 	//get fields for showing
	$scope.config = {
	'fields' : {'description' : 1,'publish' : 1,'created_on' :1,'journal' :1},
	'filter':{journal: $state.params.id},
		'sort' :{'created_on' :-1},
		'skip' : 0,
		'limit' : $scope.limit,
	};
	$scope.comments = {};
	$scope.comments ={description : '', publish: '', created_on: '', journal: ''};
   
	//grid fields definition
	$scope.commentsGrid= {
     	paginationPageSize: 5,
     	useExternalPagination: true,
		columnDefs : [
		{displayName : "Description" , field : "description"},
		{displayName : "Publish" , field : "publish"},
		{displayName : "Created" , field : "created_on", type: 'date', cellFilter: 'date:\"MM/dd/yyyy h:mma\"' },
		{displayName : "Operations" , field : "operations",cellTemplate: '<button ng-click="grid.appScope.editComments(row.entity)" class="btn btn-xs btn-warning table_btns">Edit</button><button class="btn btn-xs btn-danger table_btns" ng-click="grid.appScope.deleteComments(row.entity,rowRenderIndex)">Delete</button>',enableSorting: false },
		]
	}
   $scope.commentsGrid.enableHorizontalScrollbar = uiGridConstants.scrollbars.NEVER;
   $scope.commentsGrid.enableVerticalScrollbar = uiGridConstants.scrollbars.NEVER;

	// 	//default set comments
	getcount();
	get_data();

	//get total pages count
	$scope.getTotalPages = function(){
		return $scope.getTotalPagesCount = Math.ceil($scope.count / $scope.config.limit);
	}

	//next page work
	$scope.nextPage = function(){
		if($scope.currentpage < $scope.getTotalPagesCount){
			$scope.currentpage++;
			$scope.config.skip = $scope.config.skip + $scope.limit;
			get_data();	
		}
	}
	
	//previous page work
	$scope.previousPage = function(){
		if($scope.currentpage > 1 ){
			$scope.currentpage--;
			$scope.config.skip = $scope.config.skip - $scope.limit;
			get_data();	
		}
	}
	function getcount(){
		//get total count of comments
		comments.getallCommentsCount($scope.config,function(err,result){
			if(err){console.log(err);return;}
			else{
				$scope.count = result.count;
				$scope.getTotalPages();
			}
		})
	}

	function get_data(){
		comments.getallComments($scope.config,function(err,result){
			if(err){$scope.error = err; console.log(err); return;}
			$scope.commentsGrid.data = result;
			getcount();

		})
	}

//Delete comments
	$scope.deleteComments =  function(comment, index){
		dialogs.openConfirmDialog('Are you sure want to delete comment? ','', function(yes,no){
	      if(no){
	        console.log("Delete comment");
	        return;
	      }else{
	      	console.log('auhbjasugas');
	      	console.log('comment id is',comment._id);
	      	comments.deleteComment(comment._id,function(err,result){
	      		if(err){	
					dialogs.openErrorDialog(err);return;
	      		}else{
	      			$scope.commentsGrid.data.splice(index,1);	
	      		}
	      	})
	      }
	    })		
	}

	$scope.searchComments = function(){
		delete $scope.config.publish;

		if($scope.comments.publish != ''){
			if( $scope.comments.publish =='true'){
				$scope.config.publish= true;
			}else if( $scope.comments.publish =='false'){
				$scope.config.publish= false;
			}
		}
		
		comments.getallComments($scope.config,function(err,result){
			if(err){$scope.error = err; console.log(err); return;}
			$scope.commentsGrid.data = result;
		})

	}

	$scope.disableFilter = function(){
		delete $scope.config.publish;
		$scope.comments ={};
		get_data();
	}

// //edit page
	$scope.editComments =  function(comments){
		 $state.go('admin.editComments', {'id': comments._id});
	};


























}]);

