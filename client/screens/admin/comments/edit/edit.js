app.controller('commentsEditCtrl', ['$scope','journals','comments','dialogs','$state','fileupload', function($scope,journals,comments,dialogs,$state,fileupload){
    $scope.comments={};
    comments.getComments($state.params.id,function(err,result){
        if(err){dialogs.openErrorDialog(err);return}
        else{
            result.status = String(result.status);
            result.isadmin = String(result.isadmin);
            $scope.comments = result;
        }
    })

    $scope.cancel = function(){
        $state.go('admin.showcomments',{'id': $scope.comments.journal});
    }    

    $scope.updateComments = function() {
        $scope.error = null;
        if ($scope.createForm.$valid){

/*            if($scope.temp_file){
                fileupload.uploadfile($scope.temp_file,'journals',function(err,result){
                    if(err){ console.log("file not uploaded",err);}
                    else{
                        $scope.journal.image = result;
                             journals.updatejournal($state.params.id,$scope.journal,function(err,result){
                                if (err) { 
                                    if(err.message){
                                        $scope.error = err.message;
                                    }else{
                                         dialogs.openErrorDialog(err);
                                    }
                                    return;
                                }else{

                                    dialogs.openMessageDialog('Journal Updated Successfully');
                                    $state.go('admin.journals');
                                }

                            })
                    }

                })
            }else{*/
                comments.updateComment($state.params.id,$scope.comments,function(err,result){
                    if (err) { 
                        if(err.message){
                            $scope.error = err.message;
                        }else{
                             dialogs.openErrorDialog(err);
                        }
                        return;
                    }else{
                        dialogs.openMessageDialog('comments Updated Successfully');
                        $state.go('admin.showcomments',{'id': result.journal});
                    }

                })
        }
        else{
            $scope.error = "Form Validation Failing.."
        }
    }

}]);