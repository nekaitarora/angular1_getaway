app.controller('singleAttractionCtrl', ['$scope', 'attractions', '$window', '$location', 'journals', 'users', 'tags', 'comments', '$rootScope', 'dialogs', '$state','$filter','localStorageService','itinerarys', function($scope, attractions, $window, $location, journals, users, tags, comments, $rootScope, dialogs, $state,$filter,localStorageService,itinerarys) {
$scope.allDays = {0:"sunday",1:"monday",2:"tuesday",3:"wednesday",4:"thursday",5:"friday",6:"saturday"};
    
    $scope.rating1 = 1;
    $scope.isReadonly = true;
    // $scope.rating=1;
    $scope.currentReviewDat = [];
    $scope.allattractions = [];
    $scope.stars=[1,2,3,4,5];
     $scope.ratingValue = 1;
    $scope.timing = [];
    $scope.attractionItinerary = [];

    $scope.myreview = false;

  $scope.bookmark = function(aid){
    if($rootScope.loggedInUser){
      attractions.bookmarkAttraction(aid,function(err,result){
        if(err){return console.log(err);}
        if(result.nModified > 0){
          var user = localStorageService.get('yolo_itinerary_client');
          user.bookmark_attraction ? user.bookmark_attraction.push(aid) : user.bookmark_attraction= [aid] ;    
          $rootScope.loggedInUser = user;
          localStorageService.set('yolo_itinerary_client',user);
        }
      })
    }else{
      dialogs.openErrorDialog("Please Login!")
    }
  }
    

$scope.gotoAttraction = function(_id){
    $state.go('index.singleAttraction',{'id': _id})
}

$scope.changeimage = function(url){
    console.log('url',url);
    $scope.img = url;
}

$scope.config = {
    'fields' : {},
    'filter':{},
    'sort' :{rating:-1},
        'skip' : 0,
        'limit' : 3,
};

$scope.reviewConfig = {
    'fields' : {},
    'filter':{},
    'sort' :{rating:-1},
    'skip' : 0,
    'limit' : 3,
};

$scope.itineraryConfig = {
    'fields' : {title: 1,attractions: 1,_id: 1,machine_name: 1,image: 1},
    'filter':{featured:true},
    'sort' :{created_on:-1},
    'skip' : 0,
    'limit' : 3,
}

$scope.loggedUser = localStorageService.get('yolo_itinerary_client');
console.log('the logged in user is',$scope.loggedUser);

    $scope.attractiondata = function() {
        attractions.getAttraction($state.params.id, function(err, result) {
            if (err) { 
            console.log('can not get id');
            dialogs.openErrorDialog('Sorry! there is no such attraction that exists now');
            $state.go('index.home');
             } else {
                    if(!result._id){
                        $state.go('index');
                    }
                 else{   
                console.log('result123',result);
                if(result.published == false){

                    if(!$scope.loggedUser || result.created_by != $scope.loggedUser._id){
                        if(!$scope.loggedUser.isadmin){
                    dialogs.openErrorDialog('sorry! This attraction does not exists');
                    $state.go('index.home');
                }
                // }
            }
            }
                $scope.attraction = result;
                $scope.attractionCity = result.city;
                $scope.attractionCategory = result.category;
                console.log('attraction id is',$scope.attractionCity,$scope.attractionCategory);
                $scope.img = result.image_url[0];
                // console.log($scope.attraction);
                $scope.rating = result.rating;

            for (var a = 0; a < result.openclosetime.length; a++) {
                if(result.openclosetime[a].open.time.indexOf(':') == -1){
                    console.log('yelp attraction');
                    var c = result.openclosetime[a].open.time.toString().split("");
                    b = c[0] + c[1];
                    d = c[2] + c[3];
                    opentime = b+":"+d;

                    var e = result.openclosetime[a].close.time.toString().split("");
                    f = e[0] + e[1];
                    g = e[2] + e[3];
                    closetime = f+":"+g;
                    // console.log('c is ',closetime,opentime);
                    var open_time  = opentime;
                    var close_time = closetime;

                    var opening = new Date();
                    opening.setHours(open_time.split(':')[0]);
                    opening.setMinutes(open_time.split(':')[1]);
                    var opendate = $filter('date')(opening, 'yyyy-MM-dd hh:mm:a');

                    var closing = new Date();
                    closing.setHours(close_time.split(':')[0]);
                    closing.setMinutes(close_time.split(':')[1]);
                    var closedate = $filter('date')(closing, 'yyyy-MM-dd hh:mm:a');
                // console.log('hello', typeof opening);
                }
                else if(result.openclosetime[a].open.time.indexOf(':') != -1){
                    console.log('user attraction');
                    var opening = new Date();
                    opening.setHours(result.openclosetime[a].open.time.split(':')[0]);
                    opening.setMinutes(result.openclosetime[a].open.time.split(':')[1]);
                    var opendate = $filter('date')(opening, 'yyyy-MM-dd hh:mm:a');

                    var closing = new Date();
                    closing.setHours(result.openclosetime[a].close.time.split(':')[0]);
                    closing.setMinutes(result.openclosetime[a].close.time.split(':')[1]);
                    var closedate = $filter('date')(closing, 'yyyy-MM-dd hh:mm:a');
                }

                $scope.timing[a] = {};
                $scope.timing[a].opentiming = new Date(opendate);
                $scope.timing[a].day = result.openclosetime[a].open.day;
                $scope.timing[a].closetiming = new Date(closedate);

            }

                // $scope.rating1 = result.rating * 20;
                $scope.config.filter.city = result.city._id;
                $scope.config.filter._id = {$ne:result._id};
                $scope.allAttractions();
            }
        }
        })
    }

    $scope.reviewsData = function(){
        $scope.reviewConfig.filter.attraction_id = $state.params.id;
        attractions.getReviews($scope.reviewConfig,function(err,result){
            if(err) { dialogs.openErrorDialog(err);
                return
            }
            else{
                console.log('result of review is',result);
                if(result.length > 0){
                $scope.currentReviewData = result[0].reviews_data;
                var currentUserReview = _.findWhere($scope.currentReviewData,{"user_id":$scope.loggedUser._id})
                if(currentUserReview){
                    $scope.rating1 = currentUserReview.rating;
                }
                if (_.some($scope.currentReviewData, function(o) { return _.has(o, "description"); })) {
                    $scope.reviewDesc = true;
                }
            }
        }
        })
    }

$scope.allAttractions = function(){
    $scope.config.filter.city = $scope.attractionCity;
    $scope.config.filter.category = $scope.attractionCategory;
    $scope.config.filter.published = true;
    attractions.getAllAttractions($scope.config,function(err,result){
        if(err) { dialogs.openErrorDialog(err);
            return
        }
        else{
            console.log('result',result);
            $scope.allattractions = result;
            }
    })
}

$scope.attractiondata();
$scope.reviewsData();


$scope.editattraction = function(){
    $state.go('index.editAttraction', {'id': $state.params.id});
}

$scope.deleteAttraction = function(){
    console.log('click to delete attraction');
    attractions.deleteAttraction($state.params.id,function(err,result){
        if(err){return err;}
        if(result.length > 0){
        console.log('result is itinerary');
        dialogs.openMessageDialog('Sorry! This attraction can not be deleted because it is being used in itinerary');
    }
    else{
        console.log('result of deleted');
        dialogs.openMessageDialog('Your attraction has been deleted');
        $state.go('index.profile');
    }
    })
}



    // $scope.attraction.rating = 1;
    $scope.rateFunction = function(rating) {
        if(!$rootScope.loggedInUser){
            dialogs.openErrorDialog('please Login First');
            // $scope.rating1 = 1;
        }
        else{
        console.log('Rating selected: ' + rating);
        var data = {};
        data._id = $state.params.id;
        data.user_id = $rootScope.loggedInUser._id;
        data.username = $rootScope.loggedInUser.profile_fields.first_name;
        data.rating = rating;
        dialogs.openConfirmDialog('Are you sure want to Rate this Attraction ?','', function(yes,no){
          if(no){
            $scope.rating1 = 1;
            // $window.location.reload();
            return;
          }else{
            attractions.updateReviews(data,function(err,result){
                if(err) {return err};
                console.log('result is',result);
                // $scope.rating1 = 1;
                $scope.averageRating();


            })
          }
        })
    }
    };

    
    $scope.updateReview = function(){
        if(!$rootScope.loggedInUser){
            dialogs.openErrorDialog('please Login First');
            $scope.reviewDescription ="";
        }
        else{        
        console.log('i am here');
        var data = {};
        data._id = $state.params.id;
        data.description = $scope.reviewDescription;
        data.user_id = $rootScope.loggedInUser._id;
        data.username = $rootScope.loggedInUser.profile_fields.first_name;
        if($scope.reviewDescription){
        attractions.updateReviews(data,function(err,result){
            if(err) {return err};
            $scope.myreview =  $scope.reviewDescription;
            dialogs.openMessageDialog("Your Review Has been saved successfully");
            $scope.reviewsData();
            $scope.reviewDescription = '';
        })        
    }
    else
    {
        console.log('enter description first');
        dialogs.openErrorDialog("Please enter your review before submitting it");
        $scope.reviewDescription = '';
    }
    }
    } 

    $scope.averageRating = function(){
        attractions.getAverageRating($state.params.id,function(err,result){
            if(err) {return err};
            console.log('result ratingValue',result);
            $scope.ratingValue = result.rating;    
            // $scope.ratingValue = 0;
        //     if(result.length > 0){
        //     console.log('average rating',result[0].avgRating);
        //     $scope.ratingValue = result[0].avgRating;

        // }
        // else{
        //     // $scope.ratingValue = 1;
        //     $scope.ratingValue = result.rating;
        // }
        })
    }   

$scope.averageRating();

    $scope.allItinerarys = function(){
        $scope.itineraryConfig.filter.attraction = $state.params.id;
        itinerarys.getFilteredItineraries($scope.itineraryConfig,function(err,result){
            if(err) {return err};
            console.log('result of itinerary is',result);
            $scope.attractionItinerary = result;
        })
    }

$scope.allItinerarys();

}])
