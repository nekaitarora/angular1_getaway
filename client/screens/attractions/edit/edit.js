app.controller('editAttractionCtrl', ['$scope', 'attractions', 'categories', 'destinations', 'utils', '$state', 'dialogs', 'fileupload', '$filter','dataService', function($scope, attractions, categories, destinations, utils, $state, dialogs, fileupload, $filter,dataService) {
    $scope.filers = {};
    $scope.ratings = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5];
    $scope.allDays = {0:"sunday",1:"monday",2:"tuesday",3:"wednesday",4:"thursday",5:"friday",6:"saturday"};
    $scope.timing = [];
    $scope.image_url = [];
    $scope.rating1 = 4;
    $scope.isReadonly = true;
    $scope.attraction = {};
    $scope.attraction.rating = 1;

    $scope.clickme = function() {
        $scope.timing.push({});
    }

    $scope.removeImage = function(index){
        console.log('the image is',$scope.image_url);
        $scope.image_url.splice(index,1)
    }    

	$scope.removeopenclosetime = function(index){
		$scope.timing.splice(index,1);
	}

    $scope.cancel = function(){
        $state.go('index.singleAttraction', {'id': $state.params.id});
    }

    categories.getAllCategories(function(err, result) {
        if (err) { console.log(err); }
        $scope.categories = result;
    })

    destinations.getAllDestinations({}, function(err, result) {
        if (err) { console.log("getAllDestinations", err); }
        $scope.destinations = result;
    })

    utils.getAllLanguages(function(err, result) {
        if (err) { console.log("getAllLanguages", err); }
        $scope.languages = result;
    })

    attractions.getAttraction($state.params.id, function(err, result) {
        if (err) {
            dialogs.openErrorDialog("Wrong Attraction Id");
            $state.go('index.singleAttraction', {'id': $state.params.id});
        } else {
            $scope.attraction = result;
            $scope.image_url = $scope.attraction.image_url;
            console.log('image_url is',$scope.image_url);
            $scope.showMap($scope.attraction.latitude,$scope.attraction.longitude);
            for (var a = 0; a < result.openclosetime.length; a++) {

                var opening = new Date();
                opening.setHours(result.openclosetime[a].open.time.split(':')[0]);
                opening.setMinutes(result.openclosetime[a].open.time.split(':')[1]);
                var opendate = $filter('date')(opening, 'yyyy-MM-dd hh:mm:a');

                var closing = new Date();
                closing.setHours(result.openclosetime[a].close.time.split(':')[0]);
                closing.setMinutes(result.openclosetime[a].close.time.split(':')[1]);
                var closedate = $filter('date')(closing, 'yyyy-MM-dd hh:mm:a');
                // console.log('hello', typeof opening);
                $scope.timing[a] = {};
                $scope.timing[a].opentiming = new Date(opendate);
                $scope.timing[a].day = result.openclosetime[a].open.day;
                $scope.timing[a].closetiming = new Date(closedate);
            }
        }
        if($scope.timing.length > 0){
        	$scope.attraction.is_always_open = 'false';
        }
        else{
        	$scope.attraction.is_always_open = 'true';
        }
    })





// var myCenter;
$scope.cityPlace = function(location){
    console.log('location',location);
    if(location.location){
    $scope.citylat = location.location.lat;
    $scope.citylng = location.location.lng;
    // console.log('loc',location);
    myCenter = new google.maps.LatLng($scope.citylat,$scope.citylng);

        $scope.showMap($scope.citylat,$scope.citylng);
    }
    else{
        console.log('no location found');
        myCenter = new google.maps.LatLng();   
    }
}

    var marker;
     $scope.placeMarker =function(location) {
        if (marker && marker.setMap) {
            marker.setMap(null);
        }
        marker = new google.maps.Marker({
            position: location,
            map: map,
    })


        var infowindow = new google.maps.InfoWindow({
            content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
        });
        $scope.attraction.longitude = location.lng();
        $scope.attraction.latitude = location.lat();
        console.log('location is ',$scope.attraction.latitude,$scope.attraction.longitude);
        infowindow.open(map,marker);

    }

    $scope.showMap = function(longitude,latitude) {
        var myCenter = new google.maps.LatLng($scope.attraction.latitude,$scope.attraction.longitude);
        var mapProp = {
            center: myCenter,
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        console.log('checking',new google.maps.LatLng(longitude,latitude));
        $scope.placeMarker(new google.maps.LatLng(longitude,latitude));
        google.maps.event.addListener(map, 'click', function(event) {
            $scope.placeMarker(event.latLng);
            console.log(event.latLng);
            $scope.$digest();
        });
    }

   
    $scope.current = function() {
        console.log('hello');

        navigator.geolocation.getCurrentPosition(function(position) {
                console.log('hello', position);
                $scope.attraction.longitude = position.coords.longitude;
                $scope.attraction.latitude = position.coords.latitude;
            })
            
    }
    $scope.checkaddress = function(address) {
        console.log(address);
        dataService.getCoordinatesFromAddress(address, function(err, data) {
            console.log(data);
            $scope.attraction.longitude = data.results[0].geometry.location.lat;
            $scope.attraction.latitude = data.results[0].geometry.location.lng;
            myCenter = new google.maps.LatLng($scope.attraction.latitude,$scope.attraction.longitude); 
            $scope.showMap($scope.attraction.longitude,$scope.attraction.latitude);
        });
    }





    $scope.language_change = function() {
        $scope.attraction.language = $scope.languages[$scope.attraction.language_code];
    }

	$scope.alwaysopen = function(){
		if($scope.attraction.is_always_open == 'true'){
			if($scope.timing.length > 0){
				dialogs.openConfirmDialog('Warning Message','Your custom time settings will be erased. Do you want to continue?', function(yes,no){
	      			if(no){
	        			$scope.attraction.is_always_open ? $scope.attraction.is_always_open = 'false' : $scope.attraction.is_always_open = 'true';
	        		return;
	      			}else{
	      				$scope.attraction.is_always_open ? $scope.attraction.is_always_open = 'true' : $scope.attraction.is_always_open = 'false';
	      			}
	    		})				
			}
		}
	}

    $scope.updateAttraction = function() {
        var days1 = [];
        console.log('previous images are ',$scope.image_url);

		if($scope.attraction.is_always_open == 'true'){
			$scope.attraction.is_always_open = true;
			delete $scope.timing;
		}
        $scope.attraction.openclosetime = [];
        if($scope.timing){
        for (var i = 0; i < $scope.timing.length; i++) {
            days1[i] = {};
            days1[i].day = $scope.timing[i].day;
            days1[i].opentiming = $filter('date')($scope.timing[i].opentiming, 'HH:mm');
            days1[i].closetiming = $filter('date')($scope.timing[i].closetiming, 'HH:mm');
        }
    }
        $scope.attraction.openclosetime = days1;
        if ($scope.EditAttractionForm.$valid) {
            console.log('hello123');
            if ($scope.temp_file) {
                console.log('temp file is',$scope.temp_file.name);
                $scope.error = false;
                fileupload.uploadfile($scope.temp_file, 'attractions', function(err, result) {
                    if (err) { console.log("file not uploaded", err); } else {
                        var newImageArray = $scope.image_url.concat(result);
                        console.log('newImageArray',newImageArray);
                        $scope.attraction.image_url = newImageArray;
                        attractions.updateAttraction($scope.attraction, function(err, result) {
                            if (err) {
                                if (err.message) {
                                    return $scope.error = err.message;
                                } else {
                                    return dialogs.openErrorDialog(err);
                                }
                            } else {
                                dialogs.openMessageDialog("Attraction Updated");
                                $state.go('index.singleAttraction', {'id': $state.params.id});
                            }
                        })
                    }
                });
            } else {
                console.log('hello12345');
                attractions.updateAttraction($scope.attraction, function(err, result) {
                    if (err) {
                        if (err.message) {
                            return $scope.error = err.message;
                        } else {
                            return dialogs.openErrorDialog(err);
                        }
                    } else {
                        dialogs.openMessageDialog("Attraction Updated");
                        $state.go('index.singleAttraction', {'id': $state.params.id});
                    }
                })
            }
        } else {
            console.log($scope.EditAttractionForm);
            $scope.submitted = true;
        }
    }

}]);
