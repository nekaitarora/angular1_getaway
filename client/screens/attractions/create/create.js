app.controller('createAttractionCtrl', ['$scope', '$rootScope', 'attractions', 'categories', 'destinations', 'utils', '$state', 'dialogs', 'fileupload', '$filter', 'dataService', function($scope, $rootScope, attractions, categories, destinations, utils, $state, dialogs, fileupload, $filter, dataService) {
    $scope.filers = {};
    $scope.timing = [];
    $scope.openclose = [];
    $scope.count = 0;
    $scope.attraction = {};
    $scope.ratings = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5];
    $scope.allDays = {0:"sunday",1:"monday",2:"tuesday",3:"wednesday",4:"thursday",5:"friday",6:"saturday"};
    $scope.attraction.is_always_open = "true";

var myCenter;
$scope.cityPlace = function(location){
    console.log('location',location);
    if(location.location){
    $scope.citylat = location.location.lat;
    $scope.citylng = location.location.lng;
    // console.log('loc',location);
    myCenter = new google.maps.LatLng($scope.citylat,$scope.citylng);

        $scope.showMap($scope.citylat,$scope.citylng);
    }
    else{
        console.log('no location found');
        myCenter = new google.maps.LatLng();   
    }
}

    var marker;
     $scope.placeMarker =function(location) {
        if (marker && marker.setMap) {
            marker.setMap(null);
        }
        marker = new google.maps.Marker({
            position: location,
            draggable: true,
            map: map,
    })


        var infowindow = new google.maps.InfoWindow({
            content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
        });
        $scope.attraction.longitude = location.lng();
        $scope.attraction.latitude = location.lat();
        console.log('location is ',$scope.attraction.latitude,$scope.attraction.longitude);
        infowindow.open(map,marker);

    }

    $scope.showMap = function(longitude,latitude) {
        var mapProp = {
            center: myCenter,
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        console.log('checking',new google.maps.LatLng(longitude,latitude));
        $scope.placeMarker(new google.maps.LatLng(longitude,latitude));
        google.maps.event.addListener(map, 'click', function(event) {
            $scope.placeMarker(event.latLng);
            console.log(event.latLng);
            $scope.$digest();
        });
    }

    $scope.current = function() {
        console.log('hello');

        navigator.geolocation.getCurrentPosition(function(position) {
                console.log('hello', position);
                $scope.attraction.longitude = position.coords.longitude;
                $scope.attraction.latitude = position.coords.latitude;
            })
            
    }
    $scope.checkaddress = function(address) {
        console.log(address);
        dataService.getCoordinatesFromAddress(address, function(err, data) {
            console.log(data);
            $scope.attraction.longitude = data.results[0].geometry.location.lat;
            $scope.attraction.latitude = data.results[0].geometry.location.lng;
            myCenter = new google.maps.LatLng($scope.attraction.latitude,$scope.attraction.longitude); 
            $scope.showMap($scope.attraction.longitude,$scope.attraction.latitude);
        });
    }

    $scope.attraction.rating = 1;
    $scope.rateFunction = function(rating) {
        console.log('Rating selected: ' + rating);
    };

    $scope.clickme = function() {
        $scope.openclose.push({});
    }

    $scope.removeopenclosetime = function(index) {
        $scope.openclose.splice(index, 1);
        $scope.timing.splice(index, 1);
    }

    $scope.cancel = function() {
        $state.go('index.profile');
    }

    categories.getAllCategories(function(err, result) {
        if (err) { console.log("getAllCategories", err); }
        $scope.categories = result;
    })

    destinations.getAllDestinations({}, function(err, result) {
        if (err) { console.log("getAllDestinations", err); }
        $scope.destinations = result;
    })

    utils.getAllLanguages(function(err, result) {
        if (err) { console.log("getAllLanguages", err); }
        $scope.languages = result;
    })

    $scope.language_change = function() {
        $scope.attraction.language = $scope.languages[$scope.attraction.language_code];
    }

    $scope.alwaysopen = function() {
        if ($scope.attraction.is_always_open == 'true') {
            console.log('what?');
            if ($scope.timing.length > 0) {
                dialogs.openConfirmDialog('Warning Message', 'Your custom time settings will be erased. Do you want to continue?', function(yes, no) {
                    if (no) {
                        $scope.attraction.is_always_open ? $scope.attraction.is_always_open = 'false' : $scope.attraction.is_always_open = 'true';
                        return;
                    } else {
                        $scope.attraction.is_always_open ? $scope.attraction.is_always_open = 'true' : $scope.attraction.is_always_open = 'false';
                    }
                })
            }
        }
    }

    $scope.createAttraction = function() {
        $scope.count = 1;
        var days1 = [];
        if ($scope.CreateAttractionForm.$valid) {
            if ($scope.attraction.is_always_open == 'true') {
                $scope.attraction.is_always_open = true;
                console.log('open');
                delete $scope.timing;
            }

            if ($scope.timing) {
                $scope.attraction.openclosetime = [];
                for (var i = 0; i < $scope.timing.length; i++) {
                    days1[i] = {};
                    days1[i].day = $scope.timing[i].day;
                    days1[i].opentime = $filter('date')($scope.timing[i].opentime, 'HH:mm');
                    days1[i].closetime = $filter('date')($scope.timing[i].closetime, 'HH:mm');
                }
            }

            $scope.attraction.openclosetime = days1;
            $scope.error = false;
            console.log($scope.temp_file);
            fileupload.uploadfile($scope.temp_file, 'attractions', function(err, result) {
                if (err) { console.log("file not uploaded", err); } else {
                    $scope.attraction.image_url = result;
                    console.log('$scope.attraction is',$scope.attraction);
                    attractions.createAttraction($scope.attraction, function(err, result) {
                        if (err) {
                            if (err.message) {
                                return $scope.error = err.message;
                            } else {
                                return dialogs.openErrorDialog(err);
                            }
                        } else {
                            console.log('result',result);
                            dialogs.openMessageDialog("Attraction Created");
                            $state.go("index.profile");
                        }
                    })
                }
            });
        } else {
            $scope.submitted = true;
        }
    }

}]);


app.directive('starRating', function() {
    return {
        restrict: 'EA',
        template: '<ul class="star-rating" ng-class="{readonly: readonly}">' +
            '  <li ng-repeat="star in stars" class="star" ng-class="{filled: star.filled}" ng-click="toggle($index)">' +
            '    <i class="fa fa-star"></i>' + // or &#9733
            '  </li>' +
            '</ul>',
        scope: {
            ratingValue: '=ngModel',
            max: '=?', // optional (default is 5)
            onRatingSelect: '&?',
            readonly: '=?'
        },
        link: function(scope, element, attributes) {
            if (scope.max == undefined) {
                scope.max = 5;
            }

            function updateStars() {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };
            scope.toggle = function(index) {
                if (scope.readonly == undefined || scope.readonly === false) {
                    scope.ratingValue = index + 1;
                    scope.onRatingSelect = ({
                        rating: index + 1
                    });
                }
            };
            scope.$watch('ratingValue', function(oldValue, newValue) {
                if (oldValue == 1 || oldValue != newValue) {
                    updateStars();
                }
            });
        }
    };
});


