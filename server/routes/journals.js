var express = require('express'),
    router = express.Router(),
    journals_db = require('../libs/journals/journals_db'),
    journals_api = require('../libs/journals/journals_api'),
    comments_db = require('../libs/comments/comments_db'),
    comments_api = require('../libs/comments/comments_api'),
    users_api = require('../libs/users/users_api'),
    tags_api = require('../libs/tags/tags_api'),
    tags_db = require('../libs/tags/tags_db'),
    mail = require('../config/mail'),
    passport = require('passport'),
    dbconfig = require('../config/global_config'),
    _ = require("underscore"),
    async = require("async");


router.post('/create', function(req, res) {
    /*      if(!req.body.title || !req.body.description)
        {
            return res.status(200).send("Please send title and description");
        }*/
    if (req.user && req.user._id) {
        console.log('result of create journal is',req.body);
        var tagArray;
        var journal = {};
        journal = req.body;
        journal.created_by = req.user._id;
        journal.destinations = req.body.destinations._id;
        // journal.itineraries = req.body.itineraries._id;
        // console.log("journal tags",journal);

        async.map(journal.tags, function(item, callback) {
            if (!item._id) {
                var name1 = item.name;
                tags_db.findOne({ 'name': name1 }, function(err, data) {
                    if (err) throw err;
                    //console.log('hello found');
                    if (data) {
                        item['_id'] = data._id;
                        callback(null, item);
                    } else {

                        tags_api.createTags(item, function(err, result) {
                            if (err) {
                                callback(err);
                            } else {
                                item['_id'] = result._id
                                callback(null, item);
                            }
                        })
                    }

                });
            } else {
                item = item;
                callback(null, item);
            }

        }, function(err, results) {
            // console.log(results);
            // console.log('journal after process',journal);
            journals_api.createJournal(journal, function(err, result) {
                if (err) {
                    return res.status(400).send(err); }
                if (!req.user.isadmin || !req.user.superadmin) {
                    mail.approveJournal(result._id, req.user);
                }
                return res.status(200).send(result);
            })
        })
    }
})

// journals_api.checkIfExist(req.user._id, function(err, result)
// {
//  if(err){return res.status(400).send(err);}
//  if(result.length == 0)
//  {
//  return res.status(400).send('try again with a valid id');
//  }
//  else
//  {
//      if(req.user.isadmin  || req.user.superadmin)
//      {
//              journals_api.createJournal(journal, function(err, result) {
//              if (err) {return res.status(400).send(err);}
//              journals_db.findById(result._id,function(err,data){
//                  mail.approveJournal(result._id,req.user);
//              })
//              return res.status(200).send(result);
//              });
//      }
//      else
//      {
//          console.log('user data here is',req.user);
//          var verification_code = Math.floor(Math.random() * 90000) + 10000;
//              journals_api.createJournal(journal, function(err, result) {
//              if (err) {return res.status(400).send(err);}
//              mail.approveJournal(verification_code,req.user.local.email);
//              return res.status(200).send(result);
//              });     
//      }
//  }
// })



router.get('/all', function(req, res) {
    var Query = {};
    if (Object.keys(req.query).length > 0) {
        Query = JSON.parse(req.query.query);
    }
    journals_api.getall(Query, function(err, result) {
        if (err) {
            return res.status(400).send(err); }
        return res.status(200).send(result);
    })
})

router.get('/all/count', function(req, res) {
    var Query = {};
    if (Object.keys(req.query).length > 0) {
        Query = JSON.parse(req.query.query);
    }
    journals_api.journalsCount(Query, function(err, data) {
        if (err) {
            return res.status(400).send(err); }
        return res.status(200).json({ count: data });
    })
})

router.get('/:id', function(req, res) {
    journals_api.getjournal(req.params.id, function(err, data) {
        if (err) {
            return res.status(400).send({ message: err }); }
        data.views = data.views + 1;
        journals_api.createJournal(data, function(err, result) {
            if (err) throw err;
            // console.log('result now is', result);
        })
        console.log('data of journal is',data);
        return res.status(200).send(data);
    })
})

/*router.put('/:id', function(req, res) {
    journals_api.getjournal(req.params.id,function(err, data) {
        if(!req.user.isadmin && !req.user.issuperadmin && !req.user._id == req.params.id){
            return res.status(400).send('Permission Denied');
        }
        if(req.user._id.toString() == data.created_by.toString() || req.user.isadmin || req.user.issuperadmin){
            req.body.publish ? data.publish = req.body.publish : null;
            req.body.title ? data.title = req.body.title : null;
            req.body.description ? data.description = req.body.description : null;
            req.body.likeBy ? data.likeBy = req.body.likeBy :null;
            _.extend(data,req.body);
        }

                journals_api.updatejournal(data, function(err, data) {
                   if (err) {return res.status(400).send(err); }
             
                    return res.status(200).send(data);
                })
        });
    });*/



router.put('/:id', function(req, res) {
    journals_api.getjournal(req.params.id, function(err, data) {
        if (!req.user.isadmin && !req.user.issuperadmin && !req.user._id == req.params.id) {
            return res.status(400).send('Permission Denied');
        }
        if (req.user._id.toString() == data.created_by.toString() || req.user.isadmin || req.user.issuperadmin) {
            async.map(req.body.tags, function(item, callback) {
                if (!item._id) {
                    var name1 = item.name;
                    tags_db.findOne({ 'name': name1 }, function(err, data) {
                        if (err) throw err;
                        if (data) {
                            item['_id'] = data._id;
                            callback(null, item);
                        } else {
                            tags_api.createTags(item, function(err, result) {
                                if (err) {
                                    callback(err);
                                } else {
                                    item['_id'] = result._id
                                    callback(null, item);
                                }
                            })
                        }

                    });
                } else {
                    item = item;
                    callback(null, item);
                }
            }, function(err, results) {
                journals_api.updatejournal(req.body, function(err, data) {
                    if (err) {
                        return res.status(400).send(err); }
                    return res.status(200).send(data);
                })
            })
        }
    });
});






router.put('/like/:id', function(req, res) {
    // console.log('req.body here is', req.body.likeBy);
    journals_api.getjournal(req.params.id, function(err, data) {
        if (!req.user.isadmin && !req.user.issuperadmin && !req.user._id == req.params.id) {
            return res.status(400).send('Permission Denied');
        }
        // data.likeBy = req.body.likeBy;
        //  //data._id = req.params.id;
        //  _.extend(data,req.body);


        journals_api.likejournal(data, req.body.likeBy, function(err, data) {
            if (err) {
                return res.status(400).send(err); }
            return res.status(200).send(data);
        })
    })
})


router.put('/unlike/:id', function(req, res) {
    // console.log('req.body', req.body);
    journals_api.getjournal(req.params.id, function(err, data) {
        if (!req.user.isadmin && !req.user.issuperadmin && !req.user._id == req.params.id) {
            return res.status(400).send('Permission Denied');
        }


        journals_api.unlikejournal(data, req.user, function(err, data) {
            if (err) {
                return res.status(400).send(err); }
            return res.status(200).send(data);
        })
    })
})

router.delete('/:id', function(req, res) {
    journals_api.deletejournal(req.params.id, function(err, data) {
        if (err) {
            return res.status(400).send(err); }
        comments_db.find({ 'journal': req.params.id }, function(err, result) {
            for (var i = 0; i < result.length; i++) {
                comments_db.remove({ 'journal': result[i].journal }, function(err, result) {
                    if (err) {
                        return res.status(400).send(err); }
                })
            }
        })
        return res.status(200).send("Journal Deleted");
    })
})
router.put('/bookmark/:journalid', function(req, res) {

    if (req.user) {
        users_api.bookmarkJournal(req.user, req.params.journalid, function(err, data) {
            if (err) {
                return res.status(400).send(err); }
            return res.status(200).send(data);
        })
    } else {
        return res.status(200).send({ message: "Please login!" });
    }
})


module.exports = router;
