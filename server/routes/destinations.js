/*
* All Users Routes after /api/cities/
*/
var express = require('express'),
	router = express.Router(),
	destinations_db = require('../libs/destinations/destinations_db'),
	destinations_api = require('../libs/destinations/destinations_api');
	attractions_api = require('../libs/attractions/attractions_api');
	categories_api = require('../libs/categories/categories_api');
var request = require('request');


/**
 * Demo no reponse 
 */

/*router.get('/insertforDemo', function(req, res) {
	var destinations = [
						{city:"台北市",country :"TW"},
						{city:"Istanbul",country :"TR"},
						{city:"Paris",country :"FR"},
						{city:"Marseille",country :"FR"},
						{city:"Venice",country :"IT"},
						{city:"Berlin",country :"DE"},
						{city:"Frankfurt am Main",country :"DE"},
						{city:"東京",country :"JP"},
						{city:"大阪市 ",country :"JP"},
						{city:"Auckland ",country :"NZ"},
						{city:"Singapore ",country :"SG"},
						{city:"香港 ",country :"HK"},
						{city:"London ",country :"GB"},
					];
	destinations.forEach(function(destination){
		destinations_api.createDestination(destination,function(err,result){
		})
	})

});*/

/**
 * @api {get} destinations/countries Show all destination countries
 * @apiGroup destinations
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    
 *{
 *   "AR": "Argentina (AR)",
 *   "AU": "Australia (AU)",
 *   "AT": "Austria (AT)",
 *   "BE": "Belgium (BE)",
 *   "BR": "Brazil (BR)",
 *   "CA": "Canada (CA)",
 *   "CL": "Chile (CL)",
 *   "CZ": "Czech Republic (CZ)",
 *   "DK": "Denmark (DK)",
 *   "FI": "Finland (FI)",
 *   "FR": "France (FR)",
 *   "DE": "Germany (DE)",
 *   "HK": "Hong Kong (HK)",
 *   "IT": "Italy (IT)",
 *   "JP": "Japan (JP)",
 *   "MY": "Malaysia (MY)",
 *   "MX": "Mexico (MX)",
 *   "NZ": "New Zealand (NZ)",
 *   "NO": "Norway (NO)",
 *   "PH": "Philippines (PH)",
 *   "PL": "Poland (PL)",
 *   "PT": "Portugal (PT)",
 *   "IE": "Republic of Ireland (IE)",
 *   "SG": "Singapore (SG)",
 *   "ES": "Spain (ES)",
 *   "SE": "Sweden (SE)",
 *   "CH": "Switzerland (CH)",
 *   "TW": "Taiwan (TW)",
 *   "NL": "The Netherlands (NL)",
 *   "TR": "Turkey (TR)",
 *   "GB": "United Kingdom (GB)",
 *   "US": "United States (US)"
 *}
 */
router.get('/countries', function(req, res) {
	var countries = {"AR":"Argentina (AR)","AU":"Australia (AU)","AT":"Austria (AT)","BE":"Belgium (BE)","BR":"Brazil (BR)","CA":"Canada (CA)","CL":"Chile (CL)","CZ":"Czech Republic (CZ)","DK":"Denmark (DK)","FI":"Finland (FI)","FR":"France (FR)","DE":"Germany (DE)","HK":"Hong Kong (HK)","IT":"Italy (IT)","JP":"Japan (JP)","MY":"Malaysia (MY)","MX":"Mexico (MX)","NZ":"New Zealand (NZ)","NO":"Norway (NO)","PH":"Philippines (PH)","PL":"Poland (PL)","PT":"Portugal (PT)","IE":"Republic of Ireland (IE)","SG":"Singapore (SG)","ES":"Spain (ES)","SE":"Sweden (SE)","CH":"Switzerland (CH)","TW":"Taiwan (TW)","NL":"The Netherlands (NL)","TR":"Turkey (TR)","GB":"United Kingdom (GB)","US":"United States (US)"};
	return res.status(200).send(countries);
});

/**
 * Create New Destination
 */
router.post('/create', function(req, res) {
	console.log('body request is',req.body);
		getCoordinates(req.body,function(error,data){
		if(error){
			console.log("error in getting location",error);
		}
		if(data){
			req.body.location = data;
		}

		destinations_api.createDestination(req.body , function(err,result){
			if(err){return res.status(400).send(err);}
			return res.status(200).send(result);
		})

	})

});

/**
 * Delete Destination
 */

router.delete('/:id', function(req, res) {
	destinations_api.deleteDestination(req.params.id , function(err,result){
		if(err){return res.status(400).send(err);}
		return res.status(200).send(result);
	})
});

/**
 * @api {get} destinations/ List all Destinations
 * @apiGroup destinations
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    
 * {
 *       "_id": "5719f52a74bb8c5b78d48729",
 *       "city": "台北市",
 *       "country": "TW",
 *       "__v": 0
 *   },
 *   {
 *       "_id": "5719f52a74bb8c5b78d4872a",
 *       "city": "İstanbul",
 *       "country": "TR",
 *       "__v": 0
 *   },
 *   {
 *       "_id": "5719f52a74bb8c5b78d4872b",
 *       "city": "Paris",
 *       "country": "FR",
 *       "__v": 0
 *   },
 *   {
 *       "_id": "5719f52a74bb8c5b78d4872c",
 *       "city": "Marseille",
 *       "country": "FR",
 *       "__v": 0
 *   },
 *   {
 *       "_id": "5719f52a74bb8c5b78d4872d",
 *       "city": "Venice",
 *       "country": "IT",
 *       "__v": 0
 *   },
 *   {
 *       "_id": "5719f52a74bb8c5b78d4872e",
 *       "city": "Berlin",
 *       "country": "DE",
 *       "__v": 0
 *   },
 *   {
 *       "_id": "5719f52a74bb8c5b78d4872f",
 *       "city": "Frankfurt am Main",
 *       "country": "DE",
 *       "__v": 0
 *   },
 *   {
 *       "_id": "5719f52a74bb8c5b78d48730",
 *       "city": "東京",
 *       "country": "JP",
 *       "__v": 0
 *   },
 *   {
 *       "_id": "5719f52a74bb8c5b78d48731",
 *       "city": "大阪市 ",
 *       "country": "JP",
 *       "__v": 0
 *   },
 *   {
 *       "_id": "5719f52a74bb8c5b78d48732",
 *       "city": "Auckland ",
 *       "country": "NZ",
 *       "__v": 0
 *   },
 *   {
 *       "_id": "5719f52a74bb8c5b78d48733",
 *       "city": "Singapore ",
 *       "country": "SG",
 *       "__v": 0
 *   },
 *   {
 *       "_id": "5719f52a74bb8c5b78d48734",
 *       "city": "香港 ",
 *       "country": "HK",
 *       "__v": 0
 *   },
 *   {
 *       "_id": "5719f52a74bb8c5b78d48735",
 *       "city": "London ",
 *       "country": "GB",
 *       "__v": 0
 *   }
 */
router.get('/', function(req, res) {
	var Query = {};
	if(Object.keys(req.query).length  > 0){
		Query = JSON.parse(req.query.query);
	}
	destinations_api.getDestinations(Query,function(err,result){
		if(err){return res.status(400).send(err);}
		return res.status(200).send(result);
	})
});

/**
 * @api {get} destinations/ Show number of destination
 * @apiGroup destinations
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 * 
 */
router.get('/count', function(req, res) {
	var Query = {};
	if(Object.keys(req.query).length  > 0){
		Query = JSON.parse(req.query.query);
	}

	destinations_api.getDestinationsCount(Query,function(err,result){
		if(err){return res.status(400).send(err);}
		return res.status(200).send({count : result});
	})
});


/**
 * @api {get} destinations/:id Show destination by id
 * @apiGroup destinations
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    
 * {
 *       "_id": "5719f52a74bb8c5b78d48729",
 *       "city": "台北市",
 *       "country": "TW",
 *       "__v": 0
 *   }
 */
router.get('/:id', function(req, res) {
	destinations_api.getDestination(req.params.id,function(err,result){
		if(err){return res.status(400).send(err);}
		return res.status(200).send(result);
	})
});
/**
 * Update  Destination
 */
router.put('/:id', function(req, res) {
	console.log('request',req.body);
/*	getCoordinates(req.body,function(error,data){
		if(error){
			console.log("error in getting location",error);
		}
		if(data){
			req.body.location = data;
		}*/

		destinations_api.UpdateDestination(req.params.id,req.body,function(err,result){
			if(err){return res.status(400).send(err);}
			return res.status(200).send(result);
			});

	// });
});

/**
 * @api {get} destinations/city/:city Show destination by city
 * @apiGroup destinations
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    
 * {
 *       "_id": "5719f52a74bb8c5b78d48729",
 *       "city": "台北市",
 *       "country": "TW",
 *       "__v": 0
 *   }
 */
router.get('/city/:city', function(req, res) {
	destinations_api.getDestinationsByCity(req.params.city,function(err,result){
		if(err){return res.status(400).send(err);}
		return res.status(200).send(result);
	})
});

/**
 * @api {get} destinations/country/:country Show destination by country
 * @apiGroup destinations
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    
 * {
 *       "_id": "5719f52a74bb8c5b78d48729",
 *       "city": "台北市",
 *       "country": "TW",
 *       "__v": 0
 *   }
 */
router.get('/country/:country', function(req, res) {
	destinations_api.getDestinationsByCountry(req.params.country,function(err,result){
		if(err){return res.status(400).send(err);}
		return res.status(200).send(result);
	})
});

/**
 * Restaurant Count of destination
 */
router.get('/restaurant/count/:destination_id', function(req, res) {
	categories_api.getCategoryByName("restaurants",function(err,results){
		if(err){return res.status(400).send({message : "Restaurants Category Not Found"})}
		  attractions_api.getAllAttractionsCount({filter:{category:results._id , city : req.params.destination_id}} , function(err,result){
		    if(err){return res.status(400).send(err);}
		    return res.send({"count": result});
		  })		
	})		
})


function getCoordinates(data,cb) {
		city = data.city+" "+data.country;
		var url = "https://maps.googleapis.com/maps/api/geocode/json?address="+data.city+"+"+data.country+"&key=AIzaSyCVuaPrhXKsPoE5vNNAjitzAjLSvLvXZAY";
		
			console.log('calling google places for get nearest places---',url);
			request(url, function(error, response, body){
			if(error){return cb(error,null);}
			
			if (!error && response.statusCode == 200) {
				console.log("first time body",body);
				body = JSON.parse(body);
				console.log('body',body);
				var dest = body.results[0].geometry.location;
            	console.log("the dest is",dest);
            	return cb(null,dest);
			}
			
			if(!body.results){
				 return cb("no record", null);
			}

		});
}



module.exports =router;