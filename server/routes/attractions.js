/*
* All Attractions Routes after /api/attractions/
*/
var express = require('express'),
	router = express.Router(),
  fs = require('fs'),
  attractions_api = require('../libs/attractions/attractions_api'),
  attractions_utils = require('../libs/attractions/attractions_utils'),
  mail = require('../config/mail'),
  users_db = require('../libs/users/users_db'),
  itinerarys_api = require('../libs/itinerarys/itinerarys_api'),
  users_api = require('../libs/users/users_api'); 

// var extend = require('util')._extend;

var importing = null;

/**
 * Import all for same category  
 */
router.post('/import/all',function(req,res){
  if(importing){
    return res.status(400).send("Other Import is in progress for "+ importing);
  }
  
  importing =   req.body.term.city +" ( " + req.body.category_filter.title + " ) ";
  
  console.log( "Import All Url Called");
  req.body.offset ? req.body.offset = req.body.offset : req.body.offset = 0; 
  attractions_utils.importAllForCategory(req.body,function(err,result){
    console.log("Import Done");
    if(err){ importing = null; return res.status(400).send(err);}
    importing = null;
    return res.status(200).send(result);
  })
});

/**
 * Import with offset  
 */
router.post('/import',function(req,res){
  if(importing){
    return res.status(400).send("Other Import is in progress for "+ importing);
  }
  importing =   req.body.term.city +" ( " + req.body.category_filter.title + " ) ";

  console.log( "Import Url Called");
  attractions_utils.importAttraction(req.body,function(err,result,pending){
    console.log("Import Done");
    if(err){importing = null; return res.status(400).send(err);}
    importing = null;
    return res.status(200).send(result);
  })
});


/**
 * Import with offset  
 */
router.get('/import/progress',function(req,res){
  fs.readFile('import.json', function (err,data) {
    if (err) {
      console.log('File Reading Problem',err);
      return res.status(400).send('File Reading Failed');
    } 
    // file contains json so parse that first
    var importJson = JSON.parse(data.toString());
    return res.status(200).send(importJson);
  });
});



router.get('/reviews',function(req,res){
  var Query = {};
  if(Object.keys(req.query).length  > 0){
    Query = JSON.parse(req.query.query);
  }
  console.log('what',req.query.query);
  // Query.attraction_id = req.query.query;
  attractions_api.getAllReviews(Query , function(err,result){
    if(err){return res.status(400).send(err);}
    return res.status(200).send(result);
  })
});

router.get('/averageRating/:id',function(req,res){
  console.log("att id............",req.params.id);
  attractions_api.getAverageRating(req.params.id , function(err,result){
    if(err){return res.status(400).send(err);}
    return res.status(200).send(result);
  })
});


/**
 * @api {get} attractions/ List all attractions
 * @apiGroup attractions
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    
 *  {
 *       "_id": "5719f56ea6ac03950a003385",
 *       "id": "regency-café-london-2",
 *       "language": "ENGLISH",
 *       "language_code": "en",
 *       "yelp_full_url": "http://www.yelp.co.uk/biz/regency-caf%C3%A9-london-2?utm_campaign=yelp_api&utm_medium=api_v2_search&utm_source=HBWFcAmpcSdq8L-B0j3PZA",
 *       "address": "17-19 Regency Street,Westminster,London SW1P 4BY",
 *       "is_closed": false,
 *       "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/Djp1AgwSG-gd8bq9V1ftiA/ms.jpg",
 *       "rating": 4.5,
 *       "phone_no": "+44 20 7821 6596",
 *       "category": {
 *           "_id": "5719f4d974bb8c5b78d4871d",
 *           "name": "restaurants",
 *           "title": "Restaurants",
 *           "__v": 0
 *       },
 *       "city": {
 *           "_id": "5719f52a74bb8c5b78d48735",
 *           "city": "London ",
 *           "country": "GB",
 *           "__v": 0
 *       },
 *       "country": "GB",
 *       "longitude": -0.13222007974201,
 *       "latitude": 51.4940143171,
 *       "description": "The hubby first spotted the Regency Cafe when he watched the movie Layer Cake. He looked it up while we were recently in London and as the reviews were...",
 *       "name": "Regency Café",
 *       "__v": 0,
 *       "openclosetime": [],
 *       "created_on": "2016-04-27T05:31:58.633Z",
 *       "sub_categories": [
 *           [
 *               "Breakfast & Brunch",
 *               "breakfast_brunch"
 *           ],
 *           [
 *               "British Restaurants",
 *               "british"
 *           ],
 *           [
 *               "Coffee & Tea Shops",
 *               "coffee"
 *           ]
 *       ]
 *   },
 *   {
 *       "_id": "5719f56ea6ac03950a003386",
 *       "id": "the-fat-bear-london",
 *       "language": "ENGLISH",
 *       "language_code": "en",
 *       "yelp_full_url": "http://www.yelp.co.uk/biz/the-fat-bear-london?utm_campaign=yelp_api&utm_medium=api_v2_search&utm_source=HBWFcAmpcSdq8L-B0j3PZA",
 *       "address": "61 Carter Lane,Blackfriars,London EC4V 5DY",
 *       "is_closed": false,
 *       "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/DVXghc62cG1UTK5wHKUwKw/ms.jpg",
 *       "rating": 4.5,
 *       "phone_no": "+44 20 7236 2498",
 *       "category": {
 *           "_id": "5719f4d974bb8c5b78d4871d",
 *           "name": "restaurants",
 *           "title": "Restaurants",
 *           "__v": 0
 *       },
 *       "city": {
 *           "_id": "5719f52a74bb8c5b78d48735",
 *           "city": "London ",
 *           "country": "GB",
 *           "__v": 0
 *       },
 *       "country": "GB",
 *       "longitude": -0.10138103121642,
 *       "latitude": 51.5131833881734,
 *       "description": "Oh, what a night! Jurgen and Beth had organised a smallish yelper get-together and it was a treat! \n\nIt's a little hard to find as it's upstairs above a...",
 *       "name": "The Fat Bear",
 *       "__v": 0,
 *       "openclosetime": [],
 *       "created_on": "2016-04-27T06:45:44.228Z",
 *       "sub_categories": [
 *           [
 *               "American Restaurants",
 *               "newamerican"
 *           ],
 *           [
 *               "Soul Food",
 *               "soulfood"
 *           ],
 *           [
 *               "BBQ & Barbecue",
 *               "bbq"
 *           ]
 *       ]
 *   }
 */
router.get('/',function(req,res){
  var Query = {};
  if(Object.keys(req.query).length  > 0){
    Query = JSON.parse(req.query.query);
  }

  attractions_api.getAllAttractions(Query , function(err,result){
    if(err){return res.status(400).send(err);}
    return res.status(200).send(result);
  })
});

/**
 * @api {get} attractions/count Show number of attractions
 * @apiGroup attractions
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    
 *  {
 *       "count": "626"
 *   }
 */
router.get('/count',function(req,res){

  var Query = {};
  if(Object.keys(req.query).length  > 0){
    Query = JSON.parse(req.query.query);
  }

  attractions_api.getAllAttractionsCount(Query , function(err,result){
    if(err){return res.status(400).send(err);}
    return res.send({"count": result});
  })
});

/**
 * @api {get} attractions/:id Show attractions details
 * @apiGroup attractions
 * @apiParam {String} id 'Mandatory' id of the Attractions.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    
 *  {
 *       "_id": "5719f56ea6ac03950a003385",
 *       "id": "regency-café-london-2",
 *       "language": "ENGLISH",
 *       "language_code": "en",
 *       "yelp_full_url": "http://www.yelp.co.uk/biz/regency-caf%C3%A9-london-2?utm_campaign=yelp_api&utm_medium=api_v2_search&utm_source=HBWFcAmpcSdq8L-B0j3PZA",
 *       "address": "17-19 Regency Street,Westminster,London SW1P 4BY",
 *       "is_closed": false,
 *       "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/Djp1AgwSG-gd8bq9V1ftiA/ms.jpg",
 *       "rating": 4.5,
 *       "phone_no": "+44 20 7821 6596",
 *       "category": {
 *           "_id": "5719f4d974bb8c5b78d4871d",
 *           "name": "restaurants",
 *           "title": "Restaurants",
 *           "__v": 0
 *       },
 *       "city": {
 *           "_id": "5719f52a74bb8c5b78d48735",
 *           "city": "London ",
 *           "country": "GB",
 *           "__v": 0
 *       },
 *       "country": "GB",
 *       "longitude": -0.13222007974201,
 *       "latitude": 51.4940143171,
 *       "description": "The hubby first spotted the Regency Cafe when he watched the movie Layer Cake. He looked it up while we were recently in London and as the reviews were...",
 *       "name": "Regency Café",
 *       "__v": 0,
 *       "openclosetime": [],
 *       "created_on": "2016-04-27T05:31:58.633Z",
 *       "sub_categories": [
 *           [
 *               "Breakfast & Brunch",
 *               "breakfast_brunch"
 *           ],
 *           [
 *               "British Restaurants",
 *               "british"
 *           ],
 *           [
 *               "Coffee & Tea Shops",
 *               "coffee"
 *           ]
 *       ]
 *   }
 */
router.get('/:id',function(req,res){
  attractions_api.getAttraction(req.params.id , function(err,result){
    if(err){return res.status(400).send(err);}
    return res.status(200).send(result);
  })
});

/**
 * Update  Attraction
 */
router.put('/:id',function(req,res){
  //console.log('req.body',req.body);
  if(req.body.openclosetime){
  for(var i=0;i<req.body.openclosetime.length;i++){
    req.body.openclosetime[i].open={};
    req.body.openclosetime[i].close ={};
        req.body.openclosetime[i].open.day = req.body.openclosetime[i].day;
        req.body.openclosetime[i].open.time = req.body.openclosetime[i].opentiming;
        req.body.openclosetime[i].close.day = req.body.openclosetime[i].day;
        req.body.openclosetime[i].close.time = req.body.openclosetime[i].closetiming;
        delete req.body.openclosetime[i].opentiming;
        delete req.body.openclosetime[i].closetiming;
        delete req.body.openclosetime[i].day;
  } 
  }
  attractions_api.updateAttraction(req.params.id,req.body , function(err,result){
    if(err){return res.status(400).send(err);}
    return res.status(200).send(result);
  })
});

/**
 * Delete  Attraction
 */
router.delete('/:id',function(req,res){

    console.log('attraction id is',req.params.id);
    var query = {};
    query.filter = {};
    var obj = {$in:[req.params.id]};
    query.filter['attractions'] = obj;
    console.log('query here is as follows',query);
    itinerarys_api.getall(query, function(err, result) {
    if(err) {return res.status(400).send(err);}
    console.log('result of iti ner is',result);
    
    if(result.length > 0){
      console.log('itinerary found');
      return res.status(200).send(result);
    }
    else if(result.length == 0){
      console.log('no itinerary found');
      attractions_api.deleteAttraction(req.params.id , function(err,results){
        if(err){return res.status(400).send(err);}
        return res.status(200).send(results);
    })
    }
    })  
});

/**
 * Create  Attraction
 */
router.post('/create',function(req,res){
  //console.log('req.body',req.body);
  //console.log('req user',req.user);
  for(var i=0;i<req.body.openclosetime.length;i++){
    req.body.openclosetime[i].open={};
    req.body.openclosetime[i].close ={};
        req.body.openclosetime[i].open.day = req.body.openclosetime[i].day;
        req.body.openclosetime[i].open.time = req.body.openclosetime[i].opentime;
        req.body.openclosetime[i].close.day = req.body.openclosetime[i].day;
        req.body.openclosetime[i].close.time = req.body.openclosetime[i].closetime;
        delete req.body.openclosetime[i].opentime;
        delete req.body.openclosetime[i].closetime;
        delete req.body.openclosetime[i].day;
  }
  //console.log('openclosetime',req.body.openclosetime);
  req.body.created_by = req.user._id;
  req.body.category._id ? req.body.category = req.body.category._id : null;
  req.body.city._id ? req.body.city = req.body.city._id : null;
  attractions_api.createAttraction(req.body , function(err,result){
    if(err){return res.status(400).send(err);}
    //if (!req.user.isadmin || req.user.superadmin) {
      mail.approveAttraction(result._id, req.user);
    //}    
    return res.status(200).send(result);
  })
});
/**
 * Duplicate An  Attraction
 */
router.post('/duplicate/:id',function(req,res){
  //get attraction needed to duplicate
  attractions_api.getAttraction(req.params.id , function(err,result){
    if(err){return res.status(400).send(err);}

    var attraction =  JSON.parse(JSON.stringify(result));
    delete attraction.id;
    delete attraction._id;
    
    attraction.category = attraction.category._id;
    attraction.city = attraction.city._id;
    //now create
    attractions_api.createAttraction(attraction , function(err,result){
      if(err){return res.status(400).send(err);}
      return res.status(200).send(result);
    })
  
  })
});


/**
 * Reviews on Attraction
 */
router.put('/reviews/:id',function(req,res){
  var data = {};
  var obj={};
  data.reviews_data = [];
  data.attraction_id = req.params.id;
  obj.user_id = req.body.user_id;
  obj.username = req.body.username;
  obj.rating = req.body.rating;
  obj.description = req.body.description;
  obj.created_on = new Date();
  obj.publish = false;
  data.reviews_data.push(obj);
  // data.rating = req.body.rating;
  // data.user_id = req.body.user_id;
  attractions_api.updateReviews(req.params.id,data, function(err,result){
    if(err){return res.status(400).send(err);}
    return res.status(200).send(result);
  })
});

/**
 * Bookmark Attraction
 */
router.put('/bookmark/:aid', function(req, res) {
  if(req.user){
    users_api.bookmarkAttraction(req.user, req.params.aid, function(err, data) {
        if (err) {return res.status(400).send(err); }
        return res.status(200).send(data);
    })
  }else{
       return res.status(200).send({message : "Please login!"});
  }
})



module.exports =router;