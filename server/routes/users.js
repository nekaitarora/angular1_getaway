/*
* All Users Routes after /api/users/
*/
var express = require('express'),
	router = express.Router(),
	users_db = require('../libs/users/users_db'),
	users_api = require('../libs/users/users_api'),
	mail = require('../config/mail')
	passport = require('passport'),
	fs = require('fs'),
	async = require('async'),
	dbconfig = require('../config/global_config'),
	_ = require("underscore");

// for using methods inside db
var hash = new users_db();

/**
 * @api {post} users/register Register User
 * @apiName PostUser
 * @apiGroup User
 * @apiSampleRequest http://139.162.5.142:8080/api/users/register
 * @apiParam {String} first_name Mandatory Firstname of the User.
 * @apiParam {String} email     Mandatory Email.
 * @apiParam {String} password     Mandatory Password.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    
 *		{
 *			"_id":"571dfc28897972277e4917b6",
 *			"profile_fields":{"first_name":"ritesh"},
 *			"verification_code":"53842",
 *          "local":{
 *						"email":"ritesh.arora@daffodilsw.com",
 *						"password":"$2a$08$yZdEg6glpQk5OOTOlDoV5ufNfEdP1Z5KkRkAWWdaAvxJfnmh.WrEO"
 *					},
 *			"created_on":"2016-04-25T11:14:48.247Z",
 *			"verified":true,
 *			"status":true,
 *			"issuperadmin":false,
 *			"isadmin":false
 *		}
 * @apiError Email already exist 
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Not Found
 *     {
 *       "message": "Email already exist"
 *     }
 */
router.post('/register', function(req, res) {
   	console.log(req.body);
    //only some fields allowed like username,email,password,profile_fields
  
    if(!req.body.email || !req.body.first_name || !req.body.password){
    	return res.status(200).send("Please send email,first_name, password");
    }

    var user = {};
    user.local = {};
    user.profile_fields = {};
    
    if(users_api.IsEmail(req.body.email)){
    	user.local.email = req.body.email;
    }else{
		return res.status(400).send({ message: "Invalid Email" });
    }

    req.body.password ? user.local.password = hash.generateHash(req.body.password) : null;
    req.body.first_name ? user.profile_fields.first_name = req.body.first_name : null;

    //random verification code
    user.verification_code = Math.floor(Math.random() * 90000) + 10000;

	//check that no user exist with same email or username
    users_api.checkIfExist(user, function(err, result) {
    	if(err){return res.status(400).send(err);}
        users_api.createUser(user, function(err, result) {
            if (err) {return res.status(400).send(err);}
            mail.welcomemail(user.verification_code,req.body.email)
            return res.status(200).send(result);
        });
    })
});


/**
* @api {post} users/login Login User
* @apiName LoginUser
* @apiGroup User
*
* @apiParam {String} email     Mandatory email.
* @apiParam {String} password     Mandatory Password.
*
*
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*    
*        {
*            "_id":"571dfc28897972277e4917b6",
*            "profile_fields":{"first_name":"ritesh"},
*            "verification_code":"53842",
*          "local":{
*                        "email":"ritesh.arora@daffodilsw.com",
*                        "password":"$2a$08$yZdEg6glpQk5OOTOlDoV5ufNfEdP1Z5KkRkAWWdaAvxJfnmh.WrEO"
*                    },
*            "created_on":"2016-04-25T11:14:48.247Z",
*            "verified":true,
*            "status":true,
*            "issuperadmin":false,
*            "isadmin":false
*        }
* @apiError No User Found, Wrong Password, Email Verification Pending
*
* @apiErrorExample {json} Error-Response:
*     HTTP/1.1 400 Not Found
*     {
*       "message": "No User Found"
*     }
*
*/
router.post('/login', function(req, res, next) {

    if(!req.body.email && !req.body.password){
        return res.status(400).send({message : "Email and Password Required"});
    }

    passport.authenticate('local-login', function(err, user, info) {
        if(user && user._id){
                req.logIn(user, function(err) {
	                if (err) { return next(err); }
	                return;
	            });
        }
        if(err)   return res.status(400).send({message : err}); 
        if(info)  return res.status(400).send({message : info});
        if(user)  return res.status(200).send(user);
     })(req, res, next);
})
/**
* @api {post} users/islogin Login User
* @apiName LoginUser
* @apiGroup User
*
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*   {
*    "_id": "571e16220f231caf3bc106f2",
*    "profile_fields": {
*        "ph_no": "78777777",
*        "first_name": "Ritesh",
*        "last_name": "Arora"
*    },
*    "verification_code": "22900",
*    "__v": 0,
*    "local": {
*        "email": "ritesh.arora@daffodilsw.com",
*        "password": "$2a$08$6Qka.Q/gXkrW/G0RvvRLUejuk3O4ZpPPf2G1qObf4TLRhsPJabO8S"
*    },
*    "created_on": "2016-04-25T13:05:38.750Z",
*    "verified": true,
*    "status": true,
*    "issuperadmin": true,
*    "isadmin": true
*    }
*        
* @apiError No User Found, Wrong Password, Email Verification Pending
*
* @apiErrorExample {json} Error-Response:
*     HTTP/1.1 400 Not Found
*     {
*       "message": "User Not Login"
*     }
*
*/
router.get('/islogin',function(req,res){
	if(req.user){return res.status(200).send(req.user);}
	else{return res.status(401).send({message : "User Not Login"});}
});

/**
 * Change password
 */
router.put('/change/password',function(req,res){
	if(req.user.validPassword(req.body.old_password)){
		users_api.getUser(req.user._id,true,function(err,data){
			if(err){
				return  res.status(400).send(err);
			}else{
				req.user.local.password = hash.generateHash(req.body.new_password);
				users_api.updateUser(req.user, function(err,data){
					if(err){
						return res.status(400).send(err);
					}else{
						res.status(200).send("User Updated");
					}
				})
			}
		})
	}else{
		return res.status(400).send("Password not valid");
	}
})


/**
 * logout user
 */
router.get('/logout', function(req, res) {
	if(req.user){
	    req.logout();
	    return res.status(200).send({message :'User logout successfully'})	
	}else{
	    return res.status(400).send({message :'User not login'});	
	}
});

/**
 * get all users
 */
router.get('/all',function(req,res){
	var Query = {};
	if(Object.keys(req.query).length  > 0){
		Query = JSON.parse(req.query.query);
	}
	users_api.getAllUsers(Query , function(err,data){
		if(err){return  res.status(400).send(err)}
		return res.status(200).send(data);
	})
})

/**
 * get all users Count
 */
router.get('/all/count',function(req,res){
	var Query = {};
	if(Object.keys(req.query).length  > 0){
		Query = JSON.parse(req.query.query);
/*		console.log('user query is',req.query.query);*/
	}
	users_api.usersCount(Query,function(err,data){
		if(err){return  res.status(400).send(err);}
		return res.status(200).json({count : data});
	})
})

/**
 * @api {get} users/:id Request User information
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    
 *		{
 *			"_id":"571dfc28897972277e4917b6",
 *			"profile_fields":{"first_name":"ritesh"},
 *			"verification_code":"53842",
 *          "local":{
 *						"email":"ritesh.arora@daffodilsw.com",
 *						"password":"$2a$08$yZdEg6glpQk5OOTOlDoV5ufNfEdP1Z5KkRkAWWdaAvxJfnmh.WrEO"
 *					},
 *			"created_on":"2016-04-25T11:14:48.247Z",
 *			"verified":true,
 *			"status":true,
 *			"issuperadmin":false,
 *			"isadmin":false
 *		}
 *
 * @apiError User Not Found The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "message": "User Not Found"
 *     }
 */

router.get('/:id',function(req,res){
	users_api.getUser(req.params.id,false,function(err,data){
		if(err){return  res.status(400).send({message : err});}
		return res.status(200).send(data);
	})
})

/**
 * get user by email
 */
router.get('/email/:email',function(req,res){
	users_api.getUserByEmail(req.params.email,function(err,data){
		if(err){return  res.status(400).send(err)}
		return res.status(200).send(data);
	})
})


/**
 * verify email
 */
router.post('/verify/:email/:token',function(req,res){
	users_api.verifyUser(req.params.token,req.params.email,function(err,data){
		if(err){return  res.status(400).send(err)}
		return res.status(200).send(data);
	})
})

/**
 * delete user by id
 */
router.delete('/:id', function(req, res) {
    users_api.deleteUser(req.params.id, function(err, data) {
        if (err) {return res.status(400).send(err);}
        return res.status(200).send("User Deleted");
    })
})

/**
 *Create User(admin only)
 */
router.post('/create', function(req, res) {
    if (req.user.isadmin || req.user.superadmin) {

        //check for email and password fields
        if (!users_api.IsEmail(req.body.local.email) && !req.body.local.password && !req.body.profile_fields.first_name) {
            return res.status(400).send({ message: "Email / Password / first_name missing" });
        }
        users_api.IsEmail(req.body.local.email) ? req.body.local.email : req.body.local.email = null;
        req.body.local.password ? req.body.local.password = hash.generateHash(req.body.local.password) : null;
        req.body.issuperadmin = false;
        req.body.verification_code = Math.floor(Math.random() * 90000) + 10000;

        //check that no user exist with same email or username
        users_api.checkIfExist(req.body, function(err, result) {
            if (err) {
                return res.status(400).send(err); }

            // an example using an object instead of an array
            async.parallel({
                    one: function(callback) {
                        if (req.body.timeline_image && req.body.timeline_image.indexOf("base64") > -1) {
                            var base64Data = req.body.timeline_image.replace(/^data:image\/png;base64,/, "");
                            fs.writeFile("public/uploads/users/timeline_" + req.body.local.email + ".png", base64Data, 'base64', function(err) {
                                callback(null, "uploads/users/timeline_" + req.body.local.email + ".png");
                            });
                        } else {
                            callback(null, req.body.timeline_image);
                        }

                    },
                    two: function(callback) {
                        if (req.body.image && req.body.image.indexOf("base64") > -1) {
                            var base64Data = req.body.image.replace(/^data:image\/png;base64,/, "");
                            fs.writeFile("public/uploads/users/image_" + req.body.local.email + ".png", base64Data, 'base64', function(err) {
                                callback(null, "uploads/users/image_" + req.body.local.email + ".png");
                            });
                        } else {
                            callback(null, req.body.image);
                        }
                    }
                },
                function(err, results) {
                    req.body.timeline_image = results.one;
                    req.body.image = results.two;
                    users_api.createUser(req.body, function(err, data) {
                        if (err) {
                            return res.status(400).send(err) }
                        return res.status(200).send(data);
                    });
                });
        });
    } else {
        res.status(400).send('Permission Denied');
    }

})

/**
 * Udpate User (admin/client)
 */
router.put('/:id', function(req, res) {

    // an example using an object instead of an array
    async.parallel({
            one: function(callback) {
                if (req.body.timeline_image && req.body.timeline_image.indexOf("base64") > -1) {
                    var base64Data = req.body.timeline_image.replace(/^data:image\/png;base64,/, "");
                    fs.writeFile("public/uploads/users/timeline_" + req.body._id + ".png", base64Data, 'base64', function(err) {
                        callback(null, "uploads/users/timeline_" + req.body._id + ".png");
                    });
                } else {
                    callback(null, req.body.timeline_image);
                }

            },
            two: function(callback) {
                if (req.body.image && req.body.image.indexOf("base64") > -1) {
                    var base64Data = req.body.image.replace(/^data:image\/png;base64,/, "");
                    fs.writeFile("public/uploads/users/image_" + req.body._id + ".png", base64Data, 'base64', function(err) {
                        callback(null, "uploads/users/image_" + req.body._id + ".png");
                    });
                } else {
                    callback(null, req.body.image);
                }
            }
        },
        function(err, results) {
            // results is now equals to: {one: 1, two: 2}
            req.body.timeline_image = results.one;
            req.body.image = results.two;
            users_api.getUser(req.params.id, true, function(err, data) {
                //some one try to be smart
                if (!req.user.isadmin && !req.user.issuperadmin && !req.user._id == req.params.id) {
                    return res.status(400).send('Permission Denied');
                }
                if (req.body.local.password) {
                    var newpassword = req.body.local.password;
                    newpassword = hash.generateHash(newpassword);
                    req.body.local.password = newpassword;
                    data.local.password = req.body.local.password;
                }

                //admin user updating things
                if (req.user.isadmin || req.user.issuperadmin) {
                    req.body.issuperadmin ? delete req.body.issuperadmin : null;
                    var pass = data.local.password;
                    _.extend(data, req.body);
                    data.local.password = pass;
                }

                //profile update by user
                if (req.user._id == req.params.id) {
                    req.body.local.username ? data.local.username = req.body.local.username : null;
                    req.body.local.email ? data.local.email = req.body.local.email : null;
                    req.body.profile_fields.first_name ? data.profile_fields.first_name = req.body.profile_fields.first_name : null;
                    req.body.profile_fields.last_name ? data.profile_fields.last_name = req.body.profile_fields.last_name : null;
                    req.body.profile_fields.ph_no ? data.profile_fields.ph_no = req.body.profile_fields.ph_no : null;
                    req.body.image ? data.image = req.body.image : null;
                    var pass = data.local.password;
                    _.extend(data, req.body);
                    data.local.password = pass;
                }
                users_api.updateUser(data, function(err, data) {
                    if (err) {
                        return res.status(400).send(err); }
                    	return res.status(200).send(data);
                })
            });
        });
})

/**
 * @api {post} users/forgot/password/:email Send reset password link
 * @apiGroup User
 *
 * @apiParam {String} email User email id.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 * 
 */
router.post('/forgot/password/:email',function(req,res){
	users_api.getUserByEmail(req.params.email , function(err,data){
		if(err){return  res.status(400).send(err);}
		console.log(data);
		if(data._id){
		var path = '<<domain>>#/forgot/password/'+data.verification_code+'/'+data.local.email;
		var html = '<table style="text-align:center;width:100%;background:url('+dbconfig.domain+'images/slide-one.jpg) no-repeat center center / 100% auto; background-size: 100%;"> <tr> <td align="center"><img src="'+dbconfig.domain+'images/logo-getawey.png"></td> </tr> <tr> <td align="left" style="font-weight:bold;font-size:15px;color:white;">Hi '+data.profile_fields.first_name+'</td> </tr><tr><td><h2 style="font-size:30px;color:white">Welcome to travel itinerary</h2></td></tr><tr> <td align="center" style="height:40px;"><a href="'+path+'" style="font-size:20px;padding:10px;background-color: #4a97d2;color:white;text-decoration: none;font-weight: bold;">Forgot Password</a></td> </tr> </table>';

			mail.sendmail(data.local.email, 'Reset password' , html);
			return res.status(200).send("Email Send");

		}else{
			return  res.status(401).send({messgae :"Email does not exist"});
		}
	})
})



/**
 * Set New Password For Forgot
 */
router.put('/:email/:token',function(req,res){
	if(!req.body.password ){
		return res.status(400).send({messgae :"Please send new password"});
	}
	req.body.password = hash.generateHash(req.body.password); 
	users_api.setNewPassword(req.params.email , req.params.token , req.body.password,function(err,data){
		if(err){return  res.status(400).send(err);}

		return res.status(200).send({messgae : "Password Updated"});

	})
})




/**
 * Facebook login (admin/client)
 */

router.get('/login/facebook', function(req, res, next) {
    passport.authenticate('facebook',{scope :'email'},  function(err, user, info) {
        if(err)   return res.status(400).send(err); 
        if(user._id){
           req.logIn(user, function(err) {
                if (err) { return next(err); }
                return res.redirect("/#/profile/edit");
            });
        }
     })(req, res, next);
})






// /**
//  * get all users
//  */
// router.get('/all/forautocomplete',function(req,res){
// 	var Query = {};
// 	if(Object.keys(req.query).length  > 0){
// 		Query = JSON.parse(req.query.query);
// 	}
// 	users_api.getUsersAutocomplete(Query , function(err,data){
// 		if(err){return  res.status(400).send(err)}
// 		res.status(200).send(data);
// 	})
// })














// function validation_err(err){
// 	var errors ={};
// 	for(key in err.errors){
// 		if(err.errors[key].type == 'required'){
// 			errors[err.errors[key].path.replace("local.","")] = err.errors[key].path.replace("local.","") +' Missing';
// 		}else{
// 			 errors[err.errors[key].path.replace("local.","")] = err.errors[key].path.replace("local.","") +' already exist';
// 		}
// 	}
// 	return errors;
// }
module.exports =router;