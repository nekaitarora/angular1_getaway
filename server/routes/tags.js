var express = require('express'),
	router = express.Router(),
	tags_db = require('../libs/tags/tags_db'),
	tags_api = require('../libs/tags/tags_api'),
	journals_db = require('../libs/journals/journals_db'),
	journals_api = require('../libs/journals/journals_api'),
	comments_db = require('../libs/comments/comments_db'),
	comments_api = require('../libs/comments/comments_api'),
	mail = require('../config/mail')
	passport = require('passport'),
	dbconfig = require('../config/global_config'),
	_ = require("underscore");

router.post('/create',function(req, res){
	var tag = {};
	tag.name = req.body.name;
    if(req.user && req.user._id)
    {	
	tags_api.createTags(tag,function(err,result){
		if (err) {return res.status(400).send(err);}
		return res.status(200).send(result);
	})
}
})

router.get('/all',function(req,res){
	var Query = {};
	if(Object.keys(req.query).length  > 0){
		Query = JSON.parse(req.query.query);
	}
	tags_api.getall(Query ,function(err,result){
        if (err) {return res.status(400).send(err);}
        return res.status(200).send(result);
    })
})

router.get('/:id',function(req,res){
	tags_api.getTag(req.params.id,false,function(err,data){
		if(err){return  res.status(400).send({message : err});}
		return res.status(200).send(data);
	})
})





module.exports =router;
