/*
 * All Itinerarys Routes after /api/Itinerarys
 */
var express = require('express'),
    router = express.Router(),
    itinerarys_api = require('../libs/itinerarys/itinerarys_api'),
    journals_api = require('../libs/journals/journals_api'),
    users_api = require('../libs/users/users_api');

var global_config = require('../config/global_config');
var async = require("async");
var request = require("request");
var _ =require('underscore');


router.post("/optimizepath", function(req, res) {

    request(req.body.url, function(error, response, body) {
        if (!error && response.statusCode == 200) {
            var body = JSON.parse(body);
            return res.status(200).send(body);
        }
    })
})
router.post("/create", function(req, res) {

    if (!req.body.trip_from || !req.body.trip_to || !req.body.start_date || !req.body.end_date || !req.body.title || !req.body.machine_name) {
        return res.status(400).send({ message: "Required Feild Missing" });
    }

    if (req.body.trip_from._id) {
        req.body.trip_from = req.body.trip_from._id
    }


    if (req.body.trip_to[0]._id || (req.body.trip_to[0].destination && req.body.trip_to[0].destination._id)) {

        var new_trip_to = [];

        req.body.trip_to.forEach(function(destination) {
            var json = {};
            destination._id ? json.destination = destination._id : null;

            destination.destination && destination.destination._id ? json.destination = destination.destination._id : null;

            destination.start_date ? json.start_date = destination.start_date : null;
            destination.end_date ? json.end_date = destination.end_date : null;

            new_trip_to.push(json);
        })
        req.body.trip_to = new_trip_to;
    }
    if(req.user && req.user._id){
        req.body.created_by = req.user._id;
    }else{
        req.body.anonymous = true;
    }

    req.body.attractions = [];

    if(req.body.days) {
        Object.keys(req.body.days).forEach(function(day) {
            req.body.days[day].forEach(function(attraction) {
                if (req.body.attractions.indexOf(attraction.id) == -1) {
                    req.body.attractions.push(attraction.id);
                }
            })
        })
    }


    if (req.body._id) {
        itinerarys_api.updateItinerary(req.body._id, req.body, function(err, results) {
            if (err) {
                return res.status(400).send(err); }
            return res.status(200).send(results);
        })
    } else {
        itinerarys_api.createItinerary(req.body, function(err, results) {
            if (err) {
                return res.status(400).send(err); }
            itinerarys_api.getItinerary(results._id, results.machine_name, function(err, results) {
                if (err) {
                    return res.status(400).send(err); }
                return res.status(200).send(results);
            })
        })
    }


})

router.get('/all', function(req, res) {
    var Query = {};
    if (Object.keys(req.query).length > 0) {
        Query = JSON.parse(req.query.query);
    }
    itinerarys_api.getall(Query, function(err, result) {
        if (err) {
            return res.status(400).send(err);
        }
        return res.status(200).send(result);
    })
});

router.get('/relatedItinerary', function(req, res) {

    var Query = {};
    if (Object.keys(req.query).length > 0) {
        Query = JSON.parse(req.query.query);
    }
  
   if(Query.filter && Query.filter.attraction){
      var attractionId = Query.filter.attraction ?  Query.filter.attraction : "";
        delete Query.filter.attraction;
        var obj = {$in:[attractionId]};
        Query.filter['attractions'] = obj;
    }
    // console.log('related itinerary query is',Query);
    itinerarys_api.getall(Query, function(err, result) {
        if (err) {return res.status(400).send(err);}
/*        var firstAttraction =[];
        var itineraries_arr = [];*/
/*        _.each(result,function(itinerary){
            var filtered = _(itinerary.attractions).filter(function(item) {
                                return item != attractionId;
                            });
            if(filtered[0]){
                firstAttraction.push(filtered[0]);
            }
            console.log('firstAttraction',firstAttraction);
            itineraries_arr.push({"itinerary_id":itinerary._id,"title":itinerary.title,"machine_name":itinerary.machine_name,"attraction_id":filtered[0]});

        });*/

/*        attractions_api.getItineraryAttractions(firstAttraction,function(err,data){
            if (err) {
                  return res.status(400).send(err);
            }
            console.log("------------d---------------",data);
            for(var i=0;i<itineraries_arr.length;i++){
                var matched_attraction = _.filter(data,function(item){
                    console.log("item-------------------------------------------------",item);
                    console.log("item-------------------------------------------------",itineraries_arr[i].attraction_id);
                    return item._id.toString() == itineraries_arr[i].attraction_id.toString();
                })
                if(matched_attraction.length > 0){
                itineraries_arr[i]["image"] = matched_attraction[0].image;
            }
        }

           
        })*/                
           return res.status(200).send(result);


    })
});

router.get('/all/count', function(req, res) {
    var Query = {};
    if (Object.keys(req.query).length > 0) {
        Query = JSON.parse(req.query.query);
    }
    itinerarys_api.itinerariesCount(Query, function(err, data) {
        if (err) {
            return res.status(400).send(err);
        }
        return res.status(200).json({ count: data });
    })
});

router.get("/:machine_name/:id", function(req, res) {
    itinerarys_api.getItinerary(req.params.id, req.params.machine_name, function(err, results) {
        if (err) {
            return res.status(400).send(err); }
        return res.status(200).send(results);
    })
})


router.post("/clone/:id/:machine_name", function(req, res) {
    if (!req.user && !req.user._id) {
        res.status(400).send("pls login");
    }

    itinerarys_api.getItinerary(req.params.id, req.params.machine_name, function(err, results) {
        if (err) {
            return res.status(400).send(err); }

        if (results.isdeleted) {
            return res.status(400).send({ message: "Itinerary already deleted" });
        }
        var orignal_itinerary = Object.assign({}, results)._doc;
        orignal_itinerary.created_by = req.user._id;
        orignal_itinerary.published = false;
        delete orignal_itinerary._id;

        itinerarys_api.createItinerary(orignal_itinerary, function(err, results) {
            if (err) {
                return res.status(400).send(err); }
            itinerarys_api.getItinerary(results.id, results.machine_name, function(err, results) {
                if (err) {
                    return res.status(400).send(err); }
                return res.status(200).send(results);
            });
        })
    })
})
router.put("/:id", function(req, res) {

    if(req.user){
        req.body.created_by = req.user._id;
    }
    
    req.body.machine_name ? delete req.body.machine_name : null;
    if (req.body.days) {
    req.body.attractions = [];
        Object.keys(req.body.days).forEach(function(day) {
            req.body.days[day].forEach(function(attraction) {
                if (req.body.attractions.indexOf(attraction.id) == -1) {
                    req.body.attractions.push(attraction.id);
                }

            })
        })


    }
    itinerarys_api.updateItinerary(req.params.id, req.body, function(err, results) {
        if (err) {
            return res.status(400).send(err); }
        return res.status(200).send(results);
    })
})

router.delete("/:id/:machine_name/markdeleted", function(req, res) {
    if (!req.user || !req.user._id) {
        res.status(200).send({ message: "Need Login" })
    }
    itinerarys_api.markDeleted(req.params.id, req.params.machine_name, req.user._id, function(err, results) {
        if (err) {
            return res.status(400).send(err); }
        return res.status(200).send(results);
    })
})

router.delete("/:id", function(req, res) {
    itinerarys_api.deleteItinerary(req.params.id, function(err, results) {
        if (err) {
            return res.status(400).send(err); }
        return res.status(200).send(results);
    })
})

router.get("/distance", function(req, res) {
    var Query = {};
    if (Object.keys(req.query).length > 0) {
        Query = JSON.parse(req.query.query);
    }

    if (!Query || !Query.trip_from || !Query.trip_to) {
        return res.status(400).send({ message: "From or To Parameters Missing" });
    }

    itinerarys_api.googleApis(Query, function(err, result) {
        if (err) {
            return res.status(400).send(err); }
        return res.status(200).send(result);
    })

})

router.post("/daydistancetime", function(req, res) {
    if (!req.body.day) {
        return res.status(200).send({ message: "Please pass day's JSON" })
    }

    var dayJSON = req.body.day;
    for (var i = 1; i < dayJSON.length; i++) {
        var url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + dayJSON[i - 1].latitude + "," + dayJSON[i - 1].longitude + "&destination=" + dayJSON[i].latitude + "," + dayJSON[i].longitude;
        dayJSON[i - 1].next_distance_url = url;
    }

    async.map(dayJSON, function(attraction, callback) {
        if (attraction.next_distance_url) {
            var url = attraction.next_distance_url;
            attraction.next_travel_mode ? url += "&mode=" + attraction.next_travel_mode : null;
            // iterator function
            request(url + "&key=" + global_config.googleApisKey, function(error, response, body) {
                if (!error && response.statusCode == 200) {
                    var body = JSON.parse(body);
                    // do any further processing of the data here
                    attraction.next_distance = body.routes[0].legs[0].distance;
                    attraction.next_duration = body.routes[0].legs[0].duration;
                    attraction.next_travel_mode = body.routes[0].legs[0].steps[0].travel_mode;
                    callback(null, attraction);
                } else {
                    callback(error || response.statusCode);
                }
            });
        } else {

            callback(null, attraction);
        }
    }, function(err, results) {
        for (var i = 0; i < results.length; i++) {
            //set start time variable if not create own 8am start time
            if (!results[i] || !results[i].start_time) {
                results[i].start_time = "08:00";
            } else {
                results[i].start_time = results[i].start_time;
            }

            !results[i].stay_time ? results[i].stay_time = "02:00" : results[i].stay_time = results[i].stay_time;

            console.log("results i----", results[i]);
            //convert start and stay time in milisecond for time checking

            var start_time = results[i].start_time.split(":");
            var stay_time = results[i].stay_time.split(":");
            var newtime = [];
            newtime[0] = Number(start_time[0]) + Number(stay_time[0]);
            newtime[1] = Number(start_time[1]) + Number(stay_time[1]);
            console.log("new time ", newtime);

            if (results[i].next_duration) {
                newtime[1] = Number(newtime[1]) + Math.floor(Number(results[i].next_duration.value) / 60);
            }

            console.log("new time2 ", newtime);
            if (newtime[1] > 60) {
                newtime[0] = Number(newtime[0]) + Math.floor(Number(newtime[1]) / 60);
                newtime[1] = newtime[1] % 60;
            }
            console.log("new time3 ", newtime);

            if (newtime[0] > 23) {
                return res.status(400).send({ message: "Oops! There is not enough time for this place on this day" });
            }
            if (results[i + 1]) {
                results[i + 1].start_time = newtime.join(":");
                results[i + 1].stay_time = results[i + 1].stay_time ? results[i + 1].stay_time : '02:00';
            }
            if (i == results.length - 1) {
                delete results[i].next_distance;
                delete results[i].next_distance_url;
                delete results[i].next_duration;
                delete results[i].next_travel_mode;
            }

        }

        return res.status(200).send(results);
    });

})


router.post("/getallmodes", function(req, res) {
    var modes = ["driving", "walking", "bicycling", "transit"];
    if (!req.body.attraction) {
        return res.status(400).send("attraction required for getting all modes");
    }
    if (!req.body.attraction.next_distance_url) {
        return res.status(400).send("Next Url Missing  in attraction ");
    }
    var attraction = req.body.attraction;
    var allmodes = {};
    modes.forEach(function(mode) {
        allmodes[mode] = {};
    })
    async.map(modes, function(mode, callback) {

        // iterator function
        request(attraction.next_distance_url + "&mode=" + mode + "&key=" + global_config.googleApisKey, function(error, response, body) {
            console.log(attraction.next_distance_url + "&mode=" + mode + "&key=" + global_config.googleApisKey);
            if (!error && response.statusCode == 200) {

                var body = JSON.parse(body);
                // do any further processing of the data here

                allmodes[mode].start_address = body.routes[0].legs[0].start_address;
                allmodes[mode].end_address = body.routes[0].legs[0].end_address;
                allmodes[mode].next_distance = body.routes[0].legs[0].distance;
                allmodes[mode].next_duration = body.routes[0].legs[0].duration;
                allmodes[mode].next_travel_mode = body.routes[0].legs[0].steps[0].travel_mode;
                allmodes[mode].steps = body.routes[0].legs[0].steps;
                callback(null, allmodes[mode]);
            } else {
                callback(error || response.statusCode);
            }
        });
    }, function(err, results) {
        return res.status(200).send(allmodes);
    });

});


router.put('/bookmark/:itinerary_id', function(req, res) {
  if(req.user){
    users_api.bookmarkItinerary(req.user, req.params.itinerary_id, function(err, data) {
        if (err) {return res.status(400).send(err); }
        return res.status(200).send(data);
    })
  }else{
       return res.status(200).send({message : "Please login!"});
  }
})



router.post('/route-optimizer', function(req, res) {
    var tsp = require('tspsolver');
    if (!req.body.day) {
        return res.status(400).send("please pass attraction array in day");
    }
    req.body.day.forEach(function(day) {
        tsp.addAddress(day.address);

    })

    console.log("s", req.body.day)
    tsp.solveAtoZ(function() {
        var embed_uri = tsp.createGoogleLink(true);
        var map_uri = tsp.createGoogleLink();

        var send = {
            'addresses': tsp.addAddress,
            'embed_uri': embed_uri,
            'map_uri': map_uri
        };

        console.log(send);
        res.send(send);
    })
});



router.delete('/delete/:id', function(req, res) {

    console.log('attraction id is',req.params.id);
    var query = {};
    query.filter = {};
    var obj = {$in:[req.params.id]};
    query.filter['itineraries'] = obj;
    console.log('query here is as follows',query);
    journals_api.getall(query,function(err,result){
        if(err) {return res.status(400).send(err);}
        if(result.length > 0){
            console.log('found journal with itinerary');
            return res.status(200).send(result); 
        }
        else if(result.length == 0){
            console.log('no journal with this itinerary');
            itinerarys_api.deletePermanently(req.params.id, function(err, data) {
                if (err) {return res.status(400).send(err);}
                return res.status(200).send(data);
            })
        }      
    })

});

module.exports = router;
