/*
* All Users Routes after /api/categories/
*/
var express = require('express'),
	router = express.Router(),
	categories_api = require('../libs/categories/categories_api');



/**
 * Demo no reponse 
 */
 
/*router.get('/insertforDemo', function(req, res) {

var categories = 
[
	{name: "restaurants" , title : "Restaurants"},
	{name: "publicservicesgovt" , title : "Public Services & Government"},
	{name: "localservices" , title : "Local Services"},
	{name: "localflavor" , title : "Local Flavor"},
	{name: "hotelstravel" , title : "Hotels & Travel"},
	{name: "eventservices" , title : "Event Planning & Services"},
	{name: "beautysvc" , title : "Beauty & Spas"},
	{name: "auto" , title : "Automotive"},
	{name: "arts" , title : "Arts & Entertainment"},
	{name: "active" , title : "Active Life"},
	{name: "financialservices" , title : "Financial Services"},
	{name: "health" , title : "Health & Medical"},
];
	categories.forEach(function(category){
		categories_api.createCategories(category,function(err,result){
		})
	})

});*/


router.post('/create', function(req, res) {
	categories_api.createCategories({name : req.body.name, title : req.body.title} , function(err,result){
		if(err){return res.status(400).send(err);}
		return res.status(200).send(result);
	})
});

router.delete('/:id', function(req, res) {
	categories_api.deleteCategories(req.params.id , function(err,result){
		if(err){return res.status(400).send(err);}
		return res.status(200).send(result);
	})
});

/**
 * @api {get} categories/ List all Categories
 * @apiGroup categories
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    
 *		{
 *         "_id": "5719f4d974bb8c5b78d4871d",
 *         "name": "restaurants",
 *         "title": "Restaurants",
 *         "__v": 0
 *  	},
 *		{
 *         "_id": "5719f4d974bb8c5b78d4871e	",
 *         "name": "publicservicesgovt",
 *         "title": "Public Services & Government",
 *         "__v": 0
 *  	}
 */
router.get('/', function(req, res) {
	categories_api.getCategories(function(err,result){
		if(err){return res.status(400).send(err);}
		return res.status(200).send(result);
	})
});


/**
 * @api {get} categories/:id List the Category by given id
 * @apiGroup categories
 * @apiParam {String} id Mandatory id of the Categories.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    
 *		{
 *         "_id": "5719f4d974bb8c5b78d4871d",
 *         "name": "restaurants",
 *         "title": "Restaurants",
 *         "__v": 0
 *  	}
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Not Found
 *     {
 *       "message": "Data not found"
 *     }
 */
router.get('/:id', function(req, res) {
	categories_api.getCategory(req.params.id,function(err,result){
		if(err){return res.status(400).send(err);}
		if(result){
			return res.status(200).send(result);
		}else{
			return res.status(400).send({message : 'Data not found'});
		}
	})
});

/**
 * @api {get} categories/:name List the Category by given name
 * @apiGroup categories
 * @apiParam {String} name Mandatory name of the Categories.
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    
 *		{
 *         "_id": "5719f4d974bb8c5b78d4871d",
 *         "name": "restaurants",
 *         "title": "Restaurants",
 *         "__v": 0
 *  	}
 *
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 400 Not Found
 *     {
 *       "message": "Data not found"
 *     }
 */
router.get('name/:name', function(req, res) {
	categories_api.getCategoryByName(req.params.name,function(err,result){
		if(err){return res.status(400).send(err);}
		if(result){
			return res.status(200).send(result);
		}else{
			return res.status(400).send({message : 'Data not found'});
		}
	})
});


module.exports =router;