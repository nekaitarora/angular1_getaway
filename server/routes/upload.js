var fs = require('fs');
var multer = require('multer');
module.exports =function(app,basepath){

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
  	var newDestination = './public/uploads/'+req.params.path+"/";
	
	try {
	    fs.statSync(newDestination);
	} catch (err) {
	    fs.mkdirSync(newDestination);
	}

    cb(null, newDestination);
  },

  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }

})
 

app.post("/upload/:path", multer({storage : storage}).single('file'), function(req, res) {
	var path = req.file.path.replace("public","");
	res.status(200).send(path);
});

app.post("/multiUpload/:path", multer({storage : storage}).array('file',10), function(req, res) {
	var paths=[];
	// console.log('file',req.files);

	for(var a=0;a<req.files.length;a++)
	{
	var path = req.files[a].path.replace("public","");
	paths.push(path);
	}
	res.status(200).send(paths);
});

}