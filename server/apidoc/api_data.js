define({ "api": [
  {
    "type": "get",
    "url": "users/:id",
    "title": "Request User information",
    "name": "GetUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n   \n\t\t{\n\t\t\t\"_id\":\"571dfc28897972277e4917b6\",\n\t\t\t\"profile_fields\":{\"first_name\":\"ritesh\"},\n\t\t\t\"verification_code\":\"53842\",\n         \"local\":{\n\t\t\t\t\t\t\"email\":\"ritesh.arora@daffodilsw.com\",\n\t\t\t\t\t\t\"password\":\"$2a$08$yZdEg6glpQk5OOTOlDoV5ufNfEdP1Z5KkRkAWWdaAvxJfnmh.WrEO\"\n\t\t\t\t\t},\n\t\t\t\"created_on\":\"2016-04-25T11:14:48.247Z\",\n\t\t\t\"verified\":true,\n\t\t\t\"status\":true,\n\t\t\t\"issuperadmin\":false,\n\t\t\t\"isadmin\":false\n\t\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "User",
            "description": "<p>Not Found The id of the User was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"User Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "users/islogin",
    "title": "Login User",
    "name": "LoginUser",
    "group": "User",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n{\n \"_id\": \"571e16220f231caf3bc106f2\",\n \"profile_fields\": {\n     \"ph_no\": \"78777777\",\n     \"first_name\": \"Ritesh\",\n     \"last_name\": \"Arora\"\n },\n \"verification_code\": \"22900\",\n \"__v\": 0,\n \"local\": {\n     \"email\": \"ritesh.arora@daffodilsw.com\",\n     \"password\": \"$2a$08$6Qka.Q/gXkrW/G0RvvRLUejuk3O4ZpPPf2G1qObf4TLRhsPJabO8S\"\n },\n \"created_on\": \"2016-04-25T13:05:38.750Z\",\n \"verified\": true,\n \"status\": true,\n \"issuperadmin\": true,\n \"isadmin\": true\n }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "No",
            "description": "<p>User Found, Wrong Password, Email Verification Pending</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Not Found\n{\n  \"message\": \"User Not Login\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "users/login",
    "title": "Login User",
    "name": "LoginUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Mandatory email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Mandatory Password.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n\n   {\n       \"_id\":\"571dfc28897972277e4917b6\",\n       \"profile_fields\":{\"first_name\":\"ritesh\"},\n       \"verification_code\":\"53842\",\n     \"local\":{\n                   \"email\":\"ritesh.arora@daffodilsw.com\",\n                   \"password\":\"$2a$08$yZdEg6glpQk5OOTOlDoV5ufNfEdP1Z5KkRkAWWdaAvxJfnmh.WrEO\"\n               },\n       \"created_on\":\"2016-04-25T11:14:48.247Z\",\n       \"verified\":true,\n       \"status\":true,\n       \"issuperadmin\":false,\n       \"isadmin\":false\n   }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "No",
            "description": "<p>User Found, Wrong Password, Email Verification Pending</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Not Found\n{\n  \"message\": \"No User Found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "users/register",
    "title": "Register User",
    "name": "PostUser",
    "group": "User",
    "sampleRequest": [
      {
        "url": "http://139.162.5.142:8080/api/users/register"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>Mandatory Firstname of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Mandatory Email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Mandatory Password.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n   \n\t\t{\n\t\t\t\"_id\":\"571dfc28897972277e4917b6\",\n\t\t\t\"profile_fields\":{\"first_name\":\"ritesh\"},\n\t\t\t\"verification_code\":\"53842\",\n         \"local\":{\n\t\t\t\t\t\t\"email\":\"ritesh.arora@daffodilsw.com\",\n\t\t\t\t\t\t\"password\":\"$2a$08$yZdEg6glpQk5OOTOlDoV5ufNfEdP1Z5KkRkAWWdaAvxJfnmh.WrEO\"\n\t\t\t\t\t},\n\t\t\t\"created_on\":\"2016-04-25T11:14:48.247Z\",\n\t\t\t\"verified\":true,\n\t\t\t\"status\":true,\n\t\t\t\"issuperadmin\":false,\n\t\t\t\"isadmin\":false\n\t\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Email",
            "description": "<p>already exist</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Not Found\n{\n  \"message\": \"Email already exist\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "users/forgot/password/:email",
    "title": "",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User email id.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/users.js",
    "groupTitle": "User",
    "name": "PostUsersForgotPasswordEmail"
  },
  {
    "type": "get",
    "url": "attractions/",
    "title": "List all attractions",
    "group": "attractions",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n  \n{\n     \"_id\": \"5719f56ea6ac03950a003385\",\n     \"id\": \"regency-café-london-2\",\n     \"language\": \"ENGLISH\",\n     \"language_code\": \"en\",\n     \"yelp_full_url\": \"http://www.yelp.co.uk/biz/regency-caf%C3%A9-london-2?utm_campaign=yelp_api&utm_medium=api_v2_search&utm_source=HBWFcAmpcSdq8L-B0j3PZA\",\n     \"address\": \"17-19 Regency Street,Westminster,London SW1P 4BY\",\n     \"is_closed\": false,\n     \"image_url\": \"https://s3-media4.fl.yelpcdn.com/bphoto/Djp1AgwSG-gd8bq9V1ftiA/ms.jpg\",\n     \"rating\": 4.5,\n     \"phone_no\": \"+44 20 7821 6596\",\n     \"category\": {\n         \"_id\": \"5719f4d974bb8c5b78d4871d\",\n         \"name\": \"restaurants\",\n         \"title\": \"Restaurants\",\n         \"__v\": 0\n     },\n     \"city\": {\n         \"_id\": \"5719f52a74bb8c5b78d48735\",\n         \"city\": \"London \",\n         \"country\": \"GB\",\n         \"__v\": 0\n     },\n     \"country\": \"GB\",\n     \"longitude\": -0.13222007974201,\n     \"latitude\": 51.4940143171,\n     \"description\": \"The hubby first spotted the Regency Cafe when he watched the movie Layer Cake. He looked it up while we were recently in London and as the reviews were...\",\n     \"name\": \"Regency Café\",\n     \"__v\": 0,\n     \"openclosetime\": [],\n     \"created_on\": \"2016-04-27T05:31:58.633Z\",\n     \"sub_categories\": [\n         [\n             \"Breakfast & Brunch\",\n             \"breakfast_brunch\"\n         ],\n         [\n             \"British Restaurants\",\n             \"british\"\n         ],\n         [\n             \"Coffee & Tea Shops\",\n             \"coffee\"\n         ]\n     ]\n },\n {\n     \"_id\": \"5719f56ea6ac03950a003386\",\n     \"id\": \"the-fat-bear-london\",\n     \"language\": \"ENGLISH\",\n     \"language_code\": \"en\",\n     \"yelp_full_url\": \"http://www.yelp.co.uk/biz/the-fat-bear-london?utm_campaign=yelp_api&utm_medium=api_v2_search&utm_source=HBWFcAmpcSdq8L-B0j3PZA\",\n     \"address\": \"61 Carter Lane,Blackfriars,London EC4V 5DY\",\n     \"is_closed\": false,\n     \"image_url\": \"https://s3-media1.fl.yelpcdn.com/bphoto/DVXghc62cG1UTK5wHKUwKw/ms.jpg\",\n     \"rating\": 4.5,\n     \"phone_no\": \"+44 20 7236 2498\",\n     \"category\": {\n         \"_id\": \"5719f4d974bb8c5b78d4871d\",\n         \"name\": \"restaurants\",\n         \"title\": \"Restaurants\",\n         \"__v\": 0\n     },\n     \"city\": {\n         \"_id\": \"5719f52a74bb8c5b78d48735\",\n         \"city\": \"London \",\n         \"country\": \"GB\",\n         \"__v\": 0\n     },\n     \"country\": \"GB\",\n     \"longitude\": -0.10138103121642,\n     \"latitude\": 51.5131833881734,\n     \"description\": \"Oh, what a night! Jurgen and Beth had organised a smallish yelper get-together and it was a treat! \\n\\nIt's a little hard to find as it's upstairs above a...\",\n     \"name\": \"The Fat Bear\",\n     \"__v\": 0,\n     \"openclosetime\": [],\n     \"created_on\": \"2016-04-27T06:45:44.228Z\",\n     \"sub_categories\": [\n         [\n             \"American Restaurants\",\n             \"newamerican\"\n         ],\n         [\n             \"Soul Food\",\n             \"soulfood\"\n         ],\n         [\n             \"BBQ & Barbecue\",\n             \"bbq\"\n         ]\n     ]\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/attractions.js",
    "groupTitle": "attractions",
    "name": "GetAttractions"
  },
  {
    "type": "get",
    "url": "attractions/count",
    "title": "Show number of attractions",
    "group": "attractions",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n  \n{\n     \"count\": \"626\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/attractions.js",
    "groupTitle": "attractions",
    "name": "GetAttractionsCount"
  },
  {
    "type": "get",
    "url": "attractions/:id",
    "title": "Show attractions details",
    "group": "attractions",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>'Mandatory' id of the Attractions.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n  \n{\n     \"_id\": \"5719f56ea6ac03950a003385\",\n     \"id\": \"regency-café-london-2\",\n     \"language\": \"ENGLISH\",\n     \"language_code\": \"en\",\n     \"yelp_full_url\": \"http://www.yelp.co.uk/biz/regency-caf%C3%A9-london-2?utm_campaign=yelp_api&utm_medium=api_v2_search&utm_source=HBWFcAmpcSdq8L-B0j3PZA\",\n     \"address\": \"17-19 Regency Street,Westminster,London SW1P 4BY\",\n     \"is_closed\": false,\n     \"image_url\": \"https://s3-media4.fl.yelpcdn.com/bphoto/Djp1AgwSG-gd8bq9V1ftiA/ms.jpg\",\n     \"rating\": 4.5,\n     \"phone_no\": \"+44 20 7821 6596\",\n     \"category\": {\n         \"_id\": \"5719f4d974bb8c5b78d4871d\",\n         \"name\": \"restaurants\",\n         \"title\": \"Restaurants\",\n         \"__v\": 0\n     },\n     \"city\": {\n         \"_id\": \"5719f52a74bb8c5b78d48735\",\n         \"city\": \"London \",\n         \"country\": \"GB\",\n         \"__v\": 0\n     },\n     \"country\": \"GB\",\n     \"longitude\": -0.13222007974201,\n     \"latitude\": 51.4940143171,\n     \"description\": \"The hubby first spotted the Regency Cafe when he watched the movie Layer Cake. He looked it up while we were recently in London and as the reviews were...\",\n     \"name\": \"Regency Café\",\n     \"__v\": 0,\n     \"openclosetime\": [],\n     \"created_on\": \"2016-04-27T05:31:58.633Z\",\n     \"sub_categories\": [\n         [\n             \"Breakfast & Brunch\",\n             \"breakfast_brunch\"\n         ],\n         [\n             \"British Restaurants\",\n             \"british\"\n         ],\n         [\n             \"Coffee & Tea Shops\",\n             \"coffee\"\n         ]\n     ]\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/attractions.js",
    "groupTitle": "attractions",
    "name": "GetAttractionsId"
  },
  {
    "type": "get",
    "url": "categories/",
    "title": "List all Categories",
    "group": "categories",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n   \n\t\t{\n        \"_id\": \"5719f4d974bb8c5b78d4871d\",\n        \"name\": \"restaurants\",\n        \"title\": \"Restaurants\",\n        \"__v\": 0\n \t},\n\t\t{\n        \"_id\": \"5719f4d974bb8c5b78d4871e\t\",\n        \"name\": \"publicservicesgovt\",\n        \"title\": \"Public Services & Government\",\n        \"__v\": 0\n \t}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/categories.js",
    "groupTitle": "categories",
    "name": "GetCategories"
  },
  {
    "type": "get",
    "url": "categories/:id",
    "title": "List the Category by given id",
    "group": "categories",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Mandatory id of the Categories.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n   \n\t\t{\n        \"_id\": \"5719f4d974bb8c5b78d4871d\",\n        \"name\": \"restaurants\",\n        \"title\": \"Restaurants\",\n        \"__v\": 0\n \t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Not Found\n{\n  \"message\": \"Data not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/categories.js",
    "groupTitle": "categories",
    "name": "GetCategoriesId"
  },
  {
    "type": "get",
    "url": "categories/:name",
    "title": "List the Category by given name",
    "group": "categories",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Mandatory name of the Categories.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n   \n\t\t{\n        \"_id\": \"5719f4d974bb8c5b78d4871d\",\n        \"name\": \"restaurants\",\n        \"title\": \"Restaurants\",\n        \"__v\": 0\n \t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Not Found\n{\n  \"message\": \"Data not found\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/categories.js",
    "groupTitle": "categories",
    "name": "GetCategoriesName"
  },
  {
    "type": "get",
    "url": "destinations/",
    "title": "Show number of destination",
    "group": "destinations",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/destinations.js",
    "groupTitle": "destinations",
    "name": "GetDestinations"
  },
  {
    "type": "get",
    "url": "destinations/",
    "title": "List all Destinations",
    "group": "destinations",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n   \n{\n      \"_id\": \"5719f52a74bb8c5b78d48729\",\n      \"city\": \"台北市\",\n      \"country\": \"TW\",\n      \"__v\": 0\n  },\n  {\n      \"_id\": \"5719f52a74bb8c5b78d4872a\",\n      \"city\": \"İstanbul\",\n      \"country\": \"TR\",\n      \"__v\": 0\n  },\n  {\n      \"_id\": \"5719f52a74bb8c5b78d4872b\",\n      \"city\": \"Paris\",\n      \"country\": \"FR\",\n      \"__v\": 0\n  },\n  {\n      \"_id\": \"5719f52a74bb8c5b78d4872c\",\n      \"city\": \"Marseille\",\n      \"country\": \"FR\",\n      \"__v\": 0\n  },\n  {\n      \"_id\": \"5719f52a74bb8c5b78d4872d\",\n      \"city\": \"Venice\",\n      \"country\": \"IT\",\n      \"__v\": 0\n  },\n  {\n      \"_id\": \"5719f52a74bb8c5b78d4872e\",\n      \"city\": \"Berlin\",\n      \"country\": \"DE\",\n      \"__v\": 0\n  },\n  {\n      \"_id\": \"5719f52a74bb8c5b78d4872f\",\n      \"city\": \"Frankfurt am Main\",\n      \"country\": \"DE\",\n      \"__v\": 0\n  },\n  {\n      \"_id\": \"5719f52a74bb8c5b78d48730\",\n      \"city\": \"東京\",\n      \"country\": \"JP\",\n      \"__v\": 0\n  },\n  {\n      \"_id\": \"5719f52a74bb8c5b78d48731\",\n      \"city\": \"大阪市 \",\n      \"country\": \"JP\",\n      \"__v\": 0\n  },\n  {\n      \"_id\": \"5719f52a74bb8c5b78d48732\",\n      \"city\": \"Auckland \",\n      \"country\": \"NZ\",\n      \"__v\": 0\n  },\n  {\n      \"_id\": \"5719f52a74bb8c5b78d48733\",\n      \"city\": \"Singapore \",\n      \"country\": \"SG\",\n      \"__v\": 0\n  },\n  {\n      \"_id\": \"5719f52a74bb8c5b78d48734\",\n      \"city\": \"香港 \",\n      \"country\": \"HK\",\n      \"__v\": 0\n  },\n  {\n      \"_id\": \"5719f52a74bb8c5b78d48735\",\n      \"city\": \"London \",\n      \"country\": \"GB\",\n      \"__v\": 0\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/destinations.js",
    "groupTitle": "destinations",
    "name": "GetDestinations"
  },
  {
    "type": "get",
    "url": "destinations/city/:city",
    "title": "Show destination by city",
    "group": "destinations",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n   \n{\n      \"_id\": \"5719f52a74bb8c5b78d48729\",\n      \"city\": \"台北市\",\n      \"country\": \"TW\",\n      \"__v\": 0\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/destinations.js",
    "groupTitle": "destinations",
    "name": "GetDestinationsCityCity"
  },
  {
    "type": "get",
    "url": "destinations/countries",
    "title": "Show all destination countries",
    "group": "destinations",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n   \n{\n  \"AR\": \"Argentina (AR)\",\n  \"AU\": \"Australia (AU)\",\n  \"AT\": \"Austria (AT)\",\n  \"BE\": \"Belgium (BE)\",\n  \"BR\": \"Brazil (BR)\",\n  \"CA\": \"Canada (CA)\",\n  \"CL\": \"Chile (CL)\",\n  \"CZ\": \"Czech Republic (CZ)\",\n  \"DK\": \"Denmark (DK)\",\n  \"FI\": \"Finland (FI)\",\n  \"FR\": \"France (FR)\",\n  \"DE\": \"Germany (DE)\",\n  \"HK\": \"Hong Kong (HK)\",\n  \"IT\": \"Italy (IT)\",\n  \"JP\": \"Japan (JP)\",\n  \"MY\": \"Malaysia (MY)\",\n  \"MX\": \"Mexico (MX)\",\n  \"NZ\": \"New Zealand (NZ)\",\n  \"NO\": \"Norway (NO)\",\n  \"PH\": \"Philippines (PH)\",\n  \"PL\": \"Poland (PL)\",\n  \"PT\": \"Portugal (PT)\",\n  \"IE\": \"Republic of Ireland (IE)\",\n  \"SG\": \"Singapore (SG)\",\n  \"ES\": \"Spain (ES)\",\n  \"SE\": \"Sweden (SE)\",\n  \"CH\": \"Switzerland (CH)\",\n  \"TW\": \"Taiwan (TW)\",\n  \"NL\": \"The Netherlands (NL)\",\n  \"TR\": \"Turkey (TR)\",\n  \"GB\": \"United Kingdom (GB)\",\n  \"US\": \"United States (US)\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/destinations.js",
    "groupTitle": "destinations",
    "name": "GetDestinationsCountries"
  },
  {
    "type": "get",
    "url": "destinations/country/:country",
    "title": "Show destination by country",
    "group": "destinations",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n   \n{\n      \"_id\": \"5719f52a74bb8c5b78d48729\",\n      \"city\": \"台北市\",\n      \"country\": \"TW\",\n      \"__v\": 0\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/destinations.js",
    "groupTitle": "destinations",
    "name": "GetDestinationsCountryCountry"
  },
  {
    "type": "get",
    "url": "destinations/:id",
    "title": "Show destination by id",
    "group": "destinations",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n   \n{\n      \"_id\": \"5719f52a74bb8c5b78d48729\",\n      \"city\": \"台北市\",\n      \"country\": \"TW\",\n      \"__v\": 0\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/destinations.js",
    "groupTitle": "destinations",
    "name": "GetDestinationsId"
  }
] });
