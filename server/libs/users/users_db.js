// load the things we need
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt   = require('bcrypt-nodejs'),
    uniqueValidator = require('mongoose-unique-validator');
    
// define the schema for our user model
var userSchema = mongoose.Schema({
    isadmin           : { type: Boolean, default: false },
    issuperadmin      : { type: Boolean, default: false },
    type              : { type:String},
    status            : { type: Boolean, default: true },
    verification_code : String,
    verified          : { type: Boolean, default: false }, 
    reset_pass_token  : String,
    created_on        : { type: Date, default: Date.now },
    profile_fields    : {},
    image             : String,
    timeline_image    : String,
    bookmark_attraction : [{ type: Schema.Types.ObjectId, ref: 'attractions'}],
    bookmark_journal  : [{ type: Schema.Types.ObjectId, ref: 'journals'}],
    bookmark_itinerary  : [{ type: Schema.Types.ObjectId, ref: 'itinerarys'}],
    local             : {
        email         : String,
        password      : String,
    },
    facebook           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    }
});

// Apply the uniqueValidator plugin to userSchema. 
userSchema.plugin(uniqueValidator);

userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('Users', userSchema);