//use statergy
var LocalStrategy = require('passport-local').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    users_api = require('./users_api'),
    users_db = require('./users_db');

var configAuth = require('../../config/auth');

// for using methods inside db
var hash = new users_db();

module.exports = function(passport) {

   // used to serialize the user for the session
  passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {

        users_db.findById(id, function(err, user) {
            done(err, user);
        });
    });

    //local-login strategy
    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function(req, username, password, done) {
        process.nextTick(function() {
            var email_username = {};
            if(users_api.IsEmail(username)){
                email_username["local.email"] = username ;
            }else{
                return done( "Please Pass Email" , null);
            }
          
            users_db.findOne(email_username).exec(function(err, user) {

                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false, "No User Found" );
                }
                if (!user.validPassword(password)) {
                    return done(null, false, "Wrong Password" );
                }
                 if(user.status == false){
                    return done(null, false, "User Blocked" );
                }
                if(user.verified == false){
                    return done(null, false, "Email Verification Pending");
                }else {
                    return done(null, user);
                }
            });
        });
    }
));


    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    passport.use(new FacebookStrategy({

        clientID        : configAuth.facebookAuth.clientID,
        clientSecret    : configAuth.facebookAuth.clientSecret,
        callbackURL     : configAuth.facebookAuth.callbackURL,
        profileFields   : ['id', 'name', 'email'],
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    },
    function(req, token, refreshToken, profile, done) {

        // asynchronous
        process.nextTick(function() {
            // check if the user is already logged in
            console.log("its my profile",profile);
            if (!req.user) {

                users_db.findOne({ 'facebook.id' : profile.id }, function(err, user) {
                    if (err)
                        return done(err);

                    console.log("profile",profile);

                    if (user) {

                        // if there is a user id already but no token (user was linked at one point and then removed)
                        if (!user.facebook.token) {
                            user.facebook.token = token;
                            user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                            // user.facebook.email = profile.emails[0].value ? (profile.emails[0].value || '').toLowerCase() : "";

                            user.save(function(err) {
                                if (err)
                                    return done(err);
                                    
                                return done(null, user);
                            });
                        }

                        return done(null, user); // user found, return that user
                    } else {
                        // if there is no user, create them

                        if(profile.emails){
                             users_api.getUserByEmail((profile.emails[0].value || '').toLowerCase(),function(err,result){
                                if(result && result._id){
                                    var user   = result; // pull the user out of the session
                                    user.facebook.id    = profile.id;
                                    user.facebook.token = token;
                                    user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                                    user.facebook.email = profile.emails[0].value ? (profile.emails[0].value || '').toLowerCase() : "";

                                    user.save(function(err) {
                                        if (err)
                                            return done(err);
                                            
                                        return done(null, user);
                                    });
                                }else{
                                    var newUser            = new users_db();
                                    newUser.facebook.id    = profile.id;
                                    newUser.facebook.token = token;
                                    newUser.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                                    newUser.facebook.email = (profile.emails[0].value || '').toLowerCase();
                                    newUser.verification_code = Math.floor(Math.random()*90000) + 10000;
                                    newUser.local.email = (profile.emails[0].value || '').toLowerCase();
                                    newUser.profile_fields= {};
                                    newUser.profile_fields.first_name = profile.name.givenName;
                                    newUser.profile_fields.last_name = profile.name.familyName;
                                    newUser.save(function(err) {
                                        if (err)
                                            return done(err);
                                            
                                        return done(null, newUser);
                                    });
                                } 
                            })
                        }else{
                            var newUser            = new users_db();
                            newUser.facebook.id    = profile.id;
                            newUser.facebook.token = token;
                            newUser.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                            newUser.verification_code = Math.floor(Math.random()*90000) + 10000;
                            newUser.profile_fields= {};
                            newUser.profile_fields.first_name = profile.name.givenName;
                            newUser.profile_fields.last_name = profile.name.familyName;
                            newUser.save(function(err) {
                                if (err)
                                    return done(err);
                                    
                                return done(null, newUser);
                            });
                        }
         


                    }
                });

            } else {

                users_db.findOne({ 'facebook.id' : profile.id }, function(err, user) {
                    if(user && user._id){
                         return done("Account already attached with other account", null);
                    }
                    // user already exists and is logged in, we have to link accounts
                    var user   = req.user; // pull the user out of the session
                    user.facebook.id    = profile.id;
                    user.facebook.token = token;
                    user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                    user.facebook.email = (profile.emails[0].value || '').toLowerCase();

                    user.save(function(err) {
                        if (err)
                            return done(err);
                            
                        return done(null, user);
                    });

                });

            }
        });

    }));



}
