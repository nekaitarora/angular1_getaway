var users_db = require('./users_db');

module.exports = {

    IsEmail: function(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    },

    /**
     * Check if user exist with email and username
     */
    checkIfExist: function(json ,cb) {

      var query = {$or :[{ 'local.email': json.local.email }]};
      
      //if _id then it must not same id
        if(json._id){
            query._id  = {};
            query._id['$ne'] =json._id; 
        }
        users_db.find(query).exec(function(err, result) {
            if (err) {return cb(err, null); }

            if(result[0] && result[0].local.email == json.local.email){
                return cb({message : "Email Already Exist"},null);
            }
            
            return cb(null, result);
        })
    },

    /**
     * Bookmark journal 
     */
    bookmarkJournal: function(user,jid ,cb) {


        if(user.bookmark_journal.indexOf(jid) > -1){
            console.log('cancel bookmark journal');
            var data  = {$pull: {bookmark_journal : jid}};
        }
        else if(user.bookmark_journal.indexOf(jid) == -1){
            console.log('bookmark this journal');
            var data ={ $addToSet: {bookmark_journal : jid }};

     }
        console.log("data in bookmark",data);
        users_db.update({_id: user._id},data , function(err, result) {
            if (err) {return cb(err, null); }
            console.log("user .........",result);
            return cb(null, result);
        })
    },
    /**
     * Bookmark Itinerary 
     */
    bookmarkItinerary: function(user,iid ,cb) {
        console.log("bookmark itin",user.bookmark_itinerary);
       
        if(user.bookmark_itinerary.indexOf(iid) > -1){
            console.log('cancel bookmark itinerary');
            var data  = {$pull: {bookmark_itinerary : iid}};
        }
        else if(user.bookmark_itinerary.indexOf(iid) == -1){
            console.log('bookmark this itinerary');
           var data ={ $addToSet: {bookmark_itinerary : iid }};

     }
        console.log("data in bookmark",data);
        users_db.update({_id: user._id},data , function(err, result) {
            if (err) {return cb(err, null); }
            console.log("user .........",result);
            return cb(null, result);
        })
    },    
   
    /**
     * Bookmark Attraction 
     */
    bookmarkAttraction: function(user,aid ,cb) {

        if(user.bookmark_attraction.indexOf(aid) > -1){
            console.log('cancel bookmark attraction');
            var data  = {$pull: {bookmark_attraction : aid}};
        }
        else if(user.bookmark_attraction.indexOf(aid) == -1){
            console.log('bookmark this attraction');
            var data ={ $addToSet: {bookmark_attraction : aid }};

     }
        console.log("data in bookmark",data);
        users_db.update({_id: user._id},data , function(err, result) {
            if (err) {return cb(err, null); }
            console.log("user .........",result);
            return cb(null, result);
        })
    },

    /**
     * Create New User
     */
    createUser: function(user, cb) {
        var newUser = new users_db(user);

        newUser.save(function(err, result) {
            if (err) {return cb(err, null);}
            return cb(null, result);
        })
    },


    /**
     * Total Count of Users
     */
    usersCount: function(query,cb) {
        query.filter ? filter = query.filter : filter = {};
        filter.issuperadmin = false;
        users_db.find(filter).count().exec(function(err, result) {
            if (err) {return cb(err, null);}
            return cb(null, result);
        })
    },

    /**
     * Get User By Id
     */
    getUser: function(id,password,cb) {
        var filter={};

        !password ? filter['local.password']= false : null ;
        users_db.findOne({ _id: id }, filter).exec(function(err, result) {
            if (err) {
                console.log("getUser Error " , err);
                return cb("User Not Found", null);
            }
            return cb(null, result);
        })
    },

    /**
     * Get User By Email
     */
    getUserByEmail: function(email, cb) {
        users_db.findOne({ 'local.email': email }, { 'local.password': false }).exec(function(err, result) {
            if (err) {return cb(err, null);}
            return cb(null, result);
        })
    },

    /**
     * Delete User using Id
     */
    deleteUser: function(id, cb) {
        users_db.remove({ _id: id, issuperadmin: false }, function(err, data) {
            if (err) {return cb(err, null);}
            return cb(null, data);
        })
    },

    /**
     * Update User using Id
     */
    updateUser: function(user, cb) {
        users_db.findOneAndUpdate({ _id: user._id }, user,{"new":true}).exec(function(err, doc) {
            if (err) {
                return cb(err, null);
            }
            return cb(null, doc);
        })
    },

    /**
     * Set User as Verified
     */
    verifyUser: function(token, email, cb) {
        new_verification_code = Math.floor(Math.random() * 90000) + 10000;
        users_db.findOneAndUpdate({ verification_code: token, 'local.email': email }, { verification_code: new_verification_code, verified: true }).exec(function(err, result) {
            if (err) {return cb(err, null);}
            return cb(null, result);
        })
    },

    /**
     * Update Forgot Verified
     */
    setNewPassword: function(email,token, password,cb) {
        new_verification_code = Math.floor(Math.random() * 90000) + 10000;
        users_db.findOneAndUpdate({ verification_code: token, 'local.email': email }, {$set : {verification_code: new_verification_code,'local.password' : password,verified : true}}).exec(function(err, result) {
            if (err) {return cb(err, null);}
            return cb(null, result);
        })
    },

    /**
     * Get All Users
     */
    getAllUsers: function(query, cb) {
        var skiplimit = {};
        var sort = null;
        query.filter ? filter = query.filter : filter = {};
        if (query.searchName) {
            filter["profile_fields.first_name"] = new RegExp(query.searchName, 'i');
        }
        if (query.searchEmail) {
            filter["local.email"] = new RegExp(query.searchEmail, 'i');
        }
        query.fields ? fields = query.fields : fields = { 'local.password': false };
        query.skip ? skiplimit.skip = query.skip : null;
        query.limit ? skiplimit.limit = query.limit : null;
        query.sort ? sort = query.sort : null;
        if (sort !== null) {
            users_db.find(filter, fields, skiplimit).sort(sort).exec(function(err, result) {
                if (err) {return cb(err, null);}
                return cb(null, result);
            })
        } else {
            users_db.find(filter, fields, skiplimit).exec(function(err, result) {
                if (err) {return cb(err, null);}
                return cb(null, result);
            })
        }
    },

    // // getUsersAutocomplete : function(query,cb){
    // //     var filter = {};
    // //     query.searchName ? filter["local.username"] = new RegExp(query.searchName,'i') : filter ={}; 
    // //       users_db.aggregate([{$match: filter }, {$project: {_id:1, username: "$local.username", email :"$local.email"}, } ]).exec(function(err,result){
    // //         if(err){return cb(err,null);}
    // //         return cb(null,result);
    // //     }) 
    // // },




    
    // //check user exist with username and password and not with _id
    // checknotexist: function(id,email,username ,cb) {
    //     users_db.find({_id:{$ne : id}, $or :[{ 'local.email': email },{'local.username':username}]}).exec(function(err, result) {
    //         if (err) {return cb(err, null); }
    //         return cb(null, result);
    //     })
    // },

    // getuserfulldetails: function(id, cb) {
    //     users_db.findOne({ '_id': id }).exec(function(err, result) {
    //         if (err) {
    //             return cb(err, null); 
    //         }
    //         return cb(null, result);
    //     })
    // },





}