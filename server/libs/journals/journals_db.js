var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var journalSchema = mongoose.Schema({
	title: String,
	description : String,
	created_by : { type: Schema.Types.ObjectId, ref: 'Users'},
	created_on : { type: Date, default: Date.now },
	image_url : String,
	language_code: String,
	language : String,
	publish : { type: Boolean, default: false },
	itineraries : [{ type: Schema.Types.ObjectId, ref: 'itinerarys'}],
	destinations : { type: Schema.Types.ObjectId, ref: 'destinations'},
	tags : [{ type: Schema.Types.ObjectId, ref: 'tags'}],
	views: { type: Number, default: 0 },
	likeBy: [{ type: Schema.Types.ObjectId, ref: 'Users'}],
	featured : { type: Boolean, default: false },
});

module.exports = mongoose.model('journals', journalSchema);