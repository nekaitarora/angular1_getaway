var journals_db = require('./journals_db');
var users_db = require('../users/users_db');

module.exports = {

    checkIfExist: function(journal ,cb) {

        users_db.find({'_id':journal}).exec(function(err, result) {
            if (err) {return cb(err, null); }
            return cb(null, result);
        })
    },


    createJournal: function(journal, cb) {

        var newJournal = new journals_db(journal);
        newJournal.save(function(err, result) {
            if (err) {return cb(err, null);}
            return cb(null, result);
        })
    },

    getall: function(query,cb){
        console.log("qqqqqqq",query);
        var skiplimit = {};
        var sort = null;
        var fields ={};
        query.filter ? filter = query.filter : filter = {};
        if (query.title) {
            filter["title"] = new RegExp(query.title, 'i');
        }
        if (query.filter && query.filter.language) {
            filter["language"] = new RegExp(query.filter.language, 'i');
        }
        query.fields ? fields = query.fields : null;
        query.skip ? skiplimit.skip = query.skip : null;
        query.limit ? skiplimit.limit = query.limit : null;
        query.sort ? sort = query.sort : null;
        if (sort !== null) {
            journals_db.find(filter, fields, skiplimit).sort(sort).populate('tags').populate('destinations').populate('itineraries').exec(function(err, result) {
                if (err) {return cb(err, null);}
                return cb(null, result);                
            })
        } else {
        journals_db.find(filter, fields, skiplimit).populate('tags').populate('destinations').populate('itineraries').exec(function(err, result){
            if (err) {return cb(err, null); }
            return cb(null, result);    
        })
    }
    },


    getjournal: function(id,cb) {
        journals_db.findOne({ _id: id }).populate('created_by').populate("destinations").populate("tags").populate('itineraries').exec(function(err, result) {
            if (err) {
                return cb("journal Not Found", null);
            }  
            console.log('hello',result);      
            return cb(null, result);
        })
    },  

    journalsCount: function(query,cb) {
        if(query.filter.language){
            query.filter["language"] = new RegExp(query.filter.language, 'i');
        }
        if(query.title){
            query.filter["title"] = new RegExp(query.title, 'i');
        }        
        query.filter ? filter = query.filter : filter = {};
        journals_db.find(filter).count().exec(function(err, result) {
            if (err) {return cb(err, null);}
            return cb(null, result);
        })
    },    

    updatejournal: function(journal, cb) {
        journals_db.findOneAndUpdate({ _id: journal._id },journal).exec(function(err, doc) {
            if (err) {
                return cb(err, null);
            }
            return cb(null, doc);
        })
    },

    likejournal: function(journal,likeBy,cb) {
        journals_db.findOneAndUpdate({ _id: journal._id },{$push:{likeBy:likeBy}},{"new":true}).exec(function(err, doc) {
            if (err) {
                return cb(err, null);
            }
            return cb(null, doc);
        })
    },


    unlikejournal: function(journal,unlikeBy,cb) {
        journals_db.findOneAndUpdate({ _id: journal._id },{$pull:{likeBy:unlikeBy._id}},{"new":true}).exec(function(err, doc) {
            if (err) {
                return cb(err, null);
            }
            return cb(null, doc);
        })
    },



    deletejournal: function(id, cb) {
        journals_db.remove({ _id: id}, function(err, data) {
            if (err) {return cb(err, null);}
            return cb(null, data);
        })
    },    



}