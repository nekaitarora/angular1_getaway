var attractions_db = require('./attractions_db');
var reviews_db = require('../reviews/reviews_db');
var async = require('async');
_ = require("underscore");
var mongoose = require('mongoose');

module.exports = {

    /**
     * Create Attraction By Import
     */
    createAttractionImport: function(attraction, cb) {
        attractions_db.findOneAndUpdate({ id: attraction.id }, attraction, { upsert: true }, function(err, result) {
            if (err) {
                return cb(err, null); }
            return cb(null, result);
        })
    },

    getAllAttractions: function(query, cb) {
        var skiplimit = {};
        var sort = null;
        query.filter ? filter = query.filter : filter = {};
        if (query.name) {
            filter["name"] = new RegExp(query.name, 'i');
        }
        query.fields ? fields = query.fields : fields = {};
        query.skip ? skiplimit.skip = query.skip : null;
        query.limit ? skiplimit.limit = query.limit : null;
        query.sort ? sort = query.sort : null;
        console.log(query);
        if (sort !== null) {
            attractions_db.find(filter, fields, skiplimit).sort(sort).populate('city').populate('category').exec(function(err, result) {
                if (err) {
                    return cb(err, null); }
                return cb(null, result);
            })
        } else {
            attractions_db.find(filter, fields, skiplimit).populate('city').populate('category').exec(function(err, result) {
                if (err) {
                    return cb(err, null); }
                return cb(null, result);
            })
        }
    },

    getAllAttractionsCount: function(query, cb) {
        query.filter ? filter = query.filter : filter = {};
        attractions_db.find(filter).count().exec(function(err, result) {
            if (err) {
                return cb(err, null); }
            return cb(null, result);
        })
    },
    getAttraction: function(id, cb) {
        // console.log('id is12345',id);
        attractions_db.findOne({ _id: id }).populate('city').populate('category').exec(function(err, result) {
            if (err) {
                return cb(err, null); }
            return cb(null, result);
        })
    },
    createAttraction: function(attraction, cb) {
        attractions_db.create(attraction, function(err, result) {
            if (err) {
                return cb(err, null); }
            return cb(null, result);
        })
    },
    updateAttraction: function(id, attraction, cb) {
        attractions_db.findOneAndUpdate({ _id: id }, attraction, { new: true }, function(err, result) {
            if (err) {
                return cb(err, null); }
            return cb(null, result);
        })
    },
    deleteAttraction: function(id, cb) {
        console.log('id here is', id);
        attractions_db.remove({ _id: id, id: { $exists: false } }, function(err, result) {
            if (err) {
                return cb(err, null); }
            return cb(null, result);
        })
    },
    updateReviews: function(id, data, cb) {
        console.log('finally', id, data);
        // console.log('db',reviews_db);
        reviews_db.findOne({ "attraction_id": id }, function(err, result) {
            if (err) {
                console.log(err);
                return cb(err, null);
            }
            console.log('result', result);
            if (result !== null) {
                var updated_on = new Date();
                console.log('update', updated_on);
                var a = _.where(result.reviews_data, { "user_id": data.reviews_data[0].user_id });
                console.log('a is', a);
                if (a.length > 0) {
                    var currentUserReview = {};
                    if (data.reviews_data[0].description !== undefined) {
                        currentUserReview['reviews_data.$.description'] = data.reviews_data[0].description;
                        currentUserReview['reviews_data.$.updated_on'] = updated_on;
                    } else if (data.reviews_data[0].rating !== undefined) {
                        currentUserReview['reviews_data.$.rating'] = data.reviews_data[0].rating;
                        currentUserReview['reviews_data.$.updated_on'] = updated_on;
                    }

                    reviews_db.update({ "attraction_id": id, "reviews_data.user_id": data.reviews_data[0].user_id }, { $set: currentUserReview }, { new: true }, function(error, output) {
                        if (error) return cb(error, null);
                        console.log('output here is', output);
                        return cb(error, output);
                    });

                } else if (a.length == 0) {
                    console.log('hello');
                    reviews_db.update({ "attraction_id": id }, { $push: { "reviews_data": data.reviews_data[0] } }, { upsert: true, new: true }, function(error, output) {
                        if (error) return cb(error, null);
                        console.log('output is', output);
                        return cb(error, output);
                    })
                }

            } else if (result == null) {
                reviews_db.create(data, function(error, output) {
                    if (error) return cb(error, null);
                    console.log('output is', output);
                    return cb(error, output);
                })
            }
        })
    },

    getAllReviews: function(query, cb) {
        console.log('query', query);
        // var sort = null;
        var skiplimit = {};
        var sort = null;
        query.filter ? filter = query.filter : filter = {};
        query.fields ? fields = query.fields : fields = {};
        query.skip ? skiplimit.skip = query.skip : null;
        query.limit ? skiplimit.limit = query.limit : null;
        query.sort ? sort = query.sort : null;
        console.log(query);
        if (sort !== null) {
            reviews_db.find(filter, fields, skiplimit).sort(sort).exec(function(err, result) {
                if (err) {
                    return cb(err, null); }
                return cb(null, result);
            })
        } else {
            reviews_db.find(filter, fields, skiplimit).exec(function(err, result) {
                if (err) {
                    return cb(err, null); }
                return cb(null, result);
            })
        }
    },
    getAverageRating: function(query, cb) {
        console.log('query here is', query);
        var userTotal = 0;
        var userCount = 0;
        var yelpCount = 0;
        var yelpTotal = 0;
        var obj = {};
        obj["attraction_id"] = mongoose.Types.ObjectId(query);
        console.log("match obj", obj);


        async.parallel({
                one: function(callback) {
                    attractions_api.getAttraction(query, function(err, res) {
                        if (err) { console.log("err........", err);
                            return cb(err, null); }
                        console.log('res of av rating is ', res);
                        if (res) {
                                var yelpData = {count:0,total:0};
                            if (res.reviews.length > 0) {
                                _.each(res.reviews, function(data) {
                                    if (data.rating) {
                                        yelpData.count = yelpData.count + 1;
                                        yelpData.total = yelpData.total + data.rating;
                                        console.log('yelp rating here is', yelpTotal, yelpCount);
                                    }
                                })
                            }
                              callback(null, yelpData);
                        }
                    })
                },
                two: function(callback) {
                    reviews_db.find(obj).exec(function(err, result) {
                        if (err) {
                            return cb(err, null); }
                        console.log('the result for reviews is', result);
                        var userData = {count:0,total:0};
                        if (result.length > 0) {
                            _.each(result[0].reviews_data, function(item) {
                                if (item.rating) {
                                    userData.count = userData.count + 1;
                                    userData.total = userData.total + item.rating;
                                    console.log('user rating here is', userTotal, userCount);
                                }
                            })
                        }
                            callback(null, userData);
                    })
                }
            },
            function(err, results) {
                var rating = (results.one.total + results.two.total )/ (results.one.count + results.two.count);
                console.log("rating..........",rating);
               return cb(null, {"rating":rating});
            })
    },
    
}
