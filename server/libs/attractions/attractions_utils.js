var global_config = require('../../config/global_config'),
    fs = require('fs'),
	language = require('../../config/language'),
    attractions_api = require('./attractions_api');

/* require the modules needed */
var oauthSignature = require('oauth-signature');
var n = require('nonce')();
var request = require('request');
var qs = require('querystring');
var _ = require('lodash');

var yelp_search_url  = "http://api.yelp.com/v2/search";
var yelp_business_url  = "https://api.yelp.com/v2/business";
module.exports = {

	/**
	 *  Importing All for category
	 */
	importAllForCategory: function(data, cb) {
		console.log("call comes for import all category data");
	  var thisFile = this;
	  //make clone of orignal data
	  var orignal_data =  Object.assign({}, data);
	  	//call for imorting for offset
	    thisFile.importAttraction(data,function(err,result,pending){
	    
	    if(pending){
			console.log("Result Pending call importAllForCategory Again");
	        orignal_data.offset = orignal_data.offset+20;
	      //more data so call me again
	      thisFile.importAllForCategory(orignal_data,cb);
	    }else{
	      if(err){return cb(err);}
	      return cb(null,result);
	    }
	  })
	},


	/**
	 * Manage All Importing Work
	 */
	importAttraction: function(parameters, cb) {
		console.log('importAttraction (20 import)' ,parameters);
		//get this scope in a variable otherwise it will not work inside of other function
		
		//get _id from Obj

		var city_id = parameters.term._id;
		var city_image = parameters.term.image;

		var category_id = parameters.category_filter._id;
		var category_name = parameters.category_filter.name;
		
		delete parameters.sort;
		
		//update parameters coming from front end
		parameters.cc = parameters.term.country;
		parameters.category_filter = parameters.category_filter.name;
		parameters.location = parameters.term.city;
		parameters.limit ? parameters.limit = parameters.limit : parameters.limit = 20;

		delete parameters.term;
		
		//Read IMPORT.JSON file for last prgoress  
		fs.readFile('import.json', function (err,data) {
			if (err) {return cb("Import File Error",null,false);}
      		
      		// file contains json so parse that first
      		var importJson = JSON.parse(data.toString());
      
	      	//if already data for city + category + offset then return
	        if(importJson[parameters.location] && importJson[parameters.location][parameters.category_filter] && importJson[parameters.location][parameters.category_filter][parameters.offset] &&  importJson[parameters.location][parameters.category_filter][parameters.offset].status == 'done'){
	          	return cb('Already Imported',null,true);
	        }

	        if(importJson[parameters.location] && importJson[parameters.location][parameters.category_filter] && importJson[parameters.location][parameters.category_filter] && importJson[parameters.location][parameters.category_filter].imported == importJson[parameters.location][parameters.category_filter].total_count ){
	          	return cb('Already Imported All Contents',null,false);
	        }

	        //if offset 0 then delete it
	        parameters.offset == 0 ? delete  parameters.offset : null;

            //calling yelp api and get josn from them
            console.log("calling yelp function for data",parameters);
            request_Yelp(parameters,yelp_search_url,function(err,result,body){
              // console.log('errr',body);
              //if no offset means we delete it before so re-set it
              !parameters.offset  ? parameters.offset = 0 : null;

              //yelp error due to some reasons
          	  if(err){return cb('Looks like yelp error',null,false);}

          	  // get total records count and how much records comes
          	  if(JSON.parse(body).total > 1000){
          	  	total_count = 1000;	
          	  }else{
          	  	var total_count =JSON.parse(body).total;
          	  }

              body = JSON.parse(body).businesses;
              if(!body || !body.length){
          	  	return cb('Looks like yelp error',null,false);
              }

              var records_imported_count = body.length;
          
          	  //if no record then return 
          	  if(body.length  == 0){
          	    return cb("No more Records for this city/category",null,false);
          	  }
            console.log("Insert into db all records recursiveDbInsert");
          	  //get businesss and call recursive function for entry
              recursiveDbInsert(body,city_id,category_id,category_name,city_image,function(err ,result){

                //if no json for city then create one
                !importJson[parameters.location] ?  importJson[parameters.location]={}: null

                //if no json for category in city then create one
                !importJson[parameters.location][parameters.category_filter] ?  importJson[parameters.location][parameters.category_filter]={}: null

                // if nothing imported yet set value 0
                !importJson[parameters.location][parameters.category_filter]['imported'] ?  importJson[parameters.location][parameters.category_filter]['imported']=0 : null
                
                //set total count 
                importJson[parameters.location][parameters.category_filter]['total_count'] = total_count;

                //if err comes from recurive function then set status pending
                if(err){
                	var rec_err = err;
                	console.log("errrrrrrrrr",err);
                  importJson[parameters.location][parameters.category_filter][parameters.offset] = {
                    records : records_imported_count,
                    status : "pending",
                  }
                }

                //if done importing using recursive then update importing value and set success
                if(result){
                  importJson[parameters.location][parameters.category_filter]['imported'] = importJson[parameters.location][parameters.category_filter]['imported'] + records_imported_count;
                  importJson[parameters.location][parameters.category_filter][parameters.offset] = {
                    records : records_imported_count,
                    status : "done",
                  }
            		console.log("20 Records inserted");
                }
                var more = false;
                if(importJson[parameters.location][parameters.category_filter]['imported'] < importJson[parameters.location][parameters.category_filter]['total_count']){
                	more = true;
                }
                //write this new json to import file and return with a success
                fs.writeFile('import.json',JSON.stringify(importJson), function (err,data) {
                  if(err){return cb("File Write Error",null);}
					if(rec_err){
						return cb(rec_err,null)
					}
					console.log("File write done , more is",more);

                  return cb(null,importJson,more);
                });
              })
            });
        });
	}
}

	/**
	 * Google place api for opening and closing time
	 */
	function getOpeningClosingTime(name,latitude,longitude,category,phone_number,cb) {
		if(!phone_number){
			return cb("no record", null)
		}
		var name = encodeURI(name);

		//get reference from google api if found
		var url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?radius=100&location="+latitude+","+longitude+"&type="+category+"&name="+name+"&key="+global_config.google_place_key;
		console.log('calling google places for get nearest places---',url);
		request(url, function(error, response, body){
			if(error){return cb(error,null);}
			console.log("first time body",body);
			body = JSON.parse(body);

			if(body.error_message){
				return cb(body.error_message, null,true)
			}

			if(body.results.length == 0){
				return cb("no record", null)
			}
			getRecordPhoneMatch(body.results ,phone_number ,function(err,result,photo){
			console.log("second time record",body);
				if(err){return cb("no record", null)}
					var new_result = {};
					new_result.openclosetime = result;
					new_result.image_url = [];
					new_result.image_url = photo;
				return cb(null,new_result);
			})
		});
	}

	 function getRecordPhoneMatch(data ,phone , cb){
	 	console.log('calling google places details for phone match');
		if(data.length > 0 && data[0]){
			var record = data[0];
			// get opening and closing time from record refernce field
			var url = "https://maps.googleapis.com/maps/api/place/details/json?reference="+record.reference+"&key="+global_config.google_place_key;
		 	
		 	request(url, function(error, response, body){
 				if(error){return cb(error,null,null)
 					console.log("if errrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr",error);

 				}
 					console.log("response status",response);

 				body = JSON.parse(body);
 				if(body.result && body.result.length == 0){
					return cb("no record", null,null)
				}
				var record = body.result;

				//insert if no phone number
				var place_phone_no =  record && record.international_phone_number  ? record.international_phone_number : null; 

				if(!place_phone_no){
			        data.shift();
		       		return getRecordPhoneMatch(data,phone, cb);
				}
					place_phone_no = place_phone_no.replace(/ /g,'');
					phone = phone.replace(/-/g,'').replace(/ /g,'');

					if(place_phone_no.indexOf(phone) > -1){
						var photo_url = [];
						if(record.photos && record.photos[0]){
							record.photos.forEach(function(image){
								var url = "https://maps.googleapis.com/maps/api/place/photo?maxwidth="+image.width+"&photoreference="+image.photo_reference+"&key="+global_config.google_place_key2;
								photo_url.push(url);
							})
						} 

						return cb(null,record.opening_hours ?  record.opening_hours.periods : null,photo_url);
					}else{
		       			data.shift();
			       		return getRecordPhoneMatch(data,phone, cb);
					}	
				});
		}else{
			return cb("no record", null);
		}
	}

	/**
	 * Insert Data in Db Recursivly
	 */
	function recursiveDbInsert(data,city_id,category_id,category_name,city_image, cb) {
		console.log("recursiveDbInsert",data.length )
	    //if array has record then alter them as per our data
	    if(data.length > 0 && data[0]){
	      record = data[0];
          console.log("---------------------",yelp_business_url+"/"+record.id);
          record.id = encodeURI(record.id);
          request_Yelp({actionlinks : true},yelp_business_url+"/"+record.id,function(err,result,body){
          	  record = JSON.parse(body);
		      var attraction ={};
		      attraction.id = record.id;
		      attraction.name = record.name ? record.name  : "";
		      attraction.description = record.snippet_text ? record.snippet_text :"";
		      attraction.latitude = record.location.coordinate && record.location.coordinate.latitude ? record.location.coordinate.latitude : "";
		      attraction.longitude =record.location.coordinate && record.location.coordinate.longitude ? record.location.coordinate.longitude : "";
		      attraction.country = record.location.country_code;
		      attraction.city = city_id;
		      attraction.published = true;
		      attraction.category = category_id;
		      attraction.sub_categories = record.categories ? record.categories : "";
		      attraction.phone_no = record.display_phone ? record.display_phone : "";
		      attraction.rating = record.rating ? record.rating : "";
		      attraction.yelp_image_url = record.image_url ? record.image_url : "" ;
		      attraction.is_closed = record.is_closed ? record.is_closed :"";
		      attraction.address = record.location && record.location.display_address ? record.location.display_address.toString() : "";
		      attraction.yelp_full_url = record.url ? record.url : "";
		      attraction.eat24_url = record.eat24_url ? record.eat24_url :"";
		      attraction.reservation_url = record.reservation_url ? record.reservation_url :"";
		      attraction.is_claimed = record.is_claimed ? record.is_claimed :"";
		      attraction.review_count = record.review_count? record.review_count :"";
		      attraction.reviews = record.reviews ? record.reviews : "";
          
			      var phone_number = record.display_phone ? record.display_phone : null;

				detectLanguage(record.location.display_address[0],function(err,result,end){
					if(err && end){return cb({"detectlanguage": err}, null);}
			        attraction.language_code = result;
			        attraction.language = language[result] ? language[result] : "";

			        //get opening and closing time for attraction
				getOpeningClosingTime(attraction.name ,attraction.latitude , attraction.longitude,category_name,phone_number,function(err,result,end){
						
						if(err && end){return cb({"googleplaces": err}, null);}

		        		if(result && result.openclosetime){
		        			attraction.openclosetime = result.openclosetime;
		        		}

		        		if(result && result.image_url && result.image_url.length > 0){
		        			attraction.image_url = result.image_url;
		        		}else{
		        			 attraction.image_url = [city_image];
		        		}

				        //if success then delete first record and call function with new data
			            attractions_api.createAttractionImport(attraction,function(err,result){
					        //if error comes then other recursive breaks and returns
					        if(err){return cb(err, null);}
				        
				        	//if success then delete first record and call function with new data
					        data.shift();
					        return recursiveDbInsert(data,city_id,category_id,category_name,city_image, cb);

			        	});
				    })

			      })
          });
	    }else{
	      //all records done or no record 
	     return cb(null, 'imported');
	    }
	}



	/**
	 * Request api of yelp and give response 
	 */
	 function request_Yelp(set_parameters,url, cb) {

		/* The type of request */
		var httpMethod = 'GET';

		/* We can setup default parameters here */
		var default_parameters = {
		    // ll:'35.6868279,139.6966516',
		    // lang : 'en',
		    // sort: '1',
		    // limit : '1',
		    // category_filter: 'hotels'
		};

		/* We set the require parameters here */
		var required_parameters = {
		  oauth_consumer_key : global_config.yelp_oauth_consumer_key,
		  oauth_token : global_config.yelp_oauth_token,
		  oauth_nonce : n(),
		  oauth_timestamp : n().toString().substr(0,10),
		  oauth_signature_method : 'HMAC-SHA1',
		  oauth_version : '1.0'
		};

		/* We combine all the parameters in order of importance */ 
		var parameters = _.assign(default_parameters, set_parameters, required_parameters);

		/* We set our secrets here */
		var consumerSecret = global_config.yelp_consumerSecret;
		var tokenSecret = global_config.yelp_tokenSecret;

		/* Then we call Yelp's Oauth 1.0a server, and it returns a signature */
		/* Note: This signature is only good for 300 seconds after the oauth_timestamp */
		var signature = oauthSignature.generate(httpMethod, url, parameters, consumerSecret, tokenSecret, { encodeSignature: false});

		/* We add the signature to the list of paramters */
		parameters.oauth_signature = signature;

		/* Then we turn the paramters object, to a query string */
		var paramURL = qs.stringify(parameters);

		/* Add the query string to the url */
		var apiURL = url+'?'+paramURL;

		/* Then we use request to send make the API Request */
		request(apiURL, function(error, response, body){
		  return cb(error, response, body);
		});
	}



	function detectLanguage(string,cb){
		var string = encodeURI(string);
		var url = "http://ws.detectlanguage.com/0.2/detect?q="+string+"&key="+global_config.detect_language_key;
		request(url, function(error, response, body){
			if(error){return cb(error,null)}

				if(!JSON.parse(body).data){
					return cb(JSON.parse(body).error.message,null,true);
				}
				// return cb(null,"en");
			return cb(null,JSON.parse(body).data.detections[0] && JSON.parse(body).data.detections[0].language ? JSON.parse(body).data.detections[0].language : "");
		});
	}