// load the things we need
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
// define the schema for our Attraction model
var attractionsSchema = mongoose.Schema({
    name : String,
    id : String,
    language_code : String,
    language : String,
    description : String,
    latitude :  Number,
    longitude : Number,
    country  : String,
    city  : { type: Schema.Types.ObjectId, ref: 'destinations'},
    phone_no : String,
    rating : Number,
    yelp_image_url : String,
   
    category : { type: Schema.Types.ObjectId, ref: 'categories'},
    sub_categories : [],
    is_closed : Boolean,
    address : String,
    yelp_full_url:String,
    budget_range : String,
    openclosetime : [],
    reviews:[],
    eat24_url : String,
    reservation_url : String,
    is_claimed : Boolean,
    review_count : Number,
    // extra fields
    published : { type: Boolean, default: false },
    is_always_open : { type: Boolean, default: false },
    image_url: [],
    created_on :  { type: Date, default: Date.now },
    created_by : {type: Schema.Types.ObjectId, ref: 'Users'}
    
});


module.exports = mongoose.model('attractions', attractionsSchema);