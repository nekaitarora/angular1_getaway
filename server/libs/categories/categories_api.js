var categories_db = require('./categories_db');

module.exports = {
	createCategories : function(json,cb){
		categories_db.create(json,function(err,result){
			if(err){return cb(err,null);}
				return cb(null,result);
		})
	},
	deleteCategories : function(id,cb){
		categories_db.remove({_id: id },function(err,result){
			if(err){return cb(err,null);}
				return cb(null,result);
		})
	},
	getCategories : function(cb){
		categories_db.find({},function(err,result){
			if(err){return cb(err,null);}
				return cb(null,result);
		})	
	},
	getCategory : function(id,cb){
		categories_db.findOne({_id: id },function(err,result){
			if(err){return cb(err,null);}
				return cb(null,result);
		})
	},
	getCategoryByName : function(name,cb){
		categories_db.findOne({name: name },function(err,result){
			if(err){return cb(err,null);}
				return cb(null,result);
		})
	}
}