var comments_db = require('./comments_db');
var users_db = require('../users/users_db');
var journals_db = require('../journals/journals_db');

module.exports = {

    commentsCount: function(query,cb) {
        query.filter ? filter = query.filter : filter = {};
        comments_db.find(filter).count().exec(function(err, result) {
            if (err) {return cb(err, null);}
            return cb(null, result);
        })
    },

    getall: function(query,cb){
        var skiplimit = {};
        var sort = null;
        query.filter ? filter = query.filter : filter = {};
        if (query.description) {
            filter["description"] = new RegExp(query.description, 'i');
        }
        if(query.publish == true){
            filter['publish'] = true;
        }
        else if(query.publish == false){
            filter['publish'] = false;
        }

        query.fields ? fields = query.fields : null;
        query.skip ? skiplimit.skip = query.skip : null;
        query.limit ? skiplimit.limit = query.limit : null;
        query.sort ? sort = query.sort : null;
        if (sort !== null) {
            comments_db.find(filter, fields, skiplimit).sort(sort).populate("created_by").exec(function(err, result) {
                if (err) {return cb(err, null);}
                return cb(null, result);
            })
        } else {
        comments_db.find(filter, fields, skiplimit).populate("created_by").exec(function(err, result){
            if (err) {return cb(err, null); }
            return cb(null, result);
        })
    }
    },

    deleteComments: function(id, cb) {
        comments_db.remove({ _id: id}, function(err, data) {
            if (err) {return cb(err, null);}
            return cb(null, data);
        })
    },

    checkIfExist: function(comments ,cb) {

        journals_db.find({'_id':comments}).exec(function(err, result) {
            if (err) {return cb(err, null); }
            return cb(null, result);
        })
    },

    createComments: function(comments, cb) {

        var newcomments = new comments_db(comments);
        newcomments.save(function(err, result) {
            if (err) {return cb(err, null);}
            return cb(null, result);
        })
    },

    getComments: function(id,cb) {
        comments_db.findOne({ _id: id }).exec(function(err, result) {
            if (err) {
                return cb("comments Not Found", null);
            }
            return cb(null, result);
        })
    },

    updateComments: function(comments, cb) {
        comments_db.findOneAndUpdate({ _id: comments._id }, comments).exec(function(err, doc) {
            if (err) {
                return cb(err, null);
            }
            return cb(null, doc);
        })
    },  


























  

    

  


    



}