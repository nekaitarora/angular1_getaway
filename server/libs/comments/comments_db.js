var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var commentsSchema = mongoose.Schema({
	description : String,
	created_by : { type: Schema.Types.ObjectId, ref: 'Users'},
	created_on : { type: Date, default: Date.now },
	publish : { type: Boolean, default: false },
	journal : { type: Schema.Types.ObjectId, ref: 'journals'},
});

module.exports = mongoose.model('comments', commentsSchema);