var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var reviewsSchema = mongoose.Schema({
	attraction_id: {type: Schema.Types.ObjectId, ref: 'attractions'},
	reviews_data: [],
});

module.exports = mongoose.model('reviews', reviewsSchema);