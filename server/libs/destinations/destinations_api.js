var destinations_db = require('./destinations_db');

module.exports = {
	createDestination : function(json,cb){
		destinations_db.create(json,function(err,result){
			if(err){return cb(err,null);}
				return cb(null,result);
		})
	},
	deleteDestination: function(id,cb){
		destinations_db.remove({_id: id },function(err,result){
			if(err){return cb(err,null);}
				return cb(null,result);
		})
	},
	getDestinations : function(query,cb){
        var skiplimit = {};
        var sort = null;
        query.filter ? filter = query.filter : filter = {};

        if(query.city){
            filter["city"] = new RegExp(query.city, 'i');
        }
        query.skip ? skiplimit.skip = query.skip : null;
        query.limit ? skiplimit.limit = query.limit : null;
        query.sort ? sort = query.sort : null;
        if (sort !== null) {
            destinations_db.find(filter,{}, skiplimit).sort(sort).exec(function(err, result) {
                if (err) {return cb(err, null);}
                return cb(null, result);
            })
        } else {
            destinations_db.find(filter, {}, skiplimit).exec(function(err, result) {
                if (err) {return cb(err, null);}
                return cb(null, result);
            })
        }
	},
	getDestinationsCount : function(query,cb){
		query.filter ? filter = query.filter : filter = {};
		if(query.city){
            filter["city"] = new RegExp(query.city, 'i');
        }
		destinations_db.find(filter).count().exec(function(err, result) {
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	},
	getDestination : function(id,cb){
		destinations_db.findOne({_id: id },function(err,result){
			if(err){return cb(err,null);}
				return cb(null,result);
		})
	},	
	getDestinationsByCity : function(city,cb){
		destinations_db.findOne({city: city },function(err,result){
			if(err){return cb(err,null);}
				return cb(null,result);
		})
	},
	getDestinationsByCountry : function(country,cb){
		destinations_db.findOne({country: country },function(err,result){
			if(err){return cb(err,null);}
				return cb(null,result);
		})
	},
	UpdateDestination : function(id,destination,cb){
		destinations_db.findOneAndUpdate({_id: id},destination,{upsert :true, new: true},function(err,result){
			if(err){return cb(err,null);}
				return cb(null,result);
		})
	}
}