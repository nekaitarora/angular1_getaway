// load the things we need
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
// define the schema for our Attraction model
var itinerarysSchema = mongoose.Schema({
	title : String ,
    created_on : { type: Date, default: Date.now },
    created_by : { type: Schema.Types.ObjectId, ref: 'Users'},
    published  : {type : Boolean ,default:false },
    isdeleted  : {type : Boolean ,default:false },
    start_date : { type: Date},
    end_date   : { type: Date},
    is_return  : {type : Boolean , default:true},
    attractions  : [{ type: Schema.Types.ObjectId, ref: 'attractions'}],
    machine_name : { type : String},
    trip_from  :  { type: Schema.Types.ObjectId, ref: 'destinations'},
    trip_to    : [{destination : { type: Schema.Types.ObjectId, ref: 'destinations'}, start_date:String , end_date : String }], 
    days :{},
    image : String,
    anonymous : {type : Boolean , default:false},

    featured: {type : Boolean ,default:false }

    // trip_to    : [{destination : { type: Schema.Types.ObjectId, ref: 'destinations'}, start_date:String , end_date : String ,stay_days:Number}],

    // days :[{
    //     places:[],
    //     index : String,
    //     Date : String,
    //     city : String,
    //     day_start_time : String 
    // }],
/*
    name
    id
    image_url
    longitude
    latitude
    type
    start_time
    stay_time
    next_duration
    next_distance
    next_travel_mode
    next_distance_url
*/

});
module.exports = mongoose.model('itinerarys', itinerarysSchema);