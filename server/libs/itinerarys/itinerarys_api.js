var itinerarys_db = require('./itinerarys_db');
var  global_config = require('../../config/global_config');
var request = require('request');
var _ =require('underscore');
var attractions_api = require('../attractions/attractions_api')
module.exports = {
	//call goole apis for distance 
	googleApis: function(options,cb){
		var url = "https://maps.googleapis.com/maps/api/directions/json?origin="+options.trip_from+"&destination="+options.trip_to ;
		options.mode ?url = url + "&mode="+options.mode : null;
		options.departure_time ?url = url + "&departure_time="+options.departure_time : null;
		url = url + "&key="+global_config.googleApisKey;
		request(url, function(error, response, body){
			if(error){return cb(error,null)}
			body = JSON.parse(body);
			cb(null,body)
		});
	},
	createItinerary : function(itinerary,cb){
		itinerarys_db.create(itinerary,function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	},

	getItinerary : function(id,machine_name,cb){
		itinerarys_db.findOne({_id : id , machine_name : machine_name }).populate("trip_to.destination").populate("trip_from").exec(function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	},
	updateItinerary : function(id,json,cb){
		itinerarys_db.findOneAndUpdate({_id : id },json,{new :true}).populate("trip_to.destination").populate("trip_from").exec(function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	},
	deleteItinerary : function(id,cb){
		itinerarys_db.remove({_id : id },function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	},
	markDeleted : function(id,machine_name,uid,cb){
		itinerarys_db.findOneAndUpdate({_id : id ,machine_name : machine_name , created_by : uid },{isdeleted : true},function(err,result){
			if(err){return cb(err,null);}
			return cb(null,result);
		})
	},

    getall: function(query, cb) {
        var skiplimit = {};
        var sort = null;
        var fields = {};
        query.filter ? filter = query.filter : filter = {};
        filter.anonymous = false;
        
        if (query.title) {
            filter["title"] = new RegExp(query.title, 'i');
        }
        // if (query.filter && query.filter.language) {
        //     filter["language"] = new RegExp(query.filter.language, 'i');
        // }
        query.fields ? fields = query.fields : null;
        query.skip ? skiplimit.skip = query.skip : null;
        query.limit ? skiplimit.limit = query.limit : null;
        query.sort ? sort = query.sort : null;

        if (sort !== null) {
            itinerarys_db.find(filter, fields, skiplimit).sort(sort).populate('created_by').populate('trip_to.destination').exec(function(err, result) {
                if (err) {
                    return cb(err, null); }
                return cb(null, result);
            })
        } else {
            itinerarys_db.find(filter, fields, skiplimit).populate('created_by').populate('trip_to.destination').exec(function(err, result) {
                if (err) {
                    return cb(err, null); }
                return cb(null, result);
            })
        }
    },

    itinerariesCount: function(query,cb) {
        if(query.filter && query.filter.language){
            query.filter["language"] = new RegExp(query.filter.language, 'i');
        }
        if(query.title && query.title){
            query.filter["title"] = new RegExp(query.title, 'i');
        }        
        query.filter ? filter = query.filter : filter = {};
        itinerarys_db.find(filter).count().exec(function(err, result) {
            if (err) {return cb(err, null);}
            return cb(null, result);
        })
    },	

    deletePermanently: function(id, cb) {
        itinerarys_db.remove({ _id: id}, function(err, data) {
            if (err) {return cb(err, null);}
            return cb(null, data);
        })
    },    

}