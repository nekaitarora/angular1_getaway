var journals_db = require('../journals/journals_db');
var users_db = require('../users/users_db');
var tags_db = require('./tags_db');

module.exports = {

    createTags: function(tag, cb) {

        var newTag = new tags_db(tag);
        newTag.save(function(err, result) {
            if (err) {return cb(err, null);}
            return cb(null, result);
        })
    },

    getall: function(query,cb){
        var sort = null;
        query.filter ? filter = query.filter : filter = {};
        query.fields ? fields = query.fields : null;
        query.sort ? sort = query.sort : null;
        if (sort !== null) {
            tags_db.find(filter, fields).sort(sort).exec(function(err, result) {
                if (err) {return cb(err, null);}
                return cb(null, result);
            })
        } else {
    	tags_db.find(filter, fields).exec(function(err, result){
    		if (err) {return cb(err, null); }
            return cb(null, result);
    	})
    }
    },
    getTag: function(id,password,cb) {
        //var filter={};
        tags_db.findOne({ _id: id }).exec(function(err, result) {
            if (err) {
                console.log("getTag Error " , err);
                return cb("Tag Not Found", null);
            }
            return cb(null, result);
        })
    },



}