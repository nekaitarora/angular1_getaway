var nodemailer = require("nodemailer");
var dbconfig = require('./global_config');


smtpTransport = nodemailer.createTransport("SMTP",{
  service: "Gmail",
  auth: {
    user: dbconfig.mail_username,
    pass: dbconfig.mail_password
  }
});

var mailfunctions={};
mailfunctions.welcomemail = function(token,email){
  var path = dbconfig.domain + "#/user/verify/"+token+"/"+email;

var html = '<table style="text-align:center;width:100%;background:url('+dbconfig.domain+'images/slide-one.jpg)  no-repeat center center / 100% auto ;background-size:100% 100%;"> <tr> <td align="center"><img src="'+dbconfig.domain+'images/logo-getawey.png"></td> </tr> <tr> <td align="center"><h2 style="font-size:30px;color:white">Welcome to travel itinerary</h2><h3 style="color:white">'+email+'</h3></td> </tr> <tr> <td align="center" style="height:40px;"><a href="'+path+'" style="font-size:20px;padding:10px;background-color: #4a97d2;color:white;text-decoration: none;font-weight: bold;">Activate Account</a></td> </tr> </table>';
  var subject = "Welcome Mail";
  mailfunctions.sendmail(email, subject,html);
}

mailfunctions.approveJournal = function(id,user){

var path = dbconfig.domain + "#/admin/journals/edit/"+id;
var html = '<table style="text-align:center;width:100%;background:url('+dbconfig.domain+'images/slide-one.jpg)  no-repeat center center / 100% auto ;background-size:100% 100%;"> <tr> <td align="center"><img src="'+dbconfig.domain+'images/logo-getawey.png"></td> </tr> <tr> <td align="center"><h2 style="font-size:30px;color:white">Welcome to travel itinerary</h2></td> </tr> <tr> <td align="center" style="height:40px;"><a href="'+path+'" style="font-size:20px;padding:10px;background-color: #4a97d2;color:white;text-decoration: none;font-weight: bold;">Approve Journal</a></td> </tr> </table>';
var subject = "Approve Journal request by " + user.profile_fields.first_name;
mailfunctions.sendmail(dbconfig.mail_to, subject,html);
}

mailfunctions.approveAttraction = function(id,user){
var path = dbconfig.domain + "#/admin/attractions/edit/"+id;
var html = '<table style="text-align:center;width:100%;background:url('+dbconfig.domain+'images/slide-one.jpg)  no-repeat center center / 100% auto ;background-size:100% 100%;"> <tr> <td align="center"><img src="'+dbconfig.domain+'images/logo-getawey.png"></td> </tr> <tr> <td align="center"><h2 style="font-size:30px;color:white">Welcome to travel itinerary</h2></td> </tr> <tr> <td align="center" style="height:40px;"><a href="'+path+'" style="font-size:20px;padding:10px;background-color: #4a97d2;color:white;text-decoration: none;font-weight: bold;">Approve Attraction</a></td> </tr> </table>';
var subject = "Approve Attraction request by " + user.profile_fields.first_name;
mailfunctions.sendmail( dbconfig.mail_to,subject,html);
}

mailfunctions.sendmail = function(to,subject,html ){
	html = html.replace(/<<domain>>/g, dbconfig.domain);

	console.log('going to send mail..');
	mailOptions={
	  from: dbconfig.mail_from,
	  to : to,
	  subject : subject,
	  html : html
	};
	smtpTransport.sendMail(mailOptions, function(error, response){
		if(error){
			console.log("error",error);
		}else{
			console.log("mail response", response);
		}
	})
}
module.exports = mailfunctions;