//npm modules
var express  = require('express');
var app      = express();
var mongoose = require('mongoose');
var cors = require('express-cors');
var bodyParser   = require('body-parser');
var cookieParser = require('cookie-parser');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

var passport = require('passport');
var fs = require('fs');
var path = require('path');

// configuration ===============================================================
var configDB = require('./config/global_config.js');
mongoose.connect(configDB.url); // connect to our database

app.use(cookieParser()); // read cookies 
app.use(bodyParser.json({limit: '50mb'}));  //parse body
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(express.static(__dirname + '/../client'));
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/apidoc'));

app.get('/apidoc',function(req, res){
    res.sendFile(path.join(__dirname, '/apidoc', 'index.html'));
});

// app.use(function (req, res, next) {
// 	if(req.headers.referer == 'http://testsite.com:8080/' || req.headers.referer == 'http://139.162.5.142:8080/'){
//   		next();
// 	}else{
// 		return next("Permission Denied");
// 	}
// });


//set port number
var port = process.env.PORT || 8080;

app.use(session({
    secret: 'yolo',
    store: new MongoStore({
    	url: configDB.url,
    })
}));

//cross origin setting
app.use(cors({
  allowedOrigins: [
    'testsite.com:8080','facebook.com','139.162.5.142','139.162.5.142:8080'
  ],
  credentials: true
}))

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

//routes
require('./libs/users/passport')(passport);
require('./routes/index')(app);
require('./routes/upload')(app,__dirname);

app.listen(port);
console.log('Server Start At ' + port);

module.exports = app;
